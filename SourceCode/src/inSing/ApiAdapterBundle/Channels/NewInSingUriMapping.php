<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class NewInSingUriMapping
{
    const KENTTEST  = "/events";

    const HOMEPAGE_CAROUSEL_MODULE  = "/homepage/carousel";
    const HOMEPAGE_HGW_MODULE  = "/homepage/hgw";
}
