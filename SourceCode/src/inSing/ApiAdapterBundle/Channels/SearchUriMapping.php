<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class SearchUriMapping
{
    const EVENT_SEARCH    = "/5.0/events/search";
    const EVENT_DETAILS   = "/5.0/event/{id}";
    const EVENT_PREV_NEXT = "/5.0/event/{id}/prev-next";
    const BUSINESS_DETAIL_IDS = "/5.0/business/{ids}";
    const BUSINESS_SEARCH_V5 = "/5.0/business/search";
}
