<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace inSing\ApiAdapterBundle\Channels\Helper;

/**
 * Description of ThirdPartyApi
 *
 * @author linuxmint
 */
class TableDBApiAdapter
{
    protected $_logger = null;
    protected $_cache = null;
    protected $_channel = null;
    protected $_container = null;
    protected $_config = array();

    protected $basicUrl = null;

    private function initCache($container, $cache)
    {
        $cacheObject = $container->get('new.insing.cache');
        return $cacheObject;
    }

    public function __construct($container, $cache, $channelLog, $channelConfig)
    {
        $this->_container = $container;
        $this->_logger = $container->get($channelLog);
        $this->_cache = $this->initCache($container, $cache);

        $configs = $this->loadConfig($channelConfig);

        $this->_channel = $channelLog;        

        $this->basicUrl = $configs["basic_url"];

    }

    protected function loadConfig($channel)
    {
        $channelsApi = $this->_container->getParameter("channels_api");
        $this->_config = $channelsApi[$channel];

        return $this->_config;
    }


    protected function doGetWrapper($url, $params) {

        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_GET);
        $curl->setChannel($this->_channel);

        $res = $curl->call($url, $params);

        if (is_array($res)) {
            $this->_logger->info("Success URL= " . $url);
            //$this->_logger->info(json_encode($res));
        } else {
            $this->_logger->error('RETURN NULL, URL= ' . $url);
        }

        return $res;
    }

    protected function doGetJsonWrapper($url, $params) {
        try {
            $curl = new Curl($this->_container);
            $curl->setMethod(Curl::HTTP_GET);
            $res = $curl->call($url, $params);
            if (!$res) {
                return array();
            }
            return $res;
        } catch (\Exception $exc) {
            $this->_logger->error("CALL FAIL URL= " . $url);
            $this->_logger->error($exc->getMessage());
        }
        return array();
    }

    protected function runApiByMethod($url, $method, $params) {
        switch ($method) {
            case "GET":
                if (isset($params['content_type']) && $params['content_type'] == 'json') {
                    $results = $this->doGetJsonWrapper($url, $params);
                } else {
                    $results = $this->doGetWrapper($url, $params);
                }
                break;
            default:   
                //default GET
                $results = $this->doGetWrapper($url, $params);
                break;
        }

        if (count($results['response']['data']) == 0) {
            $this->_logger->info($url);
            $this->_logger->info("Call Failed, Results: " . json_encode($results));
            $this->_logger->info("Call Failed, Params: " . json_encode($params));
        }
        //write log
        //$this->_logger->info("Call API Success", $results);
        return $results;
    }
}
