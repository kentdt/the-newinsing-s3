<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace inSing\ApiAdapterBundle\Channels\Helper;

/**
 * Description of ThirdPartyApi
 *
 * @author linuxmint
 */
class ApiHgw4Adapter extends Signature
{
    protected $_logger = null;
    protected $_cache = null;
    protected $_channel = null;
    protected $_container = null;
    protected $_config = array();

    protected $appId = null;
    protected $secret = null;
    protected $sigSecret = null;

    protected $basicUrl = null;
    protected $authenticateUrl = null;
    protected $sessionTokenCacheKey = null;


    protected function initCache($container, $cache)
    {
        $cacheObject = $container->get('new.insing.cache');
        return $cacheObject;
    }

    public function __construct($container, $cache, $channelLog, $channelConfig)
    {
        $this->_container = $container;
        $this->_logger = $container->get($channelLog);
        $this->_cache = $this->initCache($container, $cache);

        $configs = $this->loadConfig($channelConfig);

        $this->_channel = $channelLog;        

        $this->appId = $configs["app_id"];
        $this->secret = $configs["secret"];
        $this->sigSecret = $configs["sig_secret"];

        $this->authenticateUrl = $configs["authenticate_url"];
        $this->basicUrl = $configs["basic_url"];

        $this->sessionTokenCacheKey = $configs["session_token_cache_key"];

        parent::__construct($this->sigSecret);
    }

    protected function loadConfig($channel)
    {
        $channelsApi = $this->_container->getParameter("channels_api");
        $this->_config = $channelsApi[$channel];
        return $this->_config;
    }



    protected function authenticate()
    {
        try
        {
            $inputs = array('app_id' => $this->appId,
                            'secret' => $this->secret);
            $pathInfo = $this->genPathInfo($this->authenticateUrl);
            $jsonString = json_encode($inputs);
            $sig = $this->genSignatureJson($jsonString, $pathInfo);

            $curl = new Curl($this->_container);
            $curl->setPostJson(true);
            $curl->setMethod(Curl::HTTP_POST);
            $curl->setChannel($this->_channel);
            $finalUrl = $this->authenticateUrl . "?sig={$sig}";
            $result = $curl->call($finalUrl, $inputs);

            $this->_logger->info("[pathinfo]:[{$pathInfo}], [inputs]:[{$jsonString}]");
            $this->_logger->info("[url]:[{$finalUrl}]");
            $this->_logger->info("[results]:[". json_encode($result) ."]");

            //{"status":"200","session_token":"b7a041808fc5263c13efc9c18c03f784","expire_at":"1438831598"}

            if (RestfulAPIHelper::HTTP_OK != $result['status']) {
                $this->_logger->error("Authenticate fail with status:[". $result['status'] ."]");
                $this->_logger->error("[appId]:[{$this->appId}]");
                $this->_logger->error("[secret]:[{$this->secret}]");
                $this->_logger->error("[sigSecret]:[{$this->sigSecret}]");
                return null;
            }
            return $result;
        } catch (\Exception $exc) {
            $this->_logger->error("Authenticate fail with exception, " . $exc->getMessage());
            $this->_logger->error("[appId]:[{$this->appId}]");
            $this->_logger->error("[secret]:[{$this->secret}]");
            $this->_logger->error("[sigSecret]:[{$this->sigSecret}]");
        }
        return null;
    }

    protected function getSessionToken()
    {
        try {
            $cache_key = $this->sessionTokenCacheKey;
            $sessionToken = $this->_cache->getCache($cache_key);
            if($sessionToken) {
                return $sessionToken;
            }

            $result = $this->authenticate();
            if($result) {
                $sessionToken = $result['session_token'];
                $expire_at = new \DateTime();
                $expire_at->setTimestamp($result['expire_at']);
                $current_date = new \DateTime();
                $diff_date = $current_date->diff($expire_at);

                $minutes = $diff_date->days * 24 * 60;
                $minutes += $diff_date->h * 60;
                $minutes += $diff_date->i;
                $second = ($diff_date->s + ($minutes * 60));
                // Time backup
                if ($second > 600)
                {
                    $second = $second - 600;
                }

                $this->_cache->setCache($cache_key, $sessionToken, $second);
            }
            return $sessionToken;
        }
        catch (\Exception $exc)
        {
            // Log
            $this->_logger->error("getSessionToken fail., " . $exc->getMessage());
        }

        return null;
    }

    private function getFinalUrlBy($url, $fields, $isPostJson = false) {

        $pathInfo = $this->genPathInfo($url);
        if($isPostJson) {
            $jsonString = json_encode($fields);
            $sig = $this->genSignatureJson($jsonString, $pathInfo);
        } else {
            $sig = $this->genSignature($fields, $pathInfo);
        }

        if( parse_url($url, PHP_URL_QUERY) ) {
            $finalUrl = $url . '&sig=' . $sig;
        } else {
            $finalUrl = $url . '?sig=' . $sig;

        }
        return $finalUrl;
    }

    protected function runApiByMethod($url, $method, $params) {
        $session_token = $this->getSessionToken();
        if(! $session_token) {
            $this->_logger->error("Authenticate is failure, session_token: " . $session_token);
            return array();
        }
        $params["session_token"] = $session_token;
        $finalUrl = $this->getFinalUrlBy($url, $params);
        $this->_logger->error("[url]:[{$finalUrl}]");
        $this->_logger->error("[params]:[". json_encode($params) ."]");
        try {
            $curl = new Curl($this->_container);
            $curl->setChannel($this->_channel);
            switch ($method) {
                case "GET":
                    $curl->setMethod(Curl::HTTP_GET);
                    break;
                case "POST":
                    $curl->setMethod(Curl::HTTP_POST);
                    break;
                case "PUT":
                    $curl->setMethod(Curl::HTTP_PUT);
                    break;
                default:
                    //default GET
                    $curl->setMethod(Curl::HTTP_GET);
                    break;
            }

            $res = $curl->call($finalUrl, $params);
            if ($res['status'] != 200) {
                $this->_logger->error("CALL FAIL with status #200");
                $this->_logger->error("[results]:[". json_encode($res) ."]");
            }
            return $res;
        } catch (\Exception $exc) {
            $this->_logger->error("CALL FAIL with exception, errorMessage:" . $exc->getMessage());
            $this->_logger->error($exc->getMessage());
        }
        return array();
    }





}
