<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class TableDBUriMapping
{
    const DETAILS = "promotions/{id}";
    const PROMOTION = "/promotion/getFilterData";
    //http://sg.tabledb.com/tabledb-web/promotion/search/0/0?perPage=5&displayInHGW=true&partnerCode%5B%5D=hgw&countryCode%5B%5D=SG&featuredDeal=1&sortField=createdDate&order=DESC
    const SEARCH_PROMOTION = "promotion/search/{latitude}/{longitude}";
}