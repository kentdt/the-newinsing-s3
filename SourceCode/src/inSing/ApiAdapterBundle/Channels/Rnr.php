<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiRnrAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class Rnr extends ApiRnrAdapter
{
    public function __construct($container, $cache, $channelLogger)
    {
        parent::__construct($container, $cache, $channelLogger, "rnr");
    }
    
    public function getItemListApi($params = array())
    {
        $url = $this->basicUrl . RnrUriMapping::RNR_GET_ITEM_LIST;
        $params = array();
        $res = $this->runApiByMethod($url, "GET", $params);
        return $this->runApiByMethod($url, "GET", $params);        
    }
}
