<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use inSing\DataSourceBundle\Lib\UtilHelper;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController extends Controller
{
    public function articleDetailsAction($slug, $hexId)
    {

        $id = UtilHelper::urlIdToId($hexId);
        //$id = '2574647';

        $article = $this->getArticleCMSContent($id);

        if (null == $article){
            throw new NotFoundHttpException('Page Not Found');
        }

        //generate breadscrumb
        $breadcrumbs = $this->get('insing.util.helper')->getBreadCrumb();
        //generate breadscrumb
        if(!empty($article['channel'])) {
            $breadcrumbs->addItem($article['channel']);
        }
        if(!empty($article['series_name'])) {
            $breadcrumbs->addItem($article['series_name']);
        }
        if(!empty($article['title'])){
            $breadcrumbs->addItem($article['title']);
        }

        $articlePrevNext = $this->getPrevNextArticleCMSContent($id, $article['series_name']);

        return $this->render('inSingFrontendBundle:Subpage:article_detail.html.twig', array(
            'article' => $article,
            'previous_article' => isset($articlePrevNext['previous'])? $articlePrevNext['previous']: array(),
            'next_article' => isset($articlePrevNext['next'])? $articlePrevNext['next']: array()
        ));

    }

    private function getArticleCMSContent($id)
    {
        $cache = $this->get('new.insing.cache');
        $key = $this->container->getParameter('new_insing_article_cache_key') . '_' . md5($id);
        $lifetime = $this->container->getParameter('new_insing_article_cache_time');

        $result = array();
        if ($cache->checkCache($key)) {
            $result = $cache->getCache($key);
        } else {
            $cms_api = $this->get('cms_api');
            $params = array(
                'article_id' => $id
            );
            $content = $cms_api->getArticleDetails($params);

            if (!empty($content['details'])) {
                $result = $content['details'];
                //TO-DO; return needed data for article details page
                $result = $this->parseArticle($result);
                $cache->setCache($result, $key, $lifetime);
            }

        }
        return $result;
    }

    private function parseArticle($result)
    {
        $result['description'] = (!empty($result['summary'])) ? $result['summary'] : '';
        $result['default_image'] = '';
        $result['caption'] = '';
        $result['article_content'] = array();
        $result['channel'] = $result['channel'];
        $result['series_description'] = (!empty($result['series']['0']['description'])) ? $result['series']['0']['description'] : '';
        $result['series_name'] = (!empty($result['series']['0']['series_name'])) ? $result['series']['0']['series_name'] : '';

        if (!empty($result['body'])) {
            usort($result['body'], function ($a, $b) {
                return $a['sort'] - $b['sort'];
            });

            $movideo_player = array();  // for Movideo

            foreach ($result['body'] as $key => $bodyContent) {
                switch ($bodyContent['module']) {
                    case 'default_image':
                        $result['default_image'] = $bodyContent['default_image'];

                        if (isset($bodyContent['caption'])) {
                            $result['caption'] = $bodyContent['caption'];
                        }
                        break;

                    case 'rich_text':
                        // Replace iFrame in RichText from BBCode => HTML Code
                        $articleRichText = UtilHelper::iframeCodeParser($bodyContent['rich_text']);
                        $result['body'][$key]['rich_text'] = $articleRichText;
                        break;

                    case 'video':
                        if ($bodyContent['provider'] == 'Movideo' && $bodyContent['video_id'] != '') {
                            $movideo_player[] = $bodyContent['video_id'];
                        }
                        break;

                    default:
                        $result[$bodyContent['module']] = $bodyContent;
                        break;
                }
            }

            // Set Movideo Player
            $result['movideo_player'] = $movideo_player;
        }

        return $result;
    }

    private function getPrevNextArticleCMSContent($id, $seriesName = null)
    {
        $cache = $this->get('new.insing.cache');
        $key = $this->container->getParameter('new_insing_article_cache_key') . '_prev-next_' . md5($id);
        $lifetime = $this->container->getParameter('new_insing_article_cache_time');

        $results = array();
        if ($cache->checkCache($key)) {
            $results =  $cache->getCache($key);
        } else {
            $host = 'http://'.$this->getRequest()->getHost();
            $cms_api = $this->get('cms_api');
            $params = array(
                'article_id' => $id,
                'tag' => $seriesName,
                'status' => 1
            );
            $content = $cms_api->getPrevNextArticles($params);

            if (!empty($content['results'])) {
                //get content of previous article
                $previous = isset($content['results']['previous']) ? $content['results']['previous'] : array();
                if (!empty($previous)) {
                    $contentUrl = $this->generateUrl('insing_frontend_article_details', array(
                        'slug' => UtilHelper::slug($previous['title']),
                        'hexId' => UtilHelper::idToUrlId($previous['id'])
                    ));

                    $previous = array(
                        'image_url' => $previous['image_url'],
                        'title' => $previous['title'],
                        'series_name' => (!empty($previous['series']['0']['series_name'])) ? $previous['series']['0']['series_name'] : '',
                        'content_url' => $contentUrl
                    );
                }

                //get content of next article
                $next = isset($content['results']['next']) ? $content['results']['next'] : null;
                if (!empty($next)) {
                    $contentUrl = $this->generateUrl('insing_frontend_article_details', array(
                        'slug' => UtilHelper::slug($next['title']),
                        'hexId' => UtilHelper::idToUrlId($next['id'])
                    ));

                    $next = array(
                        'image_url' => $next['image_url'],
                        'title' => $next['title'],
                        'series_name' => (!empty($next['series']['0']['series_name'])) ? $next['series']['0']['series_name'] : '',
                        'content_url' => $contentUrl
                    );
                }


                $results = array(
                    'previous' => $previous,
                    'next' => $next
                );
                $cache->setCache($results, $key, $lifetime);

            }
        }
        return $results;
    }
}