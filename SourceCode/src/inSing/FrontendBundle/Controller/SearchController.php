<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    public function eventAction()
    {
        $search_api = $this->get('search_api');
        $result = $search_api->getEvents();

        return new Response(var_dump($result));
        //return new Response(json_encode($result));
    }

    public function eventDetailsAction($ids)
    {
        $search_api = $this->get('search_api');
        $result = $search_api->getEventDetails($ids);

        return new Response(var_dump($result));
        //return new Response(json_encode($result));
    }

    public function eventPrevNextAction($id)
    {
        $search_api = $this->get('search_api');
        $result = $search_api->getEventPrevNext($id);

        return new Response(var_dump($result));
        //return new Response(json_encode($result));
    }

    public function businessDetailsAction()
    {
        $ids = '131713';
        $search_api = $this->get('search_api');
        $result = $search_api->getBusinessDetails($ids);

        return new Response(var_dump($result));
        //return new Response(json_encode($result));
    }
}
