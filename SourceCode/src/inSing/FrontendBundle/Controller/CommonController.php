<?php
namespace inSing\FrontendBundle\Controller;

use inSing\DataSourceBundle\Lib\UtilHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommonController extends Controller
{
    const ItemTypeFood = "food";
    public function renderOutbrainAction()
    {
        return $this->render('inSingFrontendBundle:_Partial:_outbrain.html.twig', array(
        ));
    }

    /**
     * @return Response
     * @author Cuong Au
     */
    public function renderFindActivityAction()
    {
        $channel = strtolower($this->getRequest()->get('channel'));

        $channels = array('events','movies','hungrygowhere');

        if(in_array($channel,$channels)){
            switch($channel){
                case 'events':
                    /*
                    * Get Event Category
                    * */
                    $search_box = $this->get('insing.search_box');

                    $event_category = $search_box->renderEvent_category();
                    break;
                case 'movies':
                    break;
                case 'hungryhowhere':
                    break;
            }
        }
        else
        {

        }
        return $this->render('inSingFrontendBundle:_Partial:_find_activity.html.twig', array(
            'channel'=>$channel,
            'event_category'=>isset($event_category['data'])? $event_category['data'] : "",
        ));
    }

    public function renderRestaurantNearByAction(Request $request)
    {
        $data = array();
        $search_api = $this->get('search_api');
        $params = array();
        $restaurant_url = $this->container->getParameter('insing_content_carousel_restaurant');
        $filter_distance = $this->container->getParameter('filter_distance_default');
        if(isset($filter_distance) && $filter_distance != "") {
            $params['filter_distance'] = $filter_distance;
        }
        //set long and lat
        $params['latitude']  =  $request->get('latitude');
        $params['longitude'] =  $request->get('longitude');

        //TEST
        //$params['latitude']  =  '1.283233';
        //$params['longitude'] =  '103.8433';

        // include rnr & properties
        $params['include_rnr'] =  "Y";
        $params['include_hgw'] =  "Y";
        $params['include_properties'] =  "Y";

        $params['filter_dining_reservation_url'] = "Y"; //TEST BOOK NOW

        $per_page = $this->container->getParameter('default_restaurant_near_by_items');
        $params['per_page'] =  $per_page;
        $data_search_api_v5 = $search_api->GetBusinessSearchV5($params);
        if (isset($data_search_api_v5) && $data_search_api_v5['status'] == 200 ) {
            $result = $data_search_api_v5['data']['results'];
        }
        foreach($result as $obj) {
            $itemIds[] = $obj['details']['id'];
        }
        $params_rnr['item_id'] = $itemIds;
        $params_rnr['item_type'] = self::ItemTypeFood;

        $rnr_api = $this->get('rnr_api');
        $itemList = $rnr_api->getItemListApi($params_rnr);
        $itemList = $itemList['items'];
        if (count($itemList)) {
            foreach ($itemList as $key => $item) {
                $itemList[$item['item_id']] = $item;
                unset($itemList[$key]);
            }
        }

        if (!empty($result)){
            foreach($result as $item) {
                $item = $item['details'];
                $common = $this->container->get('common.utils');
                $restaurant_image = $common->getRestaurantNearByImages($item, '', '100x70');
                $restaurant_item_list = array();
                if (array_key_exists($item['id'], $itemList)) {
                    $restaurant_item_list = $itemList[$item['id']];
                }

                if ($restaurant_image['image_url'] == '') {
                    if (isset($restaurant_item_list['latest_review_photo']) && $restaurant_item_list['latest_review_photo']) {
                        $url_media = $common->generatePhoto($restaurant_item_list['latest_review_photo'], '100x70');
                        $restaurant_image['image_url'] = $url_media;
                        $restaurant_image['original_image_url'] = $restaurant_item_list['latest_review_photo'];
                    }
                }

                $data[] = array(
                    'name' => $item['trading_name'],
                    'image' => $restaurant_image['image_url'],
                    'cuisines' => (!empty($item['properties']['cuisine']))? implode(' • ', $item['properties']['cuisine']): '',
                    'thumbs_up_percentage' => ($item['rnr']['thumbs_up_percentage'] > 0)? round($item['rnr']['thumbs_up_percentage']): '',
                    'slug' => $item['hgw']['hgw_url'],
                    'tabledb_id' => $item['tabledb']['restaurant_id'],
                    'restaurant_url' => ('' != $item['hgw']['hgw_url'])? str_replace('{restaurant_slug}', $item['hgw']['hgw_url'], $restaurant_url): '#'
                );
            }
        }
        return $this->render('inSingFrontendBundle:_Partial:_restaurant_near_by.html.twig', array(
            'restaurants' => $data
        ));
    }

    public function renderSubmitEventRightAction()
    {
        return $this->render('inSingFrontendBundle:_Partial:_submit_event_right.html.twig', array(
        ));
    }

    /**
     * @return Response
     * @author Cuong Au
     */
    public function renderSearchEventBoxAction($param=array())
    {

        $from_date_remove_year = "";
        $to_date_remove_year = "";
        $result =array();
        $result['term'] =isset($param['term']) ? $param['term'] : "";
        $result['from_date'] =isset($param['from_date']) ? $param['from_date'] : "";
        $result['to_date'] =isset($param['to_date']) ? $param['to_date'] : "";
        $result['date_remove'] = "";
        if($result['from_date']){
            $from_date_remove_year = date('d-M',strtotime($result['from_date']));
        }
        if($result['to_date']){
            $to_date_remove_year = date('d-M',strtotime($result['to_date']));
        }
        if($from_date_remove_year){
            $result['date_remove'] = $from_date_remove_year;
            if($to_date_remove_year){
                $result['date_remove'] .= "-" . $to_date_remove_year;
            }
        }
        else{
            if($to_date_remove_year){
                $result['date_remove'] = $to_date_remove_year;
            }
        }

        /*
           * Get Event Category
           * */
        $search_box = $this->get('insing.search_box');

        $event_category = $search_box->renderEvent_category();
        /**
         * Gen Category
         */
        if(isset($param['category'])){
            $array_category = explode(",",$param['category']);
            if(!empty($array_category) && is_array($array_category)){
                $result_array = array();
                foreach($array_category as $item){
                    $result_array[$event_category['data'][$item]['url_fragment']]=$event_category['data'][$item]['name'];
                }
                $result['category']=$result_array;
            }
        }
        else{
            $result['category']="";
        }
        return $this->render('inSingFrontendBundle:_Partial:_search_event_box.html.twig', array(
            'event_category' => $event_category['data'],
            'data_search'=>$result,
        ));
    }

    /*
     * @author: Vu Luu
     */
    public function renderLatestEventsNewsAction()
    {
        $data = array();
        $cache = $this->get('new.insing.cache');
        $key = $this->container->getParameter('new_insing_article_cache_key') . '_latest';
        $lifetime = $this->container->getParameter('new_insing_article_cache_time');
        $common = $this->container->get('common.utils');
        $imageSize = $this->container->getParameter('hp_hgw_module_image_size');

        if ($cache->checkCache($key)) {
            $data = $cache->getCache($key);
        } else {
            $cms_api = $this->get('cms_api');
            $params = array(
                'channel[]' => 'Events',
                'country' => 'SG',
                'sort_by' => 'last_updated_date',
                'page_size' => 3
            );
            $results = $cms_api->findContents($params);

            if (!empty($results['results'])) {
                $results = $results['results'];
                foreach ($results as $result) {
                    $defaultImage = '';
                    foreach ($result['body'] as $key => $bodyContent) {
                        if ('default_image' == $bodyContent['module']) {
                            $defaultImage = $bodyContent['default_image'];
                        }
                    }
                    $contentUrl = $this->generateUrl('insing_frontend_article_details', array(
                        'slug' => UtilHelper::slug($result['title']),
                        'hexId' => UtilHelper::idToUrlId($result['id'])
                    ));

                    $data[] = array(
                        'title' => $result['title'],
                        'summary' => $result['summary'],
                        'default_image' => $common->generatePhotoFromUrl($defaultImage, $imageSize, $imageSize),
                        'content_url' => $contentUrl
                    );
                }
                $cache->setCache($data, $key, $lifetime);
            }
        }

        return $this->render('inSingFrontendBundle:_Partial:_latest_events_news.html.twig', array(
            'data' => $data
        ));
    }
}
