<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DealController extends Controller
{
    public function listAction()
    {
        $request = $this->get('request');
        if ($request->isXmlHttpRequest()) {
            //$query = $request->query->all();
            $deals_api = $this->get('tabledb_deals_api');

            $params = array(
                'perPage' => 5,
                'displayInHGW' => 'true',
                'partnerCode[]' => 'hgw',
                'countryCode[]' => 'SG',
                'featuredDeal' => 1,
                'sortField' => 'createdDate',
                'order' => 'DESC'
            );
            $result = $deals_api->search('/0/0?'. http_build_query($params));
            $output = array();
            if(count($result) > 0) {
                foreach ($result as $value) {
                    $output[] = array(
                        'restaurant_id' => isset($value['promotion']['restaurantId'])? $value['promotion']['restaurantId']: '' ,
                        'title' => isset($value['promotion']['title'])? $value['promotion']['title']: '',
                        'description' => isset($value['promotion']['description'])? $value['promotion']['description']: '',
                        'image' => isset($value['promotion']['promotionImage'])? $value['promotion']['promotionImage']: ''
                    );
                }
            }
        } else {
            $output = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Invalid AJAX request.'
            );
        }

        return self::outputJSON($output, '');
    }

    public function detailsAction($ids)
    {
        $request = $this->get('request');
        if ($request->isXmlHttpRequest()) {
            //$query = $request->query->all();
            $deals_api = $this->get('tabledb_deals_api');
            $result = $deals_api->getDetails($ids);
            $output = array();
            if(count($result) > 0) {
                $output = array(
                    'restaurant_id' => $result['restaurantId'],
                    'title' => $result['title'],
                    'description' => $result['description'],
                    'image' => $result['promotionImage']
                );
            }
        } else {
            $output = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Invalid AJAX request.'
            );
        }

        return self::outputJSON($output, '');
    }

    /**
     * @editor Vu Luu
     */
    private function outputJSON($result, $channel)
    {
        if (isset($result['result']['channel'])) {
            if ($result['result']['channel'] == $channel) {
                $result['validate'] = 1;
            } else {
                $result['validate'] = 0;
            }
        }
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode(Response::HTTP_OK);
        $response->setContent(json_encode($result));

        return $response;
    }
}
