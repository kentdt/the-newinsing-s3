<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use inSing\DataSourceBundle\Lib\UtilHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GalleryController extends Controller
{
    /**
     * @param $slug
     * @param $hexId
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Cuong Au + Vu Luu
     */
    public function galleryDetailsAction(Request $request,$slug, $hexId)
    {
        $id = UtilHelper::urlIdToId($hexId);
        //$id = '15259263';
        $view = $request->attributes->get('view', 'thumb');
        $galleryDetail = $this->getGalleryCMSContent($id);

        if (null == $galleryDetail){
            throw new NotFoundHttpException('Page Not Found');
        }

        $breadcrumbs = $this->get('insing.util.helper')->getBreadCrumb();
        //generate breadscrumb
        if(!empty($galleryDetail['channel'])) {
            $breadcrumbs->addItem($galleryDetail['channel']);
        }
        if(!empty($galleryDetail['series_name'])) {
            $breadcrumbs->addItem($galleryDetail['series_name']);
        }
        if(!empty($galleryDetail['title'])){
            $breadcrumbs->addItem($galleryDetail['title']);
        }
        $galleryPrevNext = $this->getPrevNextGalleryCMSContent($id, $galleryDetail['series_name']);

        if ($view == 'slide') {
            $gallery_detail_all_html = $this->container->get('templating')->render('inSingFrontendBundle:Subpage:_gallery_slide.html.twig', array(
                'slug' => $slug,
                'hexId' => $hexId,
                'gallery' => $galleryDetail,
            ));
        } else {
            $gallery_detail_all_html = $this->container->get('templating')->render('inSingFrontendBundle:Subpage:_gallery_thumb.html.twig', array(
                'slug' => $slug,
                'hexId' => $hexId,
                'gallery' => $galleryDetail,
            ));
        }

        return $this->render('inSingFrontendBundle:Subpage:gallery_detail.html.twig', array(
            'gallery' => $galleryDetail,
            'gallery_detail_all_html' => $gallery_detail_all_html,
            'previous_gallery' => isset($galleryPrevNext['previous'])? $galleryPrevNext['previous']: array(),
            'next_gallery' => isset($galleryPrevNext['next'])? $galleryPrevNext['next']: array(),
            'view' => $view
        ));

    }

    private function getGalleryCMSContent($id)
    {
        $cache = $this->get('new.insing.cache');
        $key = $this->container->getParameter('new_insing_article_cache_key') . '_gallery_' . md5($id);
        $result = array();
        $cache->clearCache($key);
        if ($cache->checkCache($key)) {
            $result = $cache->getCache($key);
        } else {
            $cms_api = $this->get('cms_api');
            $params = array(
                'gallery_id' => $id,
                'status'=>1
            );
            $content = $cms_api->getGalleryDetails($params);
            if (!empty($content['details'])) {
                $result = $content['details'];
                $result = $this->parseGallery($result);

                $cache->setCache($result, $key);
            }
        }
        return $result;
    }

    private function parseGallery($result)
    {
        $result['series_name'] = (!empty($result['series']['0']['series_name'])) ? $result['series']['0']['series_name'] : '';
        $result['series_description'] = (!empty($result['series']['0']['description'])) ? $result['series']['0']['description'] : '';
        $result['title'] = (!empty($result['title'])) ? $result['title'] : '';
        $result['publish_date'] = (!empty($result['publish_date'])) ? $result['publish_date'] : '';
        $result['last_updated_date'] = (!empty($result['last_updated_date'])) ? $result['last_updated_date'] : '';
        $result['summary'] = (!empty($result['summary'])) ? $result['summary'] : '';

        $result['caption'] = '';
        $result['article_content'] = array();
        $result['channel'] = $result['channel'];

        return $result;
    }

    private function getPrevNextGalleryCMSContent($id, $seriesName = null)
    {
        $cache = $this->get('new.insing.cache');
        $key = $this->container->getParameter('new_insing_article_cache_key') . '_gallery_prev-next_' . md5($id);
        $lifetime = $this->container->getParameter('new_insing_article_cache_time');

        $results = array();
        $cache->clearCache($key);
        if ($cache->checkCache($key)) {
            $results =  $cache->getCache($key);
        } else {
            $host = 'http://'.$this->getRequest()->getHost();
            $cms_api = $this->get('cms_api');
            $params = array(
                'gallery_id' => $id,
                'tag' => $seriesName,
                'status' => 1
            );
            $content = $cms_api->getPrevNextGallery($params);

            if (!empty($content['results'])) {

                //get content of previous article
                $previous = isset($content['results']['previous']) ? $content['results']['previous'] : array();
                if (!empty($previous)) {
                    $contentUrl = $this->generateUrl('insing_frontend_gallery_slide', array(
                        'slug' => UtilHelper::slug($previous['title']),
                        'hexId' => UtilHelper::idToUrlId($previous['id'])
                    ));
                    $previous_image = '';
                    if (!empty($previous['images'])){
                        foreach ($previous['images'] as $image){
                            if (1 == $image['is_main']){
                                $previous_image = $image['image_url'];
                                break;
                            }
                        }
                        if ('' == $previous_image ){
                            $previous_image = $previous['images'][0]['image_url'];
                        }
                    }

                    $previous = array(
                        'image_url' => $previous_image,
                        'title' => $previous['title'],
                        'series_name' => (!empty($previous['series']['0']['series_name'])) ? $previous['series']['0']['series_name'] : '',
                        'content_url' => $contentUrl
                    );
                }

                //get content of next article
                $next = isset($content['results']['next']) ? $content['results']['next'] : null;
                if (!empty($next)) {
                    $contentUrl = $this->generateUrl('insing_frontend_gallery_slide', array(
                        'slug' => UtilHelper::slug($next['title']),
                        'hexId' => UtilHelper::idToUrlId($next['id'])
                    ));
                    $next_image = '';
                    if (!empty($next['images'])){
                        foreach ($next['images'] as $image){
                            if (1 == $image['is_main']){
                                $next_image = $image['image_url'];
                                break;
                            }
                        }
                        if ('' == $next_image ){
                            $next_image = $next['images'][0]['image_url'];
                        }
                    }

                    $next = array(
                        'image_url' => $next_image,
                        'title' => $next['title'],
                        'series_name' => (!empty($previous['series']['0']['series_name'])) ? $previous['series']['0']['series_name'] : '',
                        'content_url' => $contentUrl
                    );
                }


                $results = array(
                    'previous' => $previous,
                    'next' => $next
                );
                $cache->setCache($results, $key, $lifetime);

            }
        }
        return $results;
    }
}