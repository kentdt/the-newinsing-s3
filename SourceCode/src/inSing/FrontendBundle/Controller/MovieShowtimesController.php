<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MovieShowtimesController extends Controller
{
    public function indexAction()
    {
        return $this->render('inSingFrontendBundle:MovieShowtimes:index.html.twig', array(
                // ...
            ));    }

    public function searchResultAction()
    {
        return $this->render('inSingFrontendBundle:MovieShowtimes:searchResult.html.twig', array(
                // ...
            ));    }

}
