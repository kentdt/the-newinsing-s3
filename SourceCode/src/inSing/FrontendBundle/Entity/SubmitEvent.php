<?php
namespace inSing\FrontendBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class SubmitEvent {

    /**
     * @Assert\NotBlank(message = "You must first read and understand the Terms of Use of insing.com")
     */
    protected $termOfUse;
    /**
     * @Assert\NotBlank(message = "Full name is required.")
     */
    protected $fullName;
    protected $companyName;
    /**
     * @Assert\NotBlank(message = "Email address is required.")
     * @Assert\Email(message = "Email address is invaild.")
     */
    protected $emailAddress;
    /**
     * @Assert\NotBlank(message = "ContactNo is required.")
     * @Assert\Type(type="numeric", message="ContactNo is invalid.")
     */
    protected $primaryContactNo;
    /**
     * @Assert\NotBlank(message = "Title is required.")
     */
    protected $title;
    /**
     * @Assert\NotBlank(message = "Start date is required.")
     */
    protected $startDate;
    /**
     * @Assert\NotBlank(message = "End date is required.")
     */
    protected $endDate;
    protected $time;
    /**
     * @Assert\NotBlank(message = "Venue name is required.")
     */
    protected $venueName;
    /**
     * @Assert\NotBlank(message = "UnitNo is required.")
     */
    protected $unitNo;
    /**
     * @Assert\NotBlank(message = "Building name is required.")
     */
    protected $buildingName;
    protected $blockNo;
    protected $streetName;
    /**
     * @Assert\NotBlank(message = "Postal code is required.")
     */
    protected $postalCode;
    protected $venuePhoneNumber;
    /**
     * @Assert\NotBlank(message = "Event summary is required.")
     */
    protected $eventSummary;
    /**
     * @Assert\Url(message="Event url is invalid.")
     */
    protected $eventUrl;
    protected $priceMin;
    protected $priceMax;
    /**
     * @Assert\Url(message="Ticket url is invalid.")
     */
    protected $ticketUrl;
    protected $ticketVendorName;
    /**
     * @Assert\Url(message="Ticket vender image url is invalid.")
     */
    protected $ticketVendorImageUrl;
    protected $images;
    protected $galleryId;
    protected $subcategories;
    protected $adultRating;
    protected $facebookEventIds;

    public function __construct() {
        $this->subcategories = array();
    }

    public function getFullName() {
        return $this->fullName;
    }

    public function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    public function getCompanyName() {
        return $this->companyName;
    }

    public function setCompanyName($companyName) {
        $this->companyName = $companyName;
    }

    public function getEmailAddress() {
        return $this->emailAddress;
    }

    public function setEmailAddress($emailAddress) {
        $this->emailAddress = $emailAddress;
    }

    public function getPrimaryContactNo() {
        return $this->primaryContactNo;
    }

    public function setPrimaryContactNo($primaryContactNo) {
        $this->primaryContactNo = $primaryContactNo;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    public function getTime() {
        return $this->time;
    }

    public function setTime($time) {
        $this->time = $time;
    }

    public function getVenueName() {
        return $this->venueName;
    }

    public function setVenueName($venueName) {
        $this->venueName = $venueName;
    }

    public function setUnitNo($unitNo) {
        $this->unitNo = $unitNo;
    }

    public function getUnitNo() {
        return $this->unitNo;
    }

    public function setBuildingName($buildingName) {
        $this->buildingName = $buildingName;
    }

    public function getBuildingName() {
        return $this->buildingName;
    }

    public function setBlockNo($blockNo) {
        $this->blockNo = $blockNo;
    }

    public function getBlockNo() {
        return $this->blockNo;
    }

    public function setStreetName($streetName) {
        $this->streetName = $streetName;
    }

    public function getStreetName() {
        return $this->streetName;
    }

    public function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    public function getPostalCode() {
        return $this->postalCode;
    }

    public function getEventSummary() {
        return $this->eventSummary;
    }

    public function setEventSummary($eventSummary) {
        $this->eventSummary = $eventSummary;
    }

    public function getTicketUrl() {
        return $this->ticketUrl;
    }

    public function setTicketUrl($ticketUrl) {
        $this->ticketUrl = $ticketUrl;
    }

    public function getEventUrl() {
        return $this->eventUrl;
    }

    public function setEventUrl($eventUrl) {
        $this->eventUrl = $eventUrl;
    }

    public function getImages() {
        return $this->images;
    }

    public function setImages($images) {
        $this->images = $images;
    }

    public function setVenuePhoneNumber($venuePhoneNumber) {
        $this->venuePhoneNumber = $venuePhoneNumber;
    }

    public function getVenuePhoneNumber() {
        return $this->venuePhoneNumber;
    }

    public function setPriceMin($priceMin) {
        $this->priceMin = $priceMin;
    }

    public function getPriceMin() {
        return $this->priceMin;
    }

    public function setPriceMax($priceMax) {
        $this->priceMax = $priceMax;
    }

    public function getPriceMax() {
        return $this->priceMax;
    }

    public function setTicketVendorName($ticketVendorName) {
        $this->ticketVendorName = $ticketVendorName;
    }

    public function getTicketVendorName() {
        return $this->ticketVendorName;
    }

    public function setTicketVendorImageUrl($ticketVendorImageUrl) {
        $this->ticketVendorImageUrl = $ticketVendorImageUrl;
    }

    public function getTicketVendorImageUrl() {
        return $this->ticketVendorImageUrl;
    }

    public function setGalleryId($galleryId) {
        $this->galleryId = $galleryId;
    }

    public function getGalleryId() {
        return $this->galleryId;
    }

    public function setSubcategories($subcategories) {
        $this->subcategories = $subcategories;
    }

    public function getSubcategories() {
        return $this->subcategories;
    }

    public function setAdultRating($adultRating) {
        $this->adultRating = $adultRating;
    }

    public function getAdultRating() {
        return $this->adultRating;
    }

    public function setFacebookEventIds($facebookEventIds) {
        $this->facebookEventIds = $facebookEventIds;
    }

    public function getFacebookEventIds() {
        return $this->facebookEventIds;
    }

    /**
     * @return mixed
     */
    public function getTermOfUse()
    {
        return $this->termOfUse;
    }

    /**
     * @param mixed $termOfUse
     */
    public function setTermOfUse($termOfUse)
    {
        $this->termOfUse = $termOfUse;
    }



    public function toApiParams() {
        $params = array();
        $params['name'] = $this->title;
        $params['subcategories'] = '';
        if(count($this->getSubcategories()) > 0) {
            $params['subcategories'] = implode('|', $this->getSubcategories());
        }
        $params['description'] = $this->eventSummary;
        $params['website'] = $this->eventUrl;
        $params['adult_rating'] = $this->getAdultRating() ? 'Y' : 'N';

        //convert datetime from "dd-M-yy" to "Y-m-d"
        $raw = new \DateTime($this->startDate);
        $params['start_date'] = $raw->format('Y-m-d');

        $raw = new \DateTime($this->endDate);
        $params['end_date'] = $raw->format('Y-m-d');

        //convert datetime from "mm/dd/yyyy" to "Y-m-d"
        //$params['start_date'] = preg_replace('/(\d+)\/(\d+)\/(\d+)/', "$3-$1-$2", $this->startDate);
        //$params['end_date'] = preg_replace('/(\d+)\/(\d+)\/(\d+)/', "$3-$1-$2", $this->endDate);

        $params['venue_name'] = $this->venueName;
        $params['venue_unit_number'] = $this->unitNo;
        $params['venue_building_name'] = $this->buildingName;
        $params['venue_block_number'] = $this->blockNo;
        $params['venue_street_name'] = $this->streetName;
        $params['venue_postal_code'] = $this->postalCode;
        $params['venue_phone_number'] = $this->venuePhoneNumber;
        $params['price_min'] = $this->priceMin;
        $params['price_max'] = $this->priceMax;
        $params['organizer_name'] = $this->companyName;
        $params['organizer_contact_person'] = $this->fullName;
        $params['organizer_contact_number'] = $this->primaryContactNo;
        $params['organizer_contact_email'] = $this->emailAddress;
        $params['gallery_id'] = $this->galleryId;
        $params['facebook_event_ids'] = str_replace(',', '|', $this->getFacebookEventIds());
        $params['vendor_url'] = $this->ticketUrl;
        $params['vendor_image_url'] = $this->ticketVendorImageUrl;
        $params['vendor_name'] = $this->ticketVendorName;
        $params['event_timing'] = $this->time;
        $params['published'] = 'N';

        return $params;
    }
}