<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/18/15
 * Time: 4:20 PM
 */
// src/inSing/FrontendBundle/Command/ResetCacheHomePageCommand.php
namespace inSing\FrontendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
#use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
#use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ResetCacheHomePageCommand extends ContainerAwareCommand
{
    const CACHE_KEY_STATUS_CMD_RESET_CACHE_HP = 'STATUS_CMD_RESET_CACHE_HP';
    const CACHE_TIME_STATUS_CMD_RESET_CACHE_HP = 86400; #1 day

    protected function configure()
    {
        $this
            ->setName('homepage:resetCache')
            ->setDescription('Reset all cache in home page')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // #test command: php app/console homepage:resetCache --env=frontend_dev
        $logger = $this->getContainer()->get('monolog.logger.homepage');
        $cache = $this->getContainer()->get('new.insing.cache');
        $time_start = microtime(true);

        //write status in cache
        $cache->setCache('running', self::CACHE_KEY_STATUS_CMD_RESET_CACHE_HP, self::CACHE_TIME_STATUS_CMD_RESET_CACHE_HP , true);

        $carouselCacheKey = $this->getContainer()->getParameter('new_insing_hp_carousel_module_cache_key');
        $carouselCacheTime = $this->getContainer()->getParameter('new_insing_hp_carousel_module_cache_time');

        $hgwCacheKey = $this->getContainer()->getParameter('new_insing_hp_hgw_module_cache_key');
        $hgwCacheTime = $this->getContainer()->getParameter('new_insing_hp_hgw_module_cache_time');
        $hgwHidenCacheKey = $this->getContainer()->getParameter('new_insing_hp_hgw_module_hidden_cache_key');
        $hgwHidenCacheTime = $this->getContainer()->getParameter('new_insing_hp_hgw_module_hidden_cache_time');

        $prepareHomePageCache = $this->getContainer()->get('insing.prepare_hp_cache');
        //call data from service
        $dataCarousel = $prepareHomePageCache->getDataCarouselModule();
        $dataHgw = $prepareHomePageCache->getDataHgwModule();
        $isAllowDeleteCache = false;
        $allSetCached = array('carousel' => 'none', 'hgw' => 'none', 'movie' => 'none', 'event' => 'none');
        $dataModule = array('carousel' => 'current', 'hgw' => 'current', 'movie' => 'current', 'event' => 'current');

        if(! empty($dataCarousel)
            && ! empty($dataHgw) ) {
            //delete cache
            $isAllowDeleteCache = true;

            //carousel-module
            if(! empty($dataCarousel)) {
                $dataModule['carousel'] = 'updated';
            }
            //hgw-module
            if(! empty($dataHgw)) {
                $dataModule['hgw'] = 'updated';
            }
        }

        if($isAllowDeleteCache) {
            //override cache
            //carousel-module
            if(! empty($dataCarousel) ) {
                $cache->setCache($dataCarousel, $carouselCacheKey, $carouselCacheTime, true);
                $allSetCached['carousel'] = 'done';
            }

            //hgw-module
            if(! empty($dataHgw) && ! empty($dataHgw['left']) && ! empty($dataHgw['right2']) ) {
                $cache->setCache($dataHgw, $hgwCacheKey, $hgwCacheTime, true);
                $allSetCached['hgw'] = 'done';
            }
            //hiden
            if(! empty($dataHgw) && ! empty($dataHgw['right6'])) {
                $cache->setCache($dataHgw['right6'], $hgwHidenCacheKey, $hgwHidenCacheTime, true);
            }
        }

        // MOVIE
        $movieModuleCache = $this->getContainer()->get('insing.prepare_hp_cache');
        $movieModuleCache->cacheMovieModuleData(true);

        // EVENT
        $eventModuleCache = $this->getContainer()->get('insing.prepare_hp_cache');
        $eventModuleCache->cacheEventModuleData(true);

        $allSetCached['movie'] = 'done';
        $allSetCached['event'] = 'done';
        $dataModule['movie'] = 'updated';
        $dataModule['event'] = 'updated';


        //response
        if( $isAllowDeleteCache) {
            $logger->info('Executing command homepage:resetCache success.');
            $output->writeln('modules ' . json_encode($dataModule));
            $output->writeln('reset cache success. with ' . json_encode($allSetCached));
        } else {
            $logger->info('Executing command homepage:resetCache failed.');
            $output->writeln('modules ' . json_encode($dataModule));
            $output->writeln('reset cache failed. with ' . json_encode($allSetCached));
        }
        $logger->info('modules ' . json_encode($dataModule));

        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;

        //execution time of the script
        $logger->info('Total Execution Time:'.$execution_time.' Seconds');
        $output->writeln('Total Execution Time:'.$execution_time.' Seconds');

        //write status in cache
        $cache->setCache('done', self::CACHE_KEY_STATUS_CMD_RESET_CACHE_HP, self::CACHE_TIME_STATUS_CMD_RESET_CACHE_HP , true);

    }
}