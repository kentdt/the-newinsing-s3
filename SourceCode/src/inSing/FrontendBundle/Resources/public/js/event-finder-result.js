$selector = $('#events-list-container');
var page = 2;
var load = true;

$(document).ready(function() {
    //$('.ajax-loader').hide();

    $(window).scroll(function() {
        if(!$(window).data('infinite-scroll-async'))
        {
            if ($(window).scrollTop() + $(window).height() >= $selector.offset().top + $selector.height() - 100)
            {
                if (load == true)
                {
                    // Locks the function
                    $(window).data('infinite-scroll-async', true);

                    //load more events list content
                    $.ajax({
                        url: $('#event_finder_result_ajax_load_url').val(),
                        type: 'GET',
                        //async: false,
                        data: {
                            'page': page,
                            'apiParams': apiParams
                        },
                        beforeSend: function () {
                            $('.ajax-loader').show();
                        },
                        complete: function () {
                            $(window).data('infinite-scroll-async', false);
                            $('.ajax-loader').hide();
                        },
                        success: function (response) {

                            if (response != '')
                            {
                                $('#events-list-container').append(response);
                                page++;
                            }
                            else
                            {
                                load = false;
                            }
                        }
                    });
                }
            }
        }
    });
});