/**
 * Created by vuluu on 24/09/2015.
 */
$(document).ready(function() {
    $('body').on('click', 'a#restaurant-book-now', function () {
        // do something for dynamic element
        //slug, tabledbid
        var id = $('#frmBookNow_restaurant_id');
        var frm = $('#frmBookNow');
        var action = $('#frmBookNow_action').val();
        action = action.replace("{slug_restaurant_name}", $(this).data('slug'));
        frm.attr('action', action);
        id.val($(this).data('tabledb_id'));
        //console.log(frm.attr('action'));
        frm.submit();
        return false;
    });
});