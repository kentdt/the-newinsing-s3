<?php

namespace inSing\FrontendBundle\Utils;

use Symfony\Component\DependencyInjection\Container;
use inSing\DataSourceBundle\Utilities\Common;


class PrepareHpCache
{
    private $_logger;
    private $_cache;
    private $_container;
    private $_frontpageTwig;

    /**
     * @author Dat.Dao
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->_container = $container;
        $this->_cache = $this->_container->get('new.insing.cache');
        $this->_logger = $this->_container->get('monolog.logger.homepage');
        $this->_frontpageTwig = $this->_container->get('insing.frontent.twig.frontentpage_extension');
    }

    /**
     * @author Dat.Dao
     * @return array
     */
    public function getDataCarouselModule () {
        $time_start = microtime(true);
        $data = $this->_container->get('bind_data_hompage')->getDataForCarousel();
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('TOTAL getDataCarouselModule Total Execution Time:'.$execution_time.' Seconds');
        return $data;
    }

    /**
     * @author Dat.Dao
     * @return array
     */
    public function getDataHgwModule () {
        $time_start = microtime(true);
        $data = $this->_container->get('bind_data_hompage')->getDataForHgwModule();
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $this->_logger->info('TOTAL getDataHgwModule Total Execution Time:'.$execution_time.' Seconds');
        return $data;
    }

    /**
     * @author Dat.Dao
     * @return array
     */
    public function getDataHgwRight6Module () {
        //get cache
        $cacheKey = $this->_container->getParameter('new_insing_hp_hgw_module_cache_key');
        $cacheTime = $this->_container->getParameter('new_insing_hp_hgw_module_cache_time');
        $data = $this->_cache->getCache($cacheKey);
        if(! empty($data) &&  !empty($data['right6'])) {
            return $data['right6'];
        }
        //call api
        $data = $this->_container->get('bind_data_hompage')->getDataForHgwModule();
        //set cache
        if(! empty($data) && ! empty($data['left']) && ! empty($data['right2']) ) {
            $this->_cache->setCache($data, $cacheKey, $cacheTime, true);
            $this->_logger->debug("getDataHgwRight6Module: set data into cache with cacheKey=[{$cacheKey}], cacheTime=[{$cacheTime}]");
        }

        //set hp_hgw_module_hidden
        if(! empty($data) && ! empty($data['right6'])) {
            $cacheKeyHiden = $this->_container->getParameter('new_insing_hp_hgw_module_hidden_cache_key');
            $cacheTimeHiden = $this->_container->getParameter('new_insing_hp_hgw_module_hidden_cache_time');
            $this->_cache->setCache($data['right6'], $cacheKeyHiden, $cacheTimeHiden, true);
            $this->_logger->debug("getDataHgwRight6Module: set data into cache with cacheKey=[{$cacheKeyHiden}], cacheTime=[{$cacheTimeHiden}]");
        }
        return $data['right6'];
    }

    /**
     * Get data for movie module from api, then cache them
     *
     * @param bool|false $force Don't get data from cache if $force == true
     * false: call from web
     * true: call from command
     *
     * @return array|bool
     */
    public function cacheMovieModuleData($force = false)
    {
        $key = $this->_container->getParameter('new_insing_hp_movie_module_cache_key');
        $lifetime = $this->_container->getParameter('new_insing_hp_movie_module_cache_time');
        $hiddenKey = $this->_container->getParameter('new_insing_hp_movie_module_hidden_cache_key');
        $hiddenLifetime = $this->_container->getParameter('new_insing_hp_movie_module_hidden_cache_time');

        $movieModuleData = array();
        $movieModuleHiddenData = array();

        if($this->_cache->checkCache($key) && $force == false)
        {
            $movieModuleData = $this->_cache->getCache($key);
            $this->_logger->debug("renderMovieAction: get data from cache with cacheKey=[{$key}], cacheTime=[{$lifetime}]");
        }
        else
        {
            $carouselKey = $this->_container->getParameter('new_insing_hp_carousel_module_hidden_cache_key');

            if ($this->_cache->checkCache($carouselKey))
            {
                $carouselData = $this->_cache->getCache($carouselKey);
            }
            else
            {
                $carousel_api = $this->_container->get('new_insing_api');
                $carouselData = $carousel_api->getHomePageCarouselModule(array('use_cache' => 'N', 'filter' => 'published'));
            }

            $numOfCarouselData = count($carouselData);

            $movies_api = $this->_container->get('movies_api');

            // Data of movies
            $moviesData = $movies_api->search(array('use_cache' => 'N',
                'sort_fields' => 'screenings',
                'filter_showtimes' => 'now showing',
                'sort_reverse' => 'Y',
                'page' => 1,
                'per_page' => 5 + $numOfCarouselData
            ));

            if (!empty($moviesData) && $moviesData['status'] == 200)
            {
                //$carouselData = null;

                if (!empty($carouselData) && $carouselData['status'] == 200)
                {
                    $resultData = $this->_filterMovieDuplicateContent($moviesData['data'], $carouselData['data']/*, "Movie"*/);
                }
                else
                {
                    $resultData = $moviesData['data'];
                }

                for ($i = 0; $i < 5; $i ++)
                {
                    if (!empty($resultData[$i]))
                    {
                        $movieModuleData['movies'][] = $resultData[$i];
                    }
                }
            }
            else
            {
                $movieModuleData['movies'] = false;
                $this->_logger->debug("renderMovieAction: Get movies data fail with error {$moviesData['status']}, check API Adapter logs for more details");
            }

            // Article and Gallery for movies
            $newsData = $movies_api->getNews(array('use_cache' => 'N',
                'page' => 1,
                'per_page' => 7 + $numOfCarouselData
            ));

            if (!empty($newsData) && $newsData['status'] == 200)
            {
                $this->_logger->debug("renderMovieAction: Get new data success");

                $articleIds = array();
                $galleryIds = array();
                $movieIds = array();

                foreach ($newsData['data'] as $item)
                {
                    $itemUrl = $item['item_url'];
                    $itemUrl = str_replace("http://", "", $itemUrl);
                    $arrItemUrl = explode("/", $itemUrl);

                    // Item is article
                    if ($arrItemUrl[1] == 'feature')
                    {
                        $articleIds[] = $item['id'];
                    }
                    elseif ($arrItemUrl[1] == 'gallery') // Item is gallery
                    {
                        $galleryIds[] = $item['id'];
                    }
                }

                $cms_api = $this->_container->get('cms_api');

                // Retrieve and append article to news data
                if (!empty($articleIds))
                {
                    $strArticleIds = implode(",", $articleIds);

                    $articlesData = $cms_api->getMultipleArticleDetails(array(
                        'use_cache' => 'N',
                        'article_ids' => $strArticleIds
                    ));

                    // Get movie id from article details (CMS API)
                    if ($articlesData['result_code'] == 200)
                    {
                        if (!empty($articlesData['results']))
                        {
                            // Handle for cms api SHIT response, one article returned
                            if (is_array($articlesData['results']) && isset($articlesData['results']['id']))
                            {
                                foreach ($newsData['data'] as &$item)
                                {
                                    if ($articlesData['results']['id'] == $item['id'])
                                    {
                                        $arrMovieIds = explode(",", $articlesData['results']['tags_movie_ids']);

                                        if (count($arrMovieIds) == 1 && !empty($arrMovieIds[0]))
                                        {
                                            $item['movie_id'] = $arrMovieIds[0];
                                            $movieIds[] = $arrMovieIds[0];
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach ($articlesData['results'] as $article)
                                {
                                    foreach ($newsData['data'] as &$item)
                                    {
                                        if ($article['id'] == $item['id'])
                                        {
                                            $arrMovieIds = explode(",", $article['tags_movie_ids']);

                                            if (count($arrMovieIds) == 1 && !empty($arrMovieIds[0]))
                                            {
                                                $item['movie_id'] = $arrMovieIds[0];
                                                $movieIds[] = $arrMovieIds[0];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $this->_logger->debug("renderMovieAction: Get article data fail with error {$articlesData['result_code']}, check API Adapter logs for more details");
                    }
                }

                // Retrieve and append gallery to news data
                if (!empty($galleryIds))
                {
                    $strGalleryIds = implode(",", $galleryIds);

                    $galleriesData = $cms_api->getGalleryDetails(array(
                        'use_cache' => 'N',
                        'gallery_id' => $strGalleryIds
                    ));

                    // Get movie id from gallery details (CMS API)
                    if ($galleriesData['result_code'] == 200)
                    {
                        if (!empty($galleriesData['details']))
                        {
                            // Handle for cms api SHIT response, one gallery returned
                            if (is_array($galleriesData['details']) && isset($galleriesData['details']['id']))
                            {
                                foreach ($newsData['data'] as &$item)
                                {
                                    if ($galleriesData['details']['id'] == $item['id'])
                                    {
                                        if (!empty($galleriesData['details']['tags_movie_ids']))
                                        {
                                            $arrMovieIds = explode(",", $galleriesData['details']['tags_movie_ids']);

                                            if (count($arrMovieIds) == 1 && !empty($arrMovieIds[0]))
                                            {
                                                $item['movie_id'] = $arrMovieIds[0];
                                                $movieIds[] = $arrMovieIds[0];
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach ($galleriesData['details'] as $gallery)
                                {
                                    foreach ($newsData['data'] as &$item)
                                    {
                                        if ($gallery['id'] == $item['id'])
                                        {
                                            if (!empty($gallery['tags_movie_ids']))
                                            {
                                                $arrMovieIds = explode(",", $gallery['tags_movie_ids']);

                                                if (count($arrMovieIds) == 1 && !empty($arrMovieIds[0]))
                                                {
                                                    $item['movie_id'] = $arrMovieIds[0];
                                                    $movieIds[] = $arrMovieIds[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $this->_logger->debug("renderMovieAction: Get gallery data fail with error {$galleriesData['result_code']}, check API Adapter logs for more details");
                    }
                }

                // Retrieve and append showtime url to news data
                if (!empty($movieIds))
                {
                    $strMovieIds = implode("|", $movieIds);
                    $movieDetails = $movies_api->getDetails($strMovieIds);

                    if ($movieDetails['status'] == 200)
                    {
                        // Handle for movies api SHIT response, one movie returned
                        if (isset($movieDetails['data']['id']))
                        {
                            $movieId = $movieDetails['data']['id'];
                            $movieUrl = $movieDetails['data']['movie_url'];

                            foreach ($newsData['data'] as &$item)
                            {
                                if (isset($item['movie_id']) && $item['movie_id'] == $movieId)
                                {
                                    $item['showtime_url'] = $movieUrl . "showtimes/";
                                }
                            }
                        }
                        else // Many movies returned
                        {
                            foreach ($movieDetails['data'] as $movie)
                            {
                                foreach ($newsData['data'] as &$item)
                                {
                                    if (isset($item['movie_id']) && $item['movie_id'] == $movie['id'])
                                    {
                                        $item['showtime_url'] = $movie['movie_url'] . "showtimes/";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $this->_logger->debug("renderMovieAction: Get movie details data fail with error {$movieDetails['status']}, check API Adapter logs for more details");
                    }
                }
            }
            else
            {
                $movieModuleData['news'] = false;
                $movieModuleHiddenData['news'] = false;
                $this->_logger->debug("renderMovieAction: Get movie news data fail with error {$newsData['status']}, check API Adapter logs for more details");
            }

            if (!empty($newsData) && $newsData['status'] == 200)
            {
                if (!empty($carouselData) && $carouselData['status'] == 200)
                {
                    $resultData = $this->_filterMovieDuplicateContent($newsData['data'], $carouselData['data']/*, "MovieNews"*/);
                }
                else
                {
                    $resultData = $newsData['data'];
                }

                $imageSize = $this->_container->getParameter('hp_hgw_module_image_size');

                for ($i = 0; $i < 2; $i++)
                {
                    if (!empty($resultData[$i]))
                    {
                        // Get default image if the news don't have image
                        if (empty($resultData[$i]['default_image_url']))
                        {
                            $resultData[$i]['default_image_url'] = $this->_container->get('router')->getContext()->getBaseUrl() . "/bundles/insingfrontend/images/default_306x230.jpg";
                        }
                        else
                        {
                            $resultData[$i]['default_image_url'] = $this->_frontpageTwig->changeImageSizeCms($resultData[$i]['default_image_url'], $imageSize);
                        }

                        // Truncate summary with 300 characters
                        if (!empty($resultData[$i]['summary']))
                        {
                            $resultData[$i]['summary'] = Common::textTruncate($resultData[$i]['summary'], 300);
                        }

                        $movieModuleData['news'][] = $resultData[$i];
                    }
                }

                for ($i = 2; $i < 7; $i++)
                {
                    if (!empty($resultData[$i]))
                    {
                        // Get default image if the news don't have image
                        if (empty($resultData[$i]['default_image_url']))
                        {
                            $resultData[$i]['default_image_url'] = $this->_container->get('router')->getContext()->getBaseUrl() . "/bundles/insingfrontend/images/default_306x230.jpg";
                        }
                        else
                        {
                            $resultData[$i]['default_image_url'] = $this->_frontpageTwig->changeImageSizeCms($resultData[$i]['default_image_url'], $imageSize);
                        }

                        // Truncate summary with 300 characters
                        if (!empty($resultData[$i]['summary']))
                        {
                            $resultData[$i]['summary'] = Common::textTruncate($resultData[$i]['summary'], 300);
                        }

                        $movieModuleHiddenData['news'][] = $resultData[$i];
                    }
                }
            }

            // If calling from command, we will set cache when enough data
            if ($force == false ||
                (!empty($movieModuleData) && !empty($movieModuleData['movies']) && !empty($movieModuleData['news'])))
            {
                $this->_cache->setCache($movieModuleData, $key, $lifetime, true);
                $this->_logger->debug("renderMovieAction: set data into cache with cacheKey=[{$key}], cacheTime=[{$lifetime}]");
                $this->_cache->setCache($movieModuleHiddenData, $hiddenKey, $hiddenLifetime, true);
                $this->_logger->debug("renderMovieAction: set data into cache with cacheKey=[{$hiddenKey}], cacheTime=[{$hiddenLifetime}]");
            }
        }

        return $movieModuleData;
    }

    /**
     * Get data for event module from api, then cache them
     *
     * @param bool|false $force Don't get data from cache if $force == true
     * false: call from web
     * true: call from command
     *
     * @return array|bool
     */
    public function cacheEventModuleData($force = false)
    {
        $key = $this->_container->getParameter('new_insing_hp_event_module_cache_key');
        $lifetime = $this->_container->getParameter('new_insing_hp_event_module_cache_time');
        $hiddenKey = $this->_container->getParameter('new_insing_hp_event_module_hidden_cache_key');
        $hiddenLifetime = $this->_container->getParameter('new_insing_hp_event_module_hidden_cache_time');

        $eventModuleData = array();
        $eventModuleHiddenData = array();

        if($this->_cache->checkCache($key) && $force == false)
        {
            $eventModuleData = $this->_cache->getCache($key);
            $this->_logger->debug("renderEventAction: get data from cache with cacheKey=[{$key}], cacheTime=[{$lifetime}]");
        }
        else
        {
            $carouselKey = $this->_container->getParameter('new_insing_hp_carousel_module_hidden_cache_key');

            if ($this->_cache->checkCache($carouselKey))
            {
                $carouselData = $this->_cache->getCache($carouselKey);
            }
            else
            {
                $carousel_api = $this->_container->get('new_insing_api');
                $carouselData = $carousel_api->getHomePageCarouselModule(array('use_cache' => 'N', 'filter' => 'published'));
            }

            //$numOfCarouselData = count($carouselData);

            $carouselIds = $this->_getIdsFromCarousel($carouselData);

            $events_api = $this->_container->get('events_api');

            $topthingsData = $events_api->getTopThingsToDo(array('use_cache' => 'N',
                                                                    'page' => 1,
                                                                    //'per_page' => 5 + $numOfCarouselData
                                                                    //'per_page' => 5,
                                                                    'exclude_ids' => $carouselIds['event']
                                                                ));

            if (!empty($topthingsData) && $topthingsData['status'] == 200)
            {
                /*if (!empty($carouselData) && $carouselData['status'] == 200)
                {
                    $resultData = $this->_filterDuplicateContent($topthingsData['data'], $carouselData['data'], "Event");
                }
                else
                {
                    $resultData = $topthingsData['data'];
                }*/

                $resultData = $topthingsData['data'];

                for ($i = 0; $i < 5; $i ++)
                {
                    if (!empty($resultData[$i]))
                    {
                        $eventModuleData['topthings'][] = $resultData[$i];
                    }
                }

            }
            else
            {
                $eventModuleData['topthings'] = false;
                $this->_logger->debug("renderEventAction: Get topthingstodo data fail with error {$topthingsData['status']}, check API Adapter logs for more details");
            }

            $topthingsEventIds = $this->_getEventIdsFromTopthings($topthingsData);

            if (!empty($carouselIds['event']))
            {
                $excludeEventIds = $carouselIds['event'] . ',' . $topthingsEventIds;
            }
            else
            {
                $excludeEventIds = $topthingsEventIds;
            }

            $supermoduleData = $events_api->getSupermodule(array('use_cache' => 'N',
                                                                    'page' => 1,
                                                                    'per_page' => 8,
                                                                    'exclude_ids' => $excludeEventIds,
                                                                    'exclude_article_ids' => $carouselIds['article'],
                                                                    'exclude_gallery_ids' => $carouselIds['gallery']
                                                                ));

            if (!empty($supermoduleData) && $supermoduleData['status'] == 200)
            {
                /*if (!empty($eventModuleData['topthings']))
                {
                    $supermoduleData['data'] = $this->_filterDuplicateEventModuleLeftRight($eventModuleData['topthings'], $supermoduleData['data']);
                }*/

                /*if (!empty($carouselData) && $carouselData['status'] == 200)
                {
                    $resultData = $this->_filterDuplicateContent($supermoduleData['data'], $carouselData['data'], "EventSupermodule");
                }
                else
                {
                    $resultData = $supermoduleData['data'];
                }*/

                $resultData = $supermoduleData['data'];

                $imageSize = $this->_container->getParameter('hp_hgw_module_image_size');

                for ($i = 0; $i < 2; $i++)
                {
                    if (!empty($resultData[$i]))
                    {
                        // Get default image if event don't have image
                        if (empty($resultData[$i]['imageUrl']))
                        {
                            $resultData[$i]['imageUrl'] = $this->_container->get('router')->getContext()->getBaseUrl() . "/bundles/insingfrontend/images/default_306x230.jpg";
                        }
                        else
                        {
                            $resultData[$i]['imageUrl'] = $this->_frontpageTwig->changeImageSizeCms($resultData[$i]['imageUrl'], $imageSize);
                        }

                        // Truncate summary with 300 characters
                        if (!empty($resultData[$i]['summary']))
                        {
                            $resultData[$i]['summary'] = Common::textTruncate($resultData[$i]['summary'], 300);
                        }

                        $eventModuleData['supermodule'][] = $resultData[$i];
                    }
                }

                for ($i = 2; $i < 7; $i++)
                {
                    if (!empty($resultData[$i]))
                    {
                        // Get default image if event don't have image
                        if (empty($resultData[$i]['imageUrl']))
                        {
                            $resultData[$i]['imageUrl'] = $this->_container->get('router')->getContext()->getBaseUrl() . "/bundles/insingfrontend/images/default_306x230.jpg";
                        }
                        else
                        {
                            $resultData[$i]['imageUrl'] = $this->_frontpageTwig->changeImageSizeCms($resultData[$i]['imageUrl'], $imageSize);
                        }

                        // Truncate summary with 300 characters
                        if (!empty($resultData[$i]['summary']))
                        {
                            $resultData[$i]['summary'] = Common::textTruncate($resultData[$i]['summary'], 300);
                        }

                        $eventModuleHiddenData['supermodule'][] = $resultData[$i];
                    }
                }
            }
            else
            {
                $eventModuleData['supermodule'] = false;
                $eventModuleHiddenData['supermodule'] = false;
                $this->_logger->debug("renderEventAction: Get supermodule data fail with error {$supermoduleData['status']}, check API Adapter logs for more details");
            }

            // If calling from command, we will set cache when enough data
            if ($force == false ||
                (!empty($eventModuleData) && !empty($eventModuleData['topthings']) && !empty($eventModuleData['supermodule'])))
            {
                $this->_cache->setCache($eventModuleData, $key, $lifetime, true);
                $this->_logger->debug("renderEventAction: set data into cache with cacheKey=[{$key}], cacheTime=[{$lifetime}]");
                $this->_cache->setCache($eventModuleHiddenData, $hiddenKey, $hiddenLifetime, true);
                $this->_logger->debug("renderEventAction: set data into cache with cacheKey=[{$hiddenKey}], cacheTime=[{$hiddenLifetime}]");
            }
        }

        return $eventModuleData;
    }

    /**
     * Delete items which duplicate with carousel module
     *
     * @param $sourceData
     * @param $filterData
     * @param null $contentType (Event, Movie, MovieNews, EventSupermodule)
     * @return array
     */
    private function _filterMovieDuplicateContent($sourceData, $filterData/*, $contentType = null*/)
    {
        if (!empty($sourceData) && !empty($filterData))
        {
            $result = array();

            foreach ($sourceData as $source)
            {
                $contentType = "Movie";

                if (is_array($source) && !empty($source['item_url']))
                {
                    $itemUrl = $source['item_url'];
                    $itemUrl = str_replace("http://", "", $itemUrl);
                    $arrItemUrl = explode("/", $itemUrl);

                    // Item is article
                    if ($arrItemUrl[1] == 'feature')
                    {
                        $contentType = "Article";
                    }
                    elseif ($arrItemUrl[1] == 'gallery') // Item is gallery
                    {
                        $contentType = "Gallery";
                    }
                    else
                    {
                        $contentType = "Movie";
                    }
                }

                $bDuplicate = false;

                foreach ($filterData as $filter)
                {
                    if ($filter['content_type'] == $contentType && $source['id'] == $filter['item_id'])
                    {
                        $bDuplicate = true;
                        break;
                    }
                }

                if (!$bDuplicate)
                {
                    $result[] = $source;
                }
            }

            return $result;
        }

        return $sourceData;
    }

    /**
     * Get event, article, gallery id list from carousel data
     *
     * @param $carouselData
     *
     * @return String id list
     */
    private function _getIdsFromCarousel($carouselData)
    {
        $arrIds = array(
            'event' => '',
            'article' => '',
            'gallery' => ''
        );

        if (!empty($carouselData) && $carouselData['status'] == 200)
        {
            foreach ($carouselData['data'] as $carousel)
            {
                if ($carousel['content_type'] == 'Event')
                {
                    $arrIds['event'] .= ($carousel['item_id'] . ',');
                }
                elseif ($carousel['content_type'] == 'Article')
                {
                    $arrIds['article'] .= ($carousel['item_id'] . ',');
                }
                elseif ($carousel['content_type'] == 'Gallery')
                {
                    $arrIds['gallery'] .= ($carousel['item_id'] . ',');
                }
            }
        }

        $arrIds['event'] = rtrim($arrIds['event'], ',');
        $arrIds['article'] = rtrim($arrIds['article'], ',');
        $arrIds['gallery'] = rtrim($arrIds['gallery'], ',');

        return $arrIds;
    }

    /**
     * Get event id list from "top thing to do" data
     *
     * @param $topthingsData
     *
     * @return String id list
     */
    private function _getEventIdsFromTopthings($topthingsData)
    {
        $eventIds = '';

        if (!empty($topthingsData) && $topthingsData['status'] == 200)
        {
            foreach ($topthingsData['data'] as $topthings)
            {
                if ($topthings['type'] == 'Event')
                {
                    $eventIds .= ($topthings['event_data'][0]['id'] . ',');
                }
            }
        }

        $eventIds = rtrim($eventIds, ',');

        return $eventIds;
    }

    /**
     * Delete supermodule items which duplicate with topthingtodo items
     *
     * @param $topthings
     * @param $supermodule
     */
    private function _filterDuplicateEventModuleLeftRight($topthings, $supermodule)
    {
        if (!empty($topthings) && !empty($supermodule))
        {
            $result = array();

            foreach ($supermodule as $source)
            {
                $bDuplicate = false;

                foreach ($topthings as $destination)
                {
                    if ($source['type'] == 'Event' && $destination['id'] == $source['id'])
                    {
                        $bDuplicate = true;
                        break;
                    }
                }

                if (!$bDuplicate)
                {
                    $result[] = $source;
                }
            }

            return $result;
        }

        return $supermodule;
    }
}