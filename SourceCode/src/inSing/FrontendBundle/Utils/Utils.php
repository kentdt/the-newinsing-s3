<?php

namespace inSing\FrontendBundle\Utils;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\FileLocator;
class Utils extends ContainerAwareCommand
{
    public static function toReverseHex($number)
    {
        $hex = str_pad(dechex($number), 8, '0', STR_PAD_LEFT);
        return substr($hex, 6, 2) . substr($hex, 4, 2) . substr($hex, 2, 2) . substr($hex, 0, 2);
    }

    public static function getId($reversedHexId)
    {
        $hexId = substr($reversedHexId, 6, 2) . substr($reversedHexId, 4, 2) . substr($reversedHexId, 2, 2) . substr($reversedHexId, 0, 2);
        return hexdec($hexId);
    }

    public static function removeUrlParams($url)
    {
        $url = explode('?', $url);
        return $url[0];
    }

    public static function cleanUrl($url)
    {
        $url = str_replace(array("ä", "Ä"), "a", $url); // Additional Swedish filter
        $url = str_replace(array("å", "Å"), "a", $url); // Additional Swedish filter
        $url = str_replace(array("ö", "Ö"), "o", $url); // Additional Swedish filter

        $url = preg_replace("/[^a-z0-9\s\-]/i", "", $url); // Remove special characters
        $url = preg_replace("/\s\s+/", " ", $url); // Replace multiple spaces with one space
        $url = trim($url); // Remove trailing spaces
        $url = preg_replace("/\s/", "-", $url); // Replace all spaces with hyphens
        $url = preg_replace("/\-\-+/", "-", $url); // Replace multiple hyphens with one hyphen
        $url = preg_replace("/^\-|\-$/", "", $url); // Remove leading and trailing hyphens
        $url = strtolower($url);

        return $url;
    }

    public static function generateImageUrl($resourceUrl, $urlPrefix = '', $size = '406x305')
    {
        if ($resourceUrl && strpos($resourceUrl, 'http') === false) {
            return 'http://staticc0' . rand (1, 5) . '.' . $urlPrefix .
            'insing.com/images/' . $resourceUrl . '/pc_' . $size . '.jpg';
        }

        return $resourceUrl;
    }

    public static function isNotBlankString($str)
    {
        return (isset($str) && trim($str) != '');
    }
}
