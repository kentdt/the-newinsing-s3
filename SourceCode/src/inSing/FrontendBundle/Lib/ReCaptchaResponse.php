<?php

namespace inSing\FrontendBundle\Lib;

/**
 * A ReCaptchaResponse is returned from recaptcha_check_answer()
 */
class ReCaptchaResponse
{
    var $is_valid;
    var $error;
}
