<?php

namespace inSing\UtilBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 * 
 * @author Trung Nguyen
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('insing_util');
        
        $rootNode
            ->children()
                ->arrayNode('cache')
//                     ->info('Cache configuration')
                    ->children()
                        ->arrayNode('type')
                            ->prototype('scalar')->end()
                            ->defaultValue(array('apc'))
                            ->validate()
                                ->ifTrue(function($v) {
                                    // the supported cache systems
                                    $cacheType = array('apc', 'memcache', 'filecache');
                                    $flag = false;
                                    
                                    foreach ($v as $item) {                                        
                                        if (!in_array($item, $cacheType)) {
                                            $flag = true;
                                            break;
                                        }
                                    }
                                    return $flag;
                                })
                                ->thenInvalid('This cache type has not supported yet')
                            ->end()
//                             ->info('The supported cache types. Default is [apc]')
//                             ->example('type: [ apc, memcache ]')                            
                        ->end()
                        ->scalarNode('lifetime')
                            ->defaultValue(3600)
//                             ->info('The duration in seconds cache item is available. Default is 3600s')
                        ->end()
                        ->scalarNode('namespace')
                            ->defaultValue('')
//                             ->info('The namespace is used to prefix all cache entries')
                        ->end()
                        ->arrayNode('memcache')
                            ->prototype('scalar')->end()
                            ->defaultValue(array())
//                             ->info('The memcache list')
//                             ->example('memcache: [127.0.0.1:11211, 127.0.0.1:11212]')
                        ->end()
                        ->scalarNode('path')
                            ->defaultValue('%kernel.cache_dir%')
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('log')
//                     ->info('Logger configuration')
                    ->children()
                        ->arrayNode('type')
                            ->prototype('scalar')->end()
                            ->defaultValue(array('file'))
                            ->validate()
                                ->ifTrue(function($v) {
                                    // the supported log types
                                    $logType = array('file', 'db');
                                    $flag = false;
                                
                                    foreach ($v as $item) {
                                        if (!in_array($item, $logType)) {
                                            $flag = true;
                                            break;
                                        }
                                    }
                                    return $flag;
                                })
                                ->thenInvalid('This log type has not supported yet')
                            ->end()
//                             ->info('The supported log types. Default is [file]')
//                             ->example('type: [file,db]')
                        ->end()                          
                        ->arrayNode('file')
                            ->children()
                                ->scalarNode('size')
                                    ->defaultValue(4)
//                                     ->info('The size of log file. Default is 4Mb')
                                ->end()
                                ->scalarNode('path')
                                    ->defaultValue('%kernel.logs_dir%')
//                                     ->info('The path of log file. Default is %kernel.logs_dir%')
//                                     ->example('path: /home/neo/logs')
                                ->end()
                                ->scalarNode('name')
                                    ->defaultValue('common.log')
//                                     ->info('The name of log file. Default is "common.log"')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('db')
                            ->children()
                                ->scalarNode('connection')
                                    ->cannotBeEmpty()
//                                     ->info('DB connection name')
//                                     ->example('connection: doctrine.dbal.rnr_connection')
                                ->end()
                                ->scalarNode('table')
//                                     ->info('The table name. Default is "log"')
                                    ->defaultValue('log')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('gearman')
                    ->children()
                        ->arrayNode('server')
                            ->prototype('scalar')->end()
                            ->defaultValue(array())
                        ->end()
                        ->scalarNode('timeout')
                            ->defaultValue(10)
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('db')
                    ->children()
                        ->arrayNode('master')
                            ->prototype('scalar')->end()
                            ->defaultValue(array())
                        ->end()
                        ->arrayNode('slave1')
                            ->prototype('scalar')->end()
                            ->defaultValue(array())
                        ->end()
                        ->arrayNode('slave2')
                            ->prototype('scalar')->end()
                            ->defaultValue(array())
                        ->end()
                        ->arrayNode('slave3')
                            ->prototype('scalar')->end()
                            ->defaultValue(array())
                        ->end()
                        ->scalarNode('debug_mode')
                            ->defaultValue(false)
                        ->end()
                    ->end()
                ->end()
            ->end();
        
        return $treeBuilder;
    }
}
