<?php

namespace inSing\UtilBundle\Cache;

/**
 * Base class for cache implementations.
 * 
 * @author vu.tran
 */
abstract class AbstractCache implements CacheInterface
{
    /** @var string The namespace to prefix all cache ids with */
    private $_namespace = '';
    
    /**
     * @var int
     */
    private $_cache_lifetime = null;
    
    /**
     * Set the namespace to prefix all cache ids with.
     *
     * @param string $namespace
     * @return void
     */
    public function setNamespace($namespace)
    {
        $this->_namespace = (string) $namespace;
    }

    /**
     * {@inheritdoc}
     */
    public function getCache($id)
    {
        return $this->_doGetCache( $this->_getNamespacedId($id) );
    }

    /**
     * {@inheritdoc}
     */
    public function hasCache($id)
    {
        return $this->_doHasCache( $this->_getNamespacedId($id) );
    }

    /**
     * {@inheritdoc}
     */
    public function setCache($id, $data, $lifeTime = 0)
    {
        if ($lifeTime == 0 && !is_null($this->_cache_lifetime)) {
            $lifeTime = $this->_cache_lifetime;
        }
        
        return $this->_doSetCache( $this->_getNamespacedId($id), $data, $lifeTime );
    }

    /**
     * {@inheritdoc}
     */
    public function deleteCache($id)
    {
        $id = $this->_getNamespacedId($id);

        if (strpos($id, '*') !== false) {
            return $this->deleteByRegex('/' . str_replace('*', '.*', $id) . '/');
        }

        return $this->_doDeleteCache($id);
    }

    /**
     * Delete all cache entries.
     *
     * @return array $deleted  Array of the deleted cache ids
     */
    public function deleteAll()
    {
        $ids = $this->getIds();

        foreach ($ids as $id) {
            $this->_doDeleteCache($id);
        }

        return $ids;
    }

    /**
     * Delete cache entries where the id matches a PHP regular expressions
     *
     * @param string $regex
     * @return array $deleted  Array of the deleted cache ids
     */
    public function deleteByRegex($regex)
    {
        $ids     = $this->getIds();
        $deleted = array();

        foreach ($ids as $id) {
            if (preg_match($regex, $id)) {
                $this->_doDeleteCache($id);
                $deleted[] = $id;
            }
        }

        return $deleted;
    }

    /**
     * Delete cache entries where the id has the passed prefix
     *
     * @param string $prefix
     * @return array $deleted Array of the deleted cache ids
     */
    public function deleteByPrefix($prefix)
    {
        $prefix  = $this->_getNamespacedId($prefix);
        $ids     = $this->getIds();
        $deleted = array();

        foreach ($ids as $id) {
            if (strpos($id, $prefix) === 0) {
                $this->_doDeleteCache($id);
                $deleted[] = $id;
            }
        }

        return $deleted;
    }

    /**
     * Delete cache entries where the id has the passed suffix
     *
     * @param string $suffix
     * @return array $deleted Array of the deleted cache ids
     */
    public function deleteBySuffix($suffix)
    {
        $ids     = $this->getIds();
        $deleted = array();

        foreach ($ids as $id) {
            if (substr($id, -1 * strlen($suffix)) === $suffix) {
                $this->_doDeleteCache($id);
                $deleted[] = $id;
            }
        }

        return $deleted;
    }

    /**
     * Prefix the passed id with the configured namespace value
     *
     * @param string $id  The id to namespace
     * @return string $id The namespaced id
     */
    private function _getNamespacedId($id)
    {
        return $this->_namespace . $id;
    }

    /**
     * Fetches an entry from the cache.
     *
     * @param string $id cache id The id of the cache entry to fetch.
     * @return string The cached data or FALSE, if no cache entry exists for the given id.
     */
    abstract protected function _doGetCache($id);

    /**
     * Test if an entry exists in the cache.
     *
     * @param string $id cache id The cache id of the entry to check for.
     * @return boolean TRUE if a cache entry exists for the given cache id, FALSE otherwise.
     */
    abstract protected function _doHasCache($id);

    /**
     * Puts data into the cache.
     *
     * @param string $id The cache id.
     * @param string $data The cache entry/data.
     * @param int $lifeTime The lifetime. If != false, sets a specific lifetime for this cache entry (null => infinite lifeTime).
     * @return boolean TRUE if the entry was successfully stored in the cache, FALSE otherwise.
     */
    abstract protected function _doSetCache($id, $data, $lifeTime = false);

    /**
     * Deletes a cache entry.
     *
     * @param string $id cache id
     * @return boolean TRUE if the cache entry was successfully deleted, FALSE otherwise.
     */
    abstract protected function _doDeleteCache($id);

    /**
     * Get an array of all the cache ids stored
     *
     * @return array $ids
     */
    abstract public function getIds();
    
    /**
     * @param unknown $lifeTime
     */
    public function setCacheLifetime($lifeTime) 
    {    
        $this->_cache_lifetime = $lifeTime;
    }
    
    //=====================================================================================================
    // These are old methods will be not used in future. However, we still keep them here for
    // backward compatible.
    //=====================================================================================================
    /**
     * @var string
     */
    private $_prefix = null;
    
    /**
     * Create Name for Cache base on input params array
     *
     * @author Vu Tran (re-edited by Trung Nguyen)
     * @param  array/string $param
     * @return string
     */
    public function createCacheName($param)
    {
        $tmp = array();
    
        // slutify strings closure
        $slugify = function($text) {
            // replace non letter or digits by -
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    
            // trim
            $text = trim($text, '-');
    
            // transliterate
            if (function_exists('iconv')) {
                $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            }
    
            // lowercase
            $text = strtolower($text);
    
            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);
    
            // default output
            if (empty($text)) {
                return 'n-a';
            }
    
            return $text;
        };
    
        // array filter closure
        $callback = function($value, $key) use(&$tmp, $slugify) {
            if (trim($value) != '') {
                $tmp[] = $slugify($key).'_'.$slugify($value);
            }
        };
    
        if (is_array($param)) {
            array_walk_recursive($param, $callback);
            return $this->_prefix.'_'.implode('_', $tmp);
        } else {
            return $this->_prefix.'_'.$slugify($param);
        }
    }
    
    /**
     * @param unknown $lifeTime
     */
    public function setPrefix($val)
    {
        $this->_prefix = $val;
    }
}