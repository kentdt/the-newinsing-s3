<?php

namespace inSing\UtilBundle\Cache;

/**
 * Memcache cache driver.
 * 
 * @author vu.tran
 */
class MemcacheCache extends AbstractCache
{
    /**
     * @var Memcache/Memcached
     */
    private $_memcache;
    
    /**
     * Constructor
     * 
     * @author Trung Nguyen
     */
    public function __construct($memcache)
    {
        $this->_memcache = $memcache;
    }
    
    /**
     * Sets the memcache instance to use.
     * 
     * @author Trung Nguyen
     * @param  Memcache $memcache
     */
    public function setMemcache($memcache)
    {
        $this->_memcache = $memcache;
    }

    /**
     * Gets the memcache instance used by the cache.
     *
     * @return Memcache
     */
    public function getMemcache()
    {
        return $this->_memcache;
    }

    /**
     * {@inheritdoc}
     */
    public function getIds()
    {
        $keys = array();
        $allSlabs = $this->_memcache->getExtendedStats('slabs');

        foreach ($allSlabs as $server => $slabs) {
            if (is_array($slabs)) {
                foreach (array_keys($slabs) as $slabId) {
                    $dump = $this->_memcache->getExtendedStats('cachedump', (int) $slabId);

                    if ($dump) {
                        foreach ($dump as $entries) {
                            if ($entries) {
                                $keys = array_merge($keys, array_keys($entries));
                            }
                        }
                    }
                }
            }
        }
        
        return $keys;
    }

    /**
     * {@inheritdoc}
     */
    protected function _doGetCache($id)
    {
        return $this->_memcache->get($id);
    }

    /**
     * {@inheritdoc}
     */
    protected function _doHasCache($id)
    {
        return (bool) $this->_memcache->get($id);
    }

    /**
     * {@inheritdoc}
     */
    protected function _doSetCache($id, $data, $lifeTime = 0)
    {    
        return $this->_memcache->set($id, $data, 0, (int) $lifeTime);
    }

    /**
     * {@inheritdoc}
     */
    protected function _doDeleteCache($id)
    {
        return $this->_memcache->delete($id);
    }
}