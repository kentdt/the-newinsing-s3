<?php 

namespace inSing\DataSourceBundle\Model;

use inSing\DataSourceBundle\Lib\Pimple;

abstract class DataSource
{
    /**
     * @var string
     */
    protected $pid;

    /**
     * @var array
     */
    protected $methods = array();
    
    /**
     * @var Pimple
     */
    private $container;
    
    /**
     * Constructor
     * 
     * @param Pimple $container
     */
    public function __construct(Pimple $container)
    {
        $this->container = $container;
        
        $this->methods = $this->register();
        $this->init();
    }
    
    /**
     * List out methods should be monitored by monitor system
     * Note: Dev should implement it himself ;)
     * 
     * @author Trung Nguyen
     * @return void
     */
    abstract protected function register();
    
    /**
     * Implement anything if need
     * Note: Dev should implement it himself ;)
     *
     * @author Trung Nguyen
     * @return void
     */
    abstract protected function init();
    
    /**
     * Set process ID
     * 
     * @param string $id
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }
    
    /**
     * Set methods list
     *
     * @param array $methods
     * @return array
     */
    public function setMethods(array $methods)
    {
        $this->methods = $methods;
    }
    
    /**
     * Get methods list
     * 
     * @return array
     */
    public function getMethods()
    {
        return $this->methods;
    }
    
    /**
     * Get service by ID
     * 
     * @param string $id
     * @throws \InvalidArgumentException
     */
    protected function get($id)
    {
        return $this->container->get($id);
    }
}