<?php
namespace inSing\DataSourceBundle\Utilities\ApiContent;

/**
 * Interface for Call Api
 * @author Lap To <lap.to@s3corp.com.vn>
 */
interface ApiContentInterface
{
    function getContent($params);
}
