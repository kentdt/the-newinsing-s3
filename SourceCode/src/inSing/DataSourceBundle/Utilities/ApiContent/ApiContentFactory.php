<?php
namespace inSing\DataSourceBundle\Utilities\ApiContent;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A factory to get item type Object
 * @author Lap To
 */
class ApiContentFactory
{    
    const ROOT_CLASS_PATH = 'inSing\DataSourceBundle\Utilities\ApiContent\\';
  
    /**
     * @var Controller
     */
    protected $controller;
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     * 
     * @param Controller $controller
     * @param ContainerInterface $container
     * @param Int $itemId
     * @param String $transCodeName
     */
    public function __construct(Controller $controller, ContainerInterface $container) {
        $this->controller = $controller;
        $this->container = $container;
    }
    
    /**
     * Get item type Object
     * 
     * @param String $className
     * @return ApiInterface
     */
    public function get($className) {
        $className = self::ROOT_CLASS_PATH.$className;

        if (class_exists($className)) {
            // initialize object
            $obj = new $className();
            $obj->controller = $this->controller;
            $obj->container = $this->container;
        }

        return isset($obj) ? $obj : false;
    }
}
