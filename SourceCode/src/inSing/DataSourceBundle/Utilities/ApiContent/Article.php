<?php

namespace inSing\DataSourceBundle\Utilities\ApiContent;

use inSing\DataSourceBundle\Utilities\ApiContent\ApiContentAbstract;
use inSing\DataSourceBundle\Utilities\Common;
use inSing\DataSourceBundle\Utilities\Constant;
use inSing\DataSourceBundle\Repository\TemplateModuleRepository as TplModuleRepo;

class Article extends ApiContentAbstract {

    /**
     * @var Array
     */
    protected $result = array(
        'result_code' => 0,
        'details' => array(
            'item_id' => '',
            'default_image' => '',
            'title' => '',
            'story_url' => '',
            'series_name' => '',
            'series_url' => '',
            'series_slug' => '',
            'description' => '',
            'tags_business_ids' => '',
            'other_image' => '',
	    'status' => '',
        )
    );

    /**
     * @var String
     */
    private $routeSerie;

    /**
     * @var String
     */
    private $routeStory;

    /**
     * @var String
     */
    private $routeStoryRecipe;

    public function __construct() {
        parent::__construct();
        $this->routeSerie = 'in_sing_admin_cms_series';
        $this->routeStory = 'in_sing_admin_article_details';
        $this->routeStoryRecipe = 'in_sing_admin_recipe_articles';
    }

    /**
     * Return api result
     *
     * @param Array $param Include itemId, transCodeName
     * @return Array
     */
    public function getContent($param) {
        // get article from cms api
        if (!isset($param['item_id']) || empty($param['item_id']) ||
                !isset($param['module_type']) && !empty($param['module_type']) ) {
            throw new \Exception('Invalid variable');
        }
        $itemId = $param['item_id'];
        $selectedCountry = $this->controller->getSelectedCountry();
        $transCodeName = $this->getTransCodeName($param['module_type']);
        //for picking larger picture to crop
        $largerTransCode = $this->getLargerTransCode($param['module_type']);

        $apiResult = $this->container->get('cms.api')->getArticleDetailsById($itemId, 0, null, null, $selectedCountry, false);
		
        $frontEndUrl = $this->container->getParameter('frontend_url');
        $frontEndUrl = trim($frontEndUrl[$selectedCountry], '/');
        $result = $this->result['details'];
        if (isset($apiResult['result_code']) && $apiResult['result_code'] == 200) {
            $this->result['result_code'] = 200;
            $data = $apiResult['details'];
            $result['item_id'] = $data['id'];
            $result['title'] = strip_tags($data['title']);
            //Resize Image
            if (is_array($transCodeName) && !empty($transCodeName)) {
                $firstTransCode = array_shift($transCodeName);
                $result['default_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($data['default_image'], $largerTransCode, $firstTransCode);
                $otherImage = array();
                foreach ($transCodeName as $trans) {
                    $tmp = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($data['default_image'], $largerTransCode, $trans);
                    $otherImage[$trans] = $tmp;
                }
                $result['other_image'] = json_encode($otherImage);
            } else {
                if ($param['module_type'] == TplModuleRepo::MODULE_TYPE_HERO_CAROUSEL) {
                    $result['default_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrlOptions($data['default_image'], $transCodeName, '525x525', false);
                } else {
                    $result['default_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($data['default_image'], $largerTransCode, $transCodeName);
                }
                $result['other_image'] = $data['default_image'];
            }
            $seriesName = '';
            $seriesSlug = '';
            if (is_array($data['series']) && $data['series']) {
                $key = key($data['series']);
                $seriesName = $data['series'][$key]['series_name'];
                $seriesDesc = $data['series'][$key]['description'];
                $seriesSlug = $data['series'][$key]['seo_url'];
                $seriesUrl = $this->controller->generateUrl($this->routeSerie, array('series_slug' => $seriesSlug));
                $result['series_name'] = $seriesDesc;
                $result['series_url'] = $frontEndUrl . $seriesUrl;
                $result['series_slug'] = $seriesSlug;
            }
            $result['story_url'] = $frontEndUrl .
                $this->controller->generateUrl($this->routeStory, array(
                    'series_name' => $seriesSlug != '' ? $seriesSlug : 'n-a',
                    'slug' => Common::convertToSlug($data['title']),
                    'id' => Common::idToUrlId($data['id'])
                ));

            $result['description'] = strip_tags($data['description']);
            if (!empty($data['tags_business_ids']) ){
                $tmp = explode(',', $data['tags_business_ids']);
                if (count($tmp) == 1 ) {
                    $result['tags_business_ids'] = $tmp[0];
                }
            }
	    
	    $result['status'] = $data['status'];
            $this->result['details'] = $result;
        }
        return $this->result;
    }

}

