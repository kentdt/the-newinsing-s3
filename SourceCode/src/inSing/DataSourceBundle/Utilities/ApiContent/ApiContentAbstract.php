<?php
namespace inSing\DataSourceBundle\Utilities\ApiContent;

use inSing\DataSourceBundle\Utilities\ApiContent\ApiContentInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use inSing\DataSourceBundle\Utilities\Constant;
use inSing\DataSourceBundle\Repository\TemplateModuleRepository as TplModuleRepo;

class ApiContentAbstract implements ApiContentInterface
{
    /**
     * @var Controller
     */
    protected $controller;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Array
     */
    protected $transCodeName;

    /**
     * @var String
     */
    protected $defaultTransCodeName;


    public function __construct() {
        $this->defaultTransCodeName = '300x300';

        $this->transCodeName = array(
                TplModuleRepo::MODULE_TYPE_HERO => '738x556',
                TplModuleRepo::MODULE_TYPE_HERO_CAROUSEL => '315x315',
                TplModuleRepo::MODULE_TYPE_FEATURED_STORIES => '620x468',
                TplModuleRepo::MODULE_TYPE_FEATURED_STORIES_EDITOR_PICK => '98x86',
                TplModuleRepo::MODULE_TYPE_HIGHLIGHTED_STORIES => '620x468',
                TplModuleRepo::MODULE_TYPE_HIGHLIGHTED_STORIES_ADVERTORIAL => '300x226',                
                TplModuleRepo::MODULE_TYPE_BOOK_NOW => '620x468',
                TplModuleRepo::MODULE_TYPE_HIGHLIGHTED_RESTAURANTS => '620x468',
                TplModuleRepo::MODULE_TYPE_FEATURED_DEALS => '620x468',
                TplModuleRepo::MODULE_TYPE_REVIEWS_CAROUSEL => '270x188',
        );
    }


    /**
     * Magic setter
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function getContent($params) {

    }

    /**
     * Get trans code name of module
     *
     * @author LapTo
     * @param string $countryCode
     * @param string $moduleType
     * @return string
     */
    public function getTransCodeName($moduleType) {
        if (isset($this->transCodeName[$moduleType]) && !empty($this->transCodeName[$moduleType])) {
            return $this->transCodeName[$moduleType];
        }
        return $this->defaultTransCodeName;
    }

    /**
     * Get larger trans code name - fit all modules images
     *
     * @author LapTo
     * @param string $countryCode
     * @param string $moduleType
     * @return string
     */
    public function getLargerTransCode($moduleType, $pixelPlus = 100) {
        $transCode = $this->getTransCodeName($moduleType);
        $transCode = explode('x', $transCode);
        $width = 0;
        $height = 0;
        if (count($transCode) >= 2) {
            $width = $transCode[0] + $pixelPlus;
            $height = $transCode[1] + $pixelPlus;
        }
        return $width.'x'.$height;
    }

}