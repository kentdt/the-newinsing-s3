<?php

namespace inSing\DataSourceBundle\Utilities\ApiContent;

use inSing\DataSourceBundle\Utilities\ApiContent\ApiContentAbstract;
use inSing\DataSourceBundle\Utilities\Common;
use inSing\DataSourceBundle\Utilities\Constant;
use inSing\DataSourceBundle\Repository\TemplateModuleRepository as TplModuleRepo;

class Gallery extends ApiContentAbstract {

    /**
     * @var Array
     */
    protected $result = array(
        'result_code' => 0,
        'details' => array(
            'item_id' => '',
            'default_image' => '',
            'title' => '',
            'story_url' => '',
            'series_name' => '',
            'series_url' => '',
            'series_slug' => '',
            'description' => '',
            'tags_business_ids' => '',
            'status' => '',
            'other_image' => '',
        )
    );

    /**
     * @var String
     */
    private $routeSerie;

    /**
     * @var String
     */
    private $routeStory;


    public function __construct() {
        parent::__construct();
        $this->routeSerie = 'in_sing_admin_cms_series';
        $this->routeStory = 'in_sing_hgwr_cms_gallery';
    }

    /**
     * Return api result
     *
     * @param Array $param Include itemId, transCodeName
     * @return Array
     */
    public function getContent($param) {
        // get article from cms api
        if (!isset($param['item_id']) || empty($param['item_id']) ||
                !isset($param['module_type']) && !empty($param['module_type']) ) {
            throw new \Exception('Invalid variable');
        }
        $itemId = $param['item_id'];
        $selectedCountry = $this->controller->getSelectedCountry();
        $transCodeName = $this->getTransCodeName($param['module_type']);
        //for picking larger picture to crop
        $largerTransCode = $this->getLargerTransCode($param['module_type']);

        $apiResult = $this->container->get('cms.api')->getGalleryById($itemId, 0, null, null, $selectedCountry, true);
        //Check is guide
        if (isset($param['is_guide']) && $param['is_guide'] == true) {
            $apiResult = $this->filterGuide($apiResult);
        }

        $frontEndUrl = $this->container->getParameter('frontend_url');
        $frontEndUrl = trim($frontEndUrl[$selectedCountry], '/');
        $result = $this->result['details'];

        if (isset($apiResult['result_code']) && $apiResult['result_code'] == 200) {
            $this->result['result_code'] = 200;
            $data = $apiResult['details'];
            $result['item_id'] = $data['id'];
            $result['title'] = strip_tags($data['title']);

            $defaultImage = '';
            if (is_array($data['defaultImage']) && $data['defaultImage']) {
                $defaultImage = $data['defaultImage']['image_url'];
            }
            //Resize Image
            if (is_array($transCodeName) && !empty($transCodeName)) {
                $firstTransCode = array_shift($transCodeName);
                $result['default_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($defaultImage, $largerTransCode, $firstTransCode);
                $otherImage = array();
                foreach ($transCodeName as $trans) {
                    $tmp = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($defaultImage, $largerTransCode, $trans);
                    $otherImage[$trans] = $tmp;
                }
                $result['other_image'] = json_encode($otherImage);
            } else {
                if ($param['module_type'] == TplModuleRepo::MODULE_TYPE_HERO_CAROUSEL) {
                    $result['default_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrlOptions($defaultImage, $transCodeName, '525x525', false);
                } else {
                    $result['default_image'] = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($defaultImage, $largerTransCode, $transCodeName);
                }
                $result['other_image'] = $defaultImage;
            }
            $seriesName = '';
            if (is_array($data['series']) && $data['series']) {
                $key = key($data['series']);
                $seriesName = $data['series'][$key];
                $seriesUrl = $this->controller->generateUrl($this->routeSerie, array('series_slug' => $key));
                $result['series_name'] = $seriesName;
                $result['series_url'] = $frontEndUrl . $seriesUrl;
                $result['series_slug'] = $key;
            }

            $result['story_url'] = $frontEndUrl .
                    $this->controller->generateUrl($this->routeStory, array(
                        //'series_name' => $seriesName != '' ? Common::convertToSlug($seriesName) : 'n-a',
                        'gallery_slug' => Common::convertToSlug($data['title']),
                        'gallery_id' => Common::idToUrlId($data['id'])
                    ));

            $result['description'] = strip_tags($data['description']);
            if (!empty($data['tags_business_ids']) ){
                $tmp = explode(',', $data['tags_business_ids']);
                if (count($tmp) == 1 ) {
                    $result['tags_business_ids'] = $tmp[0];
                }
            }
	    $result['status'] = $data['status'];
            $this->result['details'] = $result;
        }
        return $this->result;
    }

    protected function filterGuide($data) {
        if (isset($data['result_code']) && $data['result_code'] == 200) {
            if (isset($data['details']['gallery_type']) && $data['details']['gallery_type'] == Constant::CMS_GALLERY_TYPE_GUIDE) {
                return $data;
            }
        }
        $empty = array(
            'result_code' => 201,
            'result_message' => 'The search query returned no results.',
            'detail' => array(),
        );
        return $empty;
    }

}

