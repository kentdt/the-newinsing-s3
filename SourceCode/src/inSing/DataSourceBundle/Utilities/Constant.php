<?php

namespace inSing\DataSourceBundle\Utilities;

/**
 * Description of Constant
 *
 */
class Constant {

    // Environment
    const ENV_DEV = 'dev';
    // Time format
    const ADMIN_DATETIME_FORMAT = 'm-d-Y H:i';
    const ADMIN_DATETIME_LOG_FORMAT = 'Y-m-d H:i';

    /* Devices */
    const DEVICE_PHONE = 'phone';
    const DEVICE_TABLET = 'tablet';
    const DEVICE_COMPUTER = 'computer';

    /* Admin Layout  */
    const GENERIC_LAYOUT_TEXT = 'Generic Layout';

    /*  Page Status */
    const PAGE_STATUS_DRAFT = 'Draft';
    const PAGE_STATUS_PUBLISHED = 'Published';
    const PAGE_STATUS_SUSPENDED = 'Suspended';
    const PAGE_STATUS_ARCHIVED = 'Archived';
    const PAGE_STATUS_DELETED = 'Deleted';
    const FOOTER_SEARCHES_POPULAR_TYPE = 1;
    const FOOTER_CUISINES_POPULAR_TYPE = 2;

    /* Google Analytic  */
    const GA_MOST_VIEW = 1;
    const GA_MOST_PHOTOS = 2;
    const GA_MOST_ARTICLE = 3;
    const GA_MOST_VIEW_TOP_NUMBER = 5;
    const GA_MOST_VIEW_TOP_NUMBER_FOR_SIDEBAR_CONTENT = 3;
    const GA_MOST_VIEW_LAST_NUMBER_DAY = 7;

    /* Cache Time */
    const CACHE_LIFE_TIME_IN_TEN_MINUTE = 600;
    const CACHE_LIFE_TIME_IN_ONE_HOUR = 3600;
    const CACHE_LIFE_TIME_IN_ONE_DAY = 86400;

    /* Key Cache */
    const PAGE_POOL_CACHE_KEY_PREFIX = 'PAGE_POOL_CACHE_KEY_PREFIX';
    const PAGE_MODULE_POOL_CACHE_KEY_PREFIX = 'PAGE_MODULE_POOL_CACHE_KEY_PREFIX';
    const FOOTER_POPULAR_KEY_CACHE = 'FOOTER_POPULAR_KEY_CACHE';
    const FOOTER_MOST_POPULAR = 'FOOTER_MOST_POPULAR';
    const HEADER_CUSTOMISE_DATA = 'HEADER_CUSTOMISE_DATA';
    const MOST_POPULAR_SIDEBAR_CONTENT = 'MOST_POPULAR_SIDEBAR_CONTENT';
    const KEY_CACHE_REFRESH_BOOK_NOW_IN_PAGE = 'KEY_CACHE_REFRESH_BOOK_NOW_IN_PAGE';
    const KEY_CACHE_FORCE_UPDATE_DATA_FOR_UNPICKED_IN_PAGE = 'KEY_CACHE_FORCE_UPDATE_DATA_FOR_UNPICKED_IN_PAGE';
    const KEY_CACHE_FORCE_UPDATE_STATUS_FOR_PICKED_STORIES_IN_PAGE = 'KEY_CACHE_FORCE_UPDATE_STATUS_FOR_PICKED_STORIES_IN_PAGE';
    const KEY_CACHE_IBL_PAGE_HOT_DEALS = "KEY_CACHE_IBL_PAGE_HOT_DEALS";
    const KEY_CACHE_IBL_PAGE_REVIEW_DETAILS = "KEY_CACHE_IBL_PAGE_REVIEW_DETAILS";
    const KEY_CACHE_IBL_PAGE_REVIEWS = "KEY_CACHE_IBL_PAGE_REVIEWS";
    const KEY_CACHE_IBL_PAGE_RESTAURANT_DETAILS = "KEY_CACHE_IBL_PAGE_RESTAURANT_DETAILS";
    const API_BIZ_FILTER_CATEGORY = 'Food & Drink';
    const FOOD = 'food';
    const FOOD_MY = 'food-my';  //For RnR MY
    const FOOD_AU = 'eatability';//For RnR AU
    const BUSINESS_DETAILS_NO_CATEGORY = 'NO CATEGORY';

    /* Conntry codes */
    const COUNTRY_CODE_SINGAPORE = 'en_SG';
    const COUNTRY_CODE_MALAYSIA = 'en_MY';
    const COUNTRY_CODE_AUSTRALIA = 'en_AU';

    /* ROLE ON MENU */
    const ROLEON_HOMEPAGE_PAGE_CONTENT = 'ROLEON_HOMEPAGE_PAGE_CONTENT';
    const ROLEON_HOMEPAGE_PAGE_DETAIL = 'ROLEON_HOMEPAGE_PAGE_DETAIL';
    const ROLEON_HOMEPAGE_PAGE_LAYOUT = 'ROLEON_HOMEPAGE_PAGE_LAYOUT';
    const ROLEON_HOMEPAGE_PAGE_LOG = 'ROLEON_HOMEPAGE_PAGE_LOG';
    const ROLEON_LANDING_PAGE = 'ROLEON_LANDING_PAGE';
    const ROLEON_LANDING_PAGE_CRUD = 'ROLEON_LANDING_PAGE_CRUD';
    const ROLEON_LANDING_PAGE_CONTENT = 'ROLEON_LANDING_PAGE_CONTENT';
    const ROLEON_LANDING_PAGE_DETAIL = 'ROLEON_LANDING_PAGE_DETAIL';
    const ROLEON_LANDING_PAGE_LAYOUT = 'ROLEON_LANDING_PAGE_LAYOUT';
    const ROLEON_LANDING_PAGE_LOG = 'ROLEON_LANDING_PAGE_LOG';
    const ROLEON_IBL_PAGE_CONTENT = 'ROLEON_IBL_PAGE_CONTENT';
    const ROLEON_IBL_PAGE_DETAIL = 'ROLEON_IBL_PAGE_DETAIL';
    const ROLEON_IBL_PAGE_LAYOUT = 'ROLEON_IBL_PAGE_LAYOUT';
    const ROLEON_IBL_PAGE_LOG = 'ROLEON_IBL_PAGE_LOG';
    const ROLEON_ARTICLE_PAGE = 'ROLEON_ARTICLE_PAGE';
    const ROLEON_ARTICLE_PAGE_CONTENT = 'ROLEON_ARTICLE_PAGE_CONTENT';
    const ROLEON_ARTICLE_PAGE_DETAIL = 'ROLEON_ARTICLE_PAGE_DETAIL';
    const ROLEON_ARTICLE_PAGE_LAYOUT = 'ROLEON_ARTICLE_PAGE_LAYOUT';
    const ROLEON_ARTICLE_PAGE_LOG = 'ROLEON_ARTICLE_PAGE_LOG';
    const ROLEON_GALLERY_PAGE_CONTENT = 'ROLEON_GALLERY_PAGE_CONTENT';
    const ROLEON_GALLERY_PAGE_DETAIL = 'ROLEON_GALLERY_PAGE_DETAIL';
    const ROLEON_GALLERY_PAGE_LAYOUT = 'ROLEON_GALLERY_PAGE_LAYOUT';
    const ROLEON_GALLERY_PAGE_LOG = 'ROLEON_GALLERY_PAGE_LOG';
    const ROLEON_GUIDE_PAGE_CONTENT = 'ROLEON_GUIDE_PAGE_CONTENT';
    const ROLEON_GUIDE_PAGE_DETAIL = 'ROLEON_GUIDE_PAGE_DETAIL';
    const ROLEON_GUIDE_PAGE_LAYOUT = 'ROLEON_GUIDE_PAGE_LAYOUT';
    const ROLEON_GUIDE_PAGE_LOG = 'ROLEON_GUIDE_PAGE_LOG';
    const ROLEON_THEME_PAGE = 'ROLEON_THEME_PAGE';
    const ROLEON_THEME_PAGE_CRUD = 'ROLEON_THEME_PAGE_CRUD';
    const ROLEON_THEME_PAGE_CONTENT = 'ROLEON_THEME_PAGE_CONTENT';
    const ROLEON_THEME_PAGE_DETAIL = 'ROLEON_THEME_PAGE_DETAIL';
    const ROLEON_THEME_PAGE_LAYOUT = 'ROLEON_THEME_PAGE_LAYOUT';
    const ROLEON_THEME_PAGE_LOG = 'ROLEON_THEME_PAGE_LOG';
    const ROLEON_TEMPLATE_PAGE = 'ROLEON_TEMPLATE_PAGE';
    const ROLEON_HEADER = 'ROLEON_HEADER';
    const ROLEON_FOOTER = 'ROLEON_FOOTER';
    const ROLEON_PUSH_NOTIFICATION_PAGE = 'ROLEON_PUSH_NOTIFICATION_PAGE';
    const ROLEON_USER_MY_PAGE = 'ROLEON_USER_MY_PAGE';
    const ROLEON_MODULE_API = 'ROLEON_MODULE_API';
    const ROLEON_SERP_PAGE = 'ROLEON_SERP_PAGE';
    const ROLEON_SERP_PAGE_CRUD = 'ROLEON_SERP_PAGE_CRUD';
    const ROLEON_SERP_PAGE_CONTENT = 'ROLEON_SERP_PAGE_CONTENT';
    const ROLEON_SERP_PAGE_DETAIL = 'ROLEON_SERP_PAGE_DETAIL';
    const ROLEON_SERP_PAGE_LAYOUT = 'ROLEON_SERP_PAGE_LAYOUT';
    const ROLEON_SERP_PAGE_LOG = 'ROLEON_SERP_PAGE_LOG';
    const BLOG_REVIEW_IS_SCRAPED_YES = 1;
    const BLOG_REVIEW_IS_SCRAPED_NOT = 0;
    const BLOG_REVIEW_IS_DELETED_YES = 1;
    const BLOG_REVIEW_IS_DELETED_NOT = 0;
    const BLOG_REVIEW_VOTE_THUMB_UP = 1;
    const BLOG_REVIEW_VOTE_THUMB_DOWN = 0;
    const ROLEON_BLOG_REVIEW_MANAGEMENT_PAGE = 'ROLEON_BLOG_REVIEW_MANAGEMENT_PAGE';

    /**
     * Constant Message
     */
    const SAVED_SUCCESSFULLY = "Saved Successfully!";
    const UPDATED_SUCCESSFULLY = "Updated Successfully!";
    const DELETED_SUCCESSFULLY = "Deleted Successfully!";
    const SAVED_UNSUCCESSFULLY = "Saved Unsuccessfully!";
    const UPDATED_UNSUCCESSFULLY = "Updated Unsuccessfully!";
    const DELETED_UNSUCCESSFULLY = "Deleted Unsuccessfully!";
    const MESSAGE_REQUEST_LOGIN = "Your session has expired! Please refresh your page to log in again.";

    /**
     * @author Dat.Dao
     * St
     */
    const FACEBOOK_PROVIDER = 'Facebook';
    const TWIITER_PROVIDER = 'Twitter';
    const FOLLOWER_LIMIT = 16;
    const FILE_SIZE_UPLOAD_IN_SETTING_PAGE = 1048576; //1MB=1024 x 1024
    const FOLLOWING_TAB_SUBSCRIPTION_LIMIT = 20;

    public static function getFormatProfileImage() {
        return array('image/gif', 'image/jpeg', 'image/png');
    }

    const FOLLOWER_VIEW_ALL_LIMIT = 100;
    const PROFILE_IMAGE_SIZE_68X68 = '68x68';
    const PROFILE_IMAGE_SIZE_110X80 = '110x80';
    const PROFILE_IMAGE_SIZE = '272x205';

    /**
     * @author Dat.Dao
     * Ed
     */

    /**
     * Define item type
     * @author Lap To
     */
    const ITEM_TYPE_ARTICLE = 'article';
    const ITEM_TYPE_GALLERY = 'gallery';
    const ITEM_TYPE_GUIDE = 'guide';
    const ITEM_TYPE_BUSINESS = 'business';
    const ITEM_TYPE_RNR = 'rnr';

    /*
     * Define for Reservation Board Tab
     */
    const RESERVATION_BOARD_TAB_WHAT = 'WHAT';
    const RESERVATION_BOARD_TAB_BUDGET = 'BUDGET';
    const RESERVATION_BOARD_TAB_WHEN = 'WHEN';
    const RESERVATION_BOARD_TAB_WHERE = 'WHERE';
    const RESERVATION_BOARD_TAB_WHO = 'WHO';

    /* Cache key */
    const CACHE_KEY_PAGE_DETAIL = 'CACHE_KEY_PAGE_DETAIL';
    const CACHE_KEY_PAGE_DETAIL_MODULE = 'CACHE_KEY_PAGE_DETAIL_MODULE';
    const CACHE_KEY_LIST_PICK_DATA_IDS = 'CACHE_KEY_LIST_PICK_DATA_IDS';
    const CACHE_KEY_PAGE_DETAIL_LANDINGPAGE_OG_IMAGE = 'CACHE_KEY_PAGE_DETAIL_LANDINGPAGE_OG_IMAGE';

    /**
     *
     */
    const MODULE_SEARCH_RESERVATION_PAX_NUMBER = 30;
    const MODULE_SEARCH_RESERVATION_PAX_PREFIX = 'person';

    /**
     * Locale Name
     * @var array
     */
    public static $SPECIFIC_TIME = array(
        '0500' => '05:00 am', '0530' => '05:30 am', '0600' => '06:00 am',
        '0630' => '06:30 am', '0700' => '07:00 am', '0730' => '07:30 am',
        '0800' => '08:00 am', '0830' => '08:30 am', '0900' => '09:00 am',
        '0930' => '09:30 am', '1000' => '10:00 am', '1030' => '10:30 am',
        '1100' => '11:00 am', '1130' => '11:30 am', '1200' => '12:00 pm',
        '1230' => '12:30 pm', '1300' => '01:00 pm', '1330' => '01:30 pm',
        '1400' => '02:00 pm', '1430' => '02:30 pm', '1500' => '03:00 pm',
        '1530' => '03:30 pm', '1600' => '04:00 pm', '1630' => '04:30 pm',
        '1700' => '05:00 pm', '1730' => '05:30 pm', '1800' => '06:00 pm',
        '1830' => '06:30 pm', '1900' => '07:00 pm', '1930' => '07:30 pm',
        '2000' => '08:00 pm', '2030' => '08:30 pm', '2100' => '09:00 pm',
        '2130' => '09:30 pm', '2200' => '10:00 pm', '2230' => '10:30 pm',
        '2300' => '11:00 pm', '2330' => '11:30 pm'
    );

    /* Guide Page */

    const GUIDE_PAGE_LASTSCOOP_CACHE = 'GUIDE_PAGE_LASTSCOOP_CACHE';
    const GUIDE_PAGE_ALSOONHGW_CACHE = 'GUIDE_PAGE_ALSOONHGW_CACHE';
    const GUIDE_PAGE_PHOTOS_CACHE = 'GUIDE_PAGE_PHOTOS_CACHE';
    const GUIDE_PAGE_INFO_CACHE = 'GUIDE_PAGE_INFO_CACHE';
    const GUIDE_PAGE_BUSINESS_IMAGE_TAG = 'image_tag';
    const GUIDE_PAGE_BUSINESS_GUIDE_TAG = 'guide_tag';
    const GUIDE_PAGE_PHOTOS_SIZE = '620x465';


    /* Gallery */
    const FOOD_PHOTOS_GALLERY_PHOTOS = 'FOOD_PHOTOS_GALLERY_PHOTOS';
    const CMS_GALLERY_TYPE_IMAGE = 0;
    const CMS_GALLERY_TYPE_VIDEO = 1;
    const CMS_GALLERY_TYPE_ALL = 2;
    const CMS_GALLERY_TYPE_GUIDE = 3;
    const CMS_CONTENT_TYPE_ARTICLE = 4;
    const CMS_GUIDE_FORCE_CRON_JOB = 'CMS_GUIDE_FORCE_CRON_JOB';
    //Module page size
    const MODULE_PAGE_SIZE = 20;
    const ARTICLE_DETAIL = 'ARTICLE_DETAIL';
    const RECIPE_SEO = "RECIPE_SEO";

    /* IBL Page */
    const IBL_SUBPAGE_REVIEW = 'IBL_REVIEW';
    const IBL_SUBPAGE_REVIEW_DETAIL = 'IBL_REVIEW_DETAIL';
    const IBL_SUBPAGE_PHOTO = 'IBL_PHOTO';
    const IBL_SUBPAGE_PHOTO_DETAIL = 'IBL_PHOTO_DETAIL';
    const IBL_SUBPAGE_HOT_DEAL = 'IBL_HOT_DEAL';
    const IBL_RANGE_DATE_FOR_RESERVATION = "IBL_RANGE_DATE_FOR_RESERVATION_";
    const IBL_MAIN_DETAIL_DATA = "IBL_MAIN_DETAIL_DATA";
    const IBL_MAIN_DETAIL_FAVOURITE_REVIEW_DATA = "IBL_MAIN_DETAIL_FAVOURITE_REVIEW_DATA";
    const IBL_DETAIL_INFORMATION_DATA = "IBL_DETAIL_INFORMATION_DATA";
    const CACHE_KEY_IBL_PHOTOS_RESTAURANT_PHOTO = 'CACHE_KEY_IBL_PHOTOS_RESTAURANT_PHOTO';
    const CACHE_KEY_REVIEW_TOP_3_ARTICLE = 'CACHE_KEY_REVIEW_TOP_3_ARTICLE';
    const CACHE_KEY_IBL_PHOTOS_DISH_PHOTO = 'CACHE_KEY_IBL_PHOTOS_DISH_PHOTO';
    const CACHE_KEY_IBL_PHOTO_DETAIL_RESTAURANT = 'CACHE_KEY_IBL_PHOTO_DETAIL_RESTAURANT';
    const CACHE_KEY_MANAGEMENT_RESPONSE = 'CACHE_KEY_MANAGEMENT_RESPONSE';
    const CACHE_KEY_REPORT_ERROR = 'CACHE_KEY_REPORT_ERROR';
    const CACHE_KEY_CLAIM_BUSINESS = 'CACHE_KEY_CLAIM_BUSINESS';
    /* Exception */
    const UNABLE_TO_LOAD_PREVIEW_PAGE = 'Unable to load preview page. Please try again later.';

    /**
     * Config page title, meta tag for SERP
     * @author Lap To
     */
    const SERP_PAGE_INFO_PAGE_TITLE = '[query] in [location]: [#]places to eat at in [country]';
    const SERP_PAGE_INFO_PAGE_DESCRIPTION = '[phase1]: Find the best rated and most recommended [phase2] in [country] on [hgwr_project_names]'; //Phase 1: [country] [query],Phase 2: [filter], [query]
    const SERP_PAGE_INFO_PAGE_KEYWORDS = '[phase1], food reviews, restaurants, where to eat, what to eat'; //Phase 1: [query], [filter], [country],
    const SERP_PAGE_INFO_FOOTER_SEO = '[country]\'s best food and dining guide. From recipes & restaurants, online reservation to hawker & buffet, [hgwr_project_names] knows where to eat and what to eat.';
    //Gimmie World events type
    const GW_EVENT_EMAIL_SIGNUP = 'email_signup';
    const GW_EVENT_FACEBOOK_LOGIN = 'facebook_login';
    const GW_EVENT_FIRST_TO_REVIEW = 'first_to_review';
    const GW_EVENT_FOLLOW_TWITTER = 'follow_twitter';
    const GW_EVENT_FOLLOW_USER = 'follow_user';
    const GW_EVENT_GAIN_FOLLOWER = 'gain_follower';
    const GW_EVENT_GIVE_BAD_REVIEW = 'give_bad_review';
    const GW_EVENT_GIVE_GOOD_REVIEW = 'give_good_review';
    const GW_EVENT_GIVE_HELPFUL_VOTE = 'give_helpful_vote';
    const GW_EVENT_HELPFUL_VOTE_RECEIVED = 'helpful_vote_received';
    const GW_EVENT_LIKE_FACEBOOK = 'like_facebook';
    const GW_EVENT_POST_PHOTO = 'post_photo';
    const GW_EVENT_SHARE_DISHRESTAURANT_FB = 'share_dishrestaurant_fb';
    const GW_EVENT_SUBMIT_FULL_REVIEW = 'submit_full_review';
    const GW_EVENT_SUBMIT_QUICK_REVIEW = 'submit_quick_review';
    const GW_EVENT_SUBMIT_THUMBSDOWN = 'submit_thumbsdown';
    const GW_EVENT_SUBMIT_THUMBSUP = 'submit_thumbsup';
    const GW_EVENT_TRIED_REVIEW_CHINESE = 'tried_review_chinese';
    const GW_EVENT_TRIED_REVIEW_INDIAN = 'tried_review_indian';
    const GW_EVENT_TRIED_REVIEW_JAPANESE = 'tried_review_japanese';
    const GW_EVENT_TRIED_REVIEW_KOREAN = 'tried_review_korean';
    const GW_EVENT_TRIED_REVIEW_MALAYSIAN = 'tried_review_malaysian';
    const GW_EVENT_TRIED_REVIEW_VEGETARIAN = 'tried_review_vegetarian';
    const GW_EVENT_TRIED_REVIEW_WESTERN = 'tried_review_western';
    const GW_EVENT_TRIED_REVIEW_HALAL = 'tried_review_halal';
    const GW_EVENT_TRIED_REVIEW_DIMSUM = 'tried_review_dimsum';
    const GW_EVENT_TRIED_REVIEW_DESSERTS = 'tried_review_desserts';
    const GW_EVENT_TRIED_REVIEW_ITALIAN = 'tried_review_italian';
    const GW_EVENT_TRIED_REVIEW_FRENCH = 'tried_review_french';
    const GW_EVENT_TRIED_REVIEW_SINGAPOREAN = 'tried_review_singaporean';
    const GW_EVENT_TRIED_REVIEW_HAWKER = 'tried_review_hawker';
    const GW_EVENT_TRIED_REVIEW_CAFE = 'tried_review_cafe';
    const GW_EVENT_TRIED_REVIEW_RESTAURANT = 'tried_review_restaurant';
    const GW_EVENT_TRIED_REVIEW_MAMAK = 'tried_review_mamak';
    const GW_EVENT_TRIED_REVIEW_ROMANCE = 'tried_review_romance';
    const GW_EVENT_TRIED_REVIEW_SUPPER = 'tried_review_supper';
    const GW_EVENT_TRIED_REVIEW_HIDDENFIND = 'tried_review_hiddenfind';
    const GW_EVENT_TRIED_REVIEW_LARGEGROUPS = 'tried_review_largegroups';
    const GW_EVENT_TRIED_REVIEW_BRUNCH = 'tried_review_brunch';
    const GW_EVENT_TRIED_REVIEW_BUFFET = 'tried_review_buffet';
    const GW_EVENT_TRIED_REVIEW_PETFRIENDLY = 'tried_review_petfriendly';
    const GW_EVENT_TRIED_REVIEW_BUDGET = 'tried_review_budget';
    const GW_EVENT_TRIED_REVIEW_FAMILY = 'tried_review_family';
    const GW_EVENT_TRIED_REVIEW_HIGHTEA = 'tried_review_hightea';
    const GW_EVENT_TRIED_REVIEW_PHOTO = 'tried_review_photo';
    const GW_EVENT_UPDATE_DESCRIPTION_PHOTO = 'update_description_photo';
    const GW_EVENT_UPDATE_PROFILE = 'update_profile';
    const GW_EVENT_VERIFY_ACCOUNT = 'verify_account';
    const GW_EVENT_VISIT_RESTAURANT = 'visit_restaurant';
    const GW_EVENT_VISIT_USER = 'visit_user';
    const GW_EVENT_WANT_TRIED = 'want_tried';

    public static $currency = array(
        self::COUNTRY_CODE_SINGAPORE => 'Sgd',
        self::COUNTRY_CODE_MALAYSIA => 'Myr',
        self::COUNTRY_CODE_AUSTRALIA => 'Aud',
    );

    /**
     * @author Minh Tong
     * IBL Front End Reviews perpage
     */
    const IBL_REVIEW_PER_PAGE = 5;
    const MODULE_API_PER_PAGE = 10;
    const LIST_STRIPTAGS_HTML_REVIEW = '<a><b><strong><i><em><u><br /><br/><br>';

    /**
     * Text for page not found
     * @author LapTo
     */
    const TEXT_NOT_FOUND = 'Page Not Found';

    /**
     * @author Minh Tong
     * SERP Admin Module name
     */
    const MODULE_NAME_SERP_PLACEHOLDER = 'SERP results module';
    const MODULE_SERP_PAGE_ITEMS_TYPE_BUSSINESS = 'business';
    const MODULE_SERP_PAGE_ITEMS_TYPE_DEALS = 'deals';
    const MODULE_SERP_PAGE_ITEMS_TYPE_CONTENT = 'content';
    const MODULE_SERP_PAGE_DISPLAY_TYPE_LIST = 'list';
    const MODULE_SERP_PAGE_DISPLAY_TYPE_TILE = 'tile';
    const MODULE_SERP_PAGE_DISPLAY_TYPE_MAP = 'map';

    const MESSAGE_USER_NOT_FOUND_ACCOUNT_RESTFUL = 'user not found';
    const MESSAGE_WRONG_PASS_ACCOUNT_RESTFUL = 'incorrect password';
    const MESSAGE_INACTIVE_USER_ACCOUNT_RESTFUL = 'user is inactive user';
    const MESSAGE_CREATE_USER_ACCOUNT_RESTFUL_NICKNAME_EXIST = 'nickname already registed!';
    const MESSAGE_WRONG_PASS_ACCOUNT_RESTFUL_EDIT = 'old_password is invalid';


    // Set channel call Account API Restful
    const GENDER_MALE = "Male";
    const GENDER_FEMALE = "Female";
    const API_ACCOUNT_RESTFUL_SUCCESS_CODE = 200;
    const API_ACCOUNT_RESTFUL_402_CODE = 402;
    const API_ACCOUNT_RESTFUL_604_CODE = 604;
    const API_ACCOUNT_RESTFUL_603_CODE = 603;
    const API_ACCOUNT_RESTFUL_607_CODE = 607;
    const API_ACCOUNT_RESTFUL_611_CODE = 611;
    const API_ACCOUNT_RESTFUL_613_CODE = 613;
    const API_ACCOUNT_RESTFUL_615_CODE = 615;
    const WRONG_DATE_PROFILE_RESFUL_API = "-0001-11-30";
    const NOT_ACTIVATED_USER = "NOT_ACTIVATED_USER";
    // Profile Follow Page Size
    const PAGE_SIZE_FOLLOWING = 20;
    const PAGE_SIZE_FOLLOWER = 20;
}
