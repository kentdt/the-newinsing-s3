<?php
namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Utilities\Curl;
use inSing\DataSourceBundle\Utilities\Constant;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CmsSearchApi
{
    protected $_cms_seach_info = null;
    protected $_container = null;
    protected $_logger = null;
    protected $_country_code = 'en_SG';
    protected $_cache = null;
    public $listChannel;
    const SESSION_TOKEN_CACHE = 'CMS_SEARCH_API_SESSION_TOKEN_CACHE';

    public function __construct($container) {
        $this->_container = $container;
        $this->_country_code = $container->getParameter('country_code');
        $this->_cache = $this->_container->get('hgw.cache');
        $this->_logger = $this->_container->get('monolog.logger.cms_search_api');
        $this->listChannel = $container->getParameter('cms_channel');
        $this->init();
    }

    private function init()
    {
        $country_cms_search_api = $this->_container->getParameter('cms_search_api');
        $this->_cms_seach_info = $country_cms_search_api[$this->_country_code];
    }

    public function switchCountry($country_code)
    {
        $this->_country_code = $country_code;
        $this->init();
        return $this;
    }

    /**
     * Aggregate Search API
     * The aggregate search API returns both articles 
     * and galleries found using a set of search terms and filters.
     * @param array $business_id
     * @param array $restaurant_id
     * @param string $country_code
     * @return multitype:
     * @author tri.van
     */
    public function aggregateSearch($filters = null, $page_number = 1, $page_size = 20, $image_height = null, $image_width = null, $country_code = '') {
        try {
            if ($country_code == '') {
                $country_code = $this->_country_code;
            }

            $country_biz_listing = $this->_container->getParameter('cms_search_api');
            $this->_cms_seach_info = $country_biz_listing[$country_code];
            $url = $this->_cms_seach_info['aggregate_search_url'];

            $params = array(
                    'session_token' => $this->getSessionToken(),
                    'page_number' => $page_number,
                    'page_size' => $page_size,
                    'tag_operator' => 'OR',
                    'sort_by' => 'publish_date'
            );
            $params['channel[]'] = $this->listChannel[$country_code];

            if (!empty($filters)) {
                $params = array_merge($params, $filters);
            }
            if (!empty($image_height)) {
                $params['image_height'] = $image_height;
            }
            if (!empty($image_width)) {
                $params['image_width'] = $image_width;
            }

            $stories = $this->doGetWrapper($url, $this->_cms_seach_info['consumer_secret'], $params);

                if (Curl::HTTP_OK == $stories['result_code'] && !empty($stories['results'])) {
                    for ($i = 0; $i < count($stories['results']); $i++) {
                        if ($stories['results'][$i]['type'] == 'article') {
                            //Article
                            $stories['results'][$i] = $this->parseArticle($stories['results'][$i]);
                        } else {
                            //Gallery
                            $stories['results'][$i] = $this->parseGallery($stories['results'][$i]);
                        }
                    }
                }

//             echo '<pre>';print_r($stories);echo '</pre>';die;
            return $stories;
        } catch (\Exception $exc) {
            $this->_logger->error($exc->getMessage());
        }
        
        return null;
    }

    protected function parseArticle($article) {
        if (!empty($article)) {
            $article['createdOn'] = $article['created_date'];
            unset($article['created_date']);
            $article['publishOn'] = $article['publish_date'];
            unset($article['publish_date']);
            $article['updatedOn'] = $article['last_updated_date'];

            //Parse Series
            foreach ($article['series'] as $key => $series) {
                $article['series'][$series['seo_url']] = $series;
                unset($article['series'][$key]);
            }

            //Parse Cuisine
            foreach ($article['cuisines'] as $key => $cuisine) {
                $article['cuisines'][$cuisine['description']] = $cuisine;
                unset($article['cuisines'][$key]);
            }

            $article['description'] = (!empty($article['summary'])) ? $article['summary'] : '';
            $article['default_image'] = '';
            $article['rich_text'] = '';

            if (!empty($article['body'])) {
                usort($article['body'], function($a, $b) {
                    return $a['sort'] - $b['sort'];
                });

                    foreach ($article['body'] as $bodyContent) {
                        switch ($bodyContent['module']) {
                            case 'default_image':
                                $article['default_image'] = $bodyContent['default_image'];
                                break;
                        }
                    }
            } else {
                $article['body'] = '';
            }
        }

        $article['type'] = 0;
        $article['defaultImage'] = $article['default_image'];
        return $article;
    }

    protected function parseGallery($gallery) {
        if (!empty($gallery)) {
    
            //Parse Series
            if(isset($gallery['series'])){
                foreach ($gallery['series'] as $key => $series) {
                    $gallery['series'][$series['seo_url']] = $series['description'];
                    unset($gallery['series'][$key]);
                }
            }

            if(isset($gallery['summary'])){
                $gallery['description'] = $gallery['summary'];
            }

            $gallery['totalImage'] = 0;
            $boilerPlateIds = array();
            //Get Default Image
            $gallery['defaultImage'] = array();
            if (!empty($gallery['images'])) {
                $gallery['totalImage'] = count($gallery['images']);

                //Get Main image
                $i = 0;
                foreach ($gallery['images'] as $arrImage) {
                    if ($arrImage['is_main'] == 1 || $i == 0) {
                        $gallery['defaultImage'] = $arrImage;
                    }
                    if ($arrImage['is_main'] == 1) {
                        break;
                    }
                    if ($arrImage['biz_id']) {
                        $boilerPlateIds[] = $arrImage['biz_id'];
                    }
                    $i++;
                }
            }
            $gallery['boilerPlateIds'] = $boilerPlateIds;
        }
        $gallery['type'] = 1;
        return $gallery;
    }

    /**
     * Get session token
     * @return string sessionToken
     */
    public function getSessionToken() {
        try {
            $cache_key = $this->_country_code . '_' . self::SESSION_TOKEN_CACHE;
            $sessionToken = $this->_cache->fetch($cache_key);
            if (empty($sessionToken)) {
                $result = $this->authenticate();
                if (!empty($result) && count($result)) {
                    $sessionToken = $result['session_token'];
                    $expire_at = new \DateTime();
                    $expire_at->setTimestamp($result['expire_at']);
                    $current_date = new \DateTime();
                    $diff_date = $current_date->diff($expire_at);
    
                    $minutes = $diff_date->days * 24 * 60;
                    $minutes += $diff_date->h * 60;
                    $minutes += $diff_date->i;
                    $second = ($diff_date->s + ($minutes * 60));
                    // Time backup
                    if ($second > 600) {
                        $second = $second - 600;
                    }
    
                    $this->_cache->save($cache_key, $sessionToken, $second);
                }
            }
            return $sessionToken;
        } catch (\Exception $exc) {
            $this->_logger->error($exc->getMessage());
        }
    
        return null;
    }

    public function authenticate() {
        try {
            $result_authenticate = $this->doPostWrapper($this->_cms_seach_info['authenticate_url'], 
                    $this->_cms_seach_info['consumer_secret'], array(
                    'consumer_key' => $this->_cms_seach_info['consumer_key'],
                    'consumer_secret' => $this->_cms_seach_info['consumer_secret'],
                    'access_token' => $this->_cms_seach_info['access_token'],
                    'access_token_secret' => $this->_cms_seach_info['access_token_secret']
            ));
            if (RestfulAPIHelper::HTTP_OK != $result_authenticate['status']) {
                return null;
            }
            return $result_authenticate['data'];
        } catch (\Exception $exc) {
            $this->_logger->error($exc->getMessage());
        }
    }

    private function getCallingString($url, $secret, $fields)
    {
        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;
        $fields['sig'] = $this->genSignature($fields, $pathinfo);
        return $fields;
    }

    private function genSignature($params, $pathinfo)
    {
        $result = $pathinfo;
        ksort($params);
        foreach ($params as $key => $value)
        {
            if(is_array($value)) {
                foreach ($value as $item) {
                    $result .=  urlencode($key . $item);
                }
            }
            else {
                $result .=  urlencode($key . $value);
            }
        }
        return md5($result);
    }
    
    protected function doGetWrapper($url, $secret, $fields) {
        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_GET);
        $curl->setChannel('monolog.logger.cms_search_api');

        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields, true);

        if ($res['result_code'] == Curl::SESSION_TOKEN_TIMEOUT 
                || $res['result_code'] == Curl::HTTP_UNAUTHORIZED) {
            $this->_cache->delete($this->_country_code . '_' . self::SESSION_TOKEN_CACHE);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields, true);
        }
        else if ($res['result_code'] != Curl::HTTP_OK) {
            if(is_array($res)) {
                $this->_logger->error($url, $res);
            }
            else {
                $this->_logger->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    protected function doPostWrapper($url, $secret, $fields) {
        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_POST);
        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);
        $this->_logger->info($url, $fields);
    
        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT 
                || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED) {
            $this->_cache->delete($this->_country_code . '_' . self::SESSION_TOKEN_CACHE);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;
    
            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        }
        else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if(is_array($res)) {
                $this->_logger->error($url, $res);
            }
            else {
                $this->_logger->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    public function doCurlGet($url) {
        try {
            // initialize cURL
            $ch = curl_init();
            // set options for cURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);

            // execute HTTP POST request
            $response = curl_exec($ch);
            // close connection

            curl_close($ch);
            $ret = json_decode($response, true);
            if (isset($ret) && $ret['response']['status'] == RestfulAPIHelper::TBL_STATUS_OK) {
                return $ret['response'];
            }
            return array();
        } catch (\Exception $exc) {
            throw $exc;
        }
    }
}