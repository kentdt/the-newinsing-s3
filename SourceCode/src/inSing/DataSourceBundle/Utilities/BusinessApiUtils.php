<?php

/**
 * Description of BusinessApiUtils
 *
 * @author bien.tran
 */

namespace inSing\DataSourceBundle\Utilities;

class BusinessApiUtils {
    const RESTAURANT_CACHE = 'BUSINESS_API_RESTAURANT_CACHE';
    const IBL_PROMOTIONS_CACHE = 'BUSINESS_API_IBL_PROMOTIONS_CACHE';
    const SESSION_TOKEN_CACHE = 'BUSINESS_API_SESSION_TOKEN_CACHE';
    const IBL_RESTAURANT_TOPMUSTTRY = "BUSINESS_API_IBL_RESTAURANT_TOPMUSTTRY";
    const IBL_RESTAURANT_FAVOURITE_REVIEW = "BUSINESS_API_IBL_RESTAURANT_FAVOURITE_REVIEW";
    const IBL_RESTAURANT_REVIEW = "BUSINESS_API_IBL_RESTAURANT_REVIEW";
    const IBL_RESTAURANT_SUITABLEFOR = "BUSINESS_API_IBL_RESTAURANT_SUITABLEFOR";

    /* Restaurant Business Details */
    const BUSINESS_DETAILS_PROPERTIES_CUISINE = 'CUISINE';
    const BUSINESS_DETAILS_CATEGORY_FOOD_AND_DRINK = 'FOOD & DRINK';
    const BUSINESS_DETAILS_CATEGORY_BARS_AND_PUBS = 'BARS & PUBS';
    const BUSINESS_DETAILS_CATEGORY_RESTAURANTS = 'RESTAURANTS';
    const BUSINESS_DETAILS_CATEGORY_HAWKERS = 'HAWKERS';
    const BUSINESS_DETAILS_NO_CATEGORY = 'NO CATEGORY';

    protected $_business_seach_info = null;
    protected $_container = null;
    protected $_logger = null;
    protected $_country_code = 'en_SG';
    protected $restaurantCache = null;

    public function __construct($container) {
        $this->_container = $container;
        $this->_country_code = $container->getParameter('country_code');
        $this->restaurantCache = $this->_container->get('hgw.cache');
        $this->_logger = $this->_container->get('monolog.logger.bizapi');

        $this->init();
    }

    private function init()
    {
        $country_biz_listing = $this->_container->getParameter('business_search_api');
        $this->_business_seach_info = $country_biz_listing[$this->_country_code];
    }

    public function switchCountry($country_code)
    {
        $this->_country_code = $country_code;
        $this->init();
        return $this;
    }

    /**
     *
     * @param ContainerAwareInterface $container
     * @param array $params
     * @param boolean $include_Rnr
     * @param boolean $include_properties
     * @param boolean $include_timslots
     * @param string $country_code require on ADMIN page and Cronjob
     * @return array
     */
    public function searchForBusinesses(array $params = NULL, $include_Rnr = false, $include_properties = true, $include_timslots = false, $country_code = '') {

        $results = array();

        if($country_code != '')
        {
            $country_biz_listing = $this->_container->getParameter('business_search_api');
            $this->_business_seach_info = $country_biz_listing[$country_code];
        }

        $url = $this->_business_seach_info['search_url'];

        $session_token = $this->getSessionToken();

        $fields = array(
            'session_token' => $session_token
        );

        $fields['include_rnr'] = $include_Rnr ? 'Y' : 'N';
        $fields['include_hgw'] = 'Y';
        $fields['include_properties'] = $include_properties ? 'Y' : 'N';
        $fields['include_timeslots'] = $include_timslots ? 'Y' : 'N';
        $fields['filter_category'] = 'Food & Drink';
        $fields['search_type'] = 'food';

        /**
         * EATABILITY doesn't support include_timeslots parameter
         */
        if($country_code == 'en_AU' || $this->_country_code == 'en_AU')
        {
            unset($fields['include_timeslots']);
        }

        $fields = array_merge($fields, $params);

        $results = $this->doGetWrapper($url, $this->_business_seach_info['consumer_secret'], $fields);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $results['status'])
        {
            return array();
        }

        // set state for each biz
        $state_default_locales = $this->_container->getParameter('country_route_path_locale');
        $country_code = ($country_code == '') ? $this->_country_code : $country_code;
        foreach ($results['data']['results'] as &$biz) {
            $state = isset($biz['address']['state']) ? $biz['address']['state'] : '';
            if($state == '') {
                $state = $state_default_locales[$country_code];
            } else {
                $state = Common::convertToSlug($state);
            }
            $biz['state'] = $state;

            $stateName = isset($biz['address']['state']) ? $biz['address']['state'] : '';
            if($stateName == '') {
                $stateName = $state_default_locales[$country_code];
            }
            $biz['state_name'] = $stateName;
        }

        return $results;
    }

    /**
     *
     * @param ContainerAwareInterface $container
     * @param array $ids
     * @param array $more_params
     * @param string $country_code require on ADMIN page and Cronjob
     * @return array
     */
    public function getListBusinesses(array $ids, $more_params = array(), $country_code = '', $get_business_options = false) {

        $results = array();

        if (empty($ids)) {
            return array();
        }
        if($country_code != '')
        {
            $country_biz_listing = $this->_container->getParameter('business_search_api');
            $this->_business_seach_info = $country_biz_listing[$country_code];
        }

        $url = $this->_business_seach_info['get_detail_url'];

        $url = $url . implode('|', $ids);
        $session_token = $this->getSessionToken();

        $fields = array(
            'session_token' => $session_token,
            'include_timeslots' => 'Y'
        );
        if ($get_business_options) {
            $fields['include_business_options'] = 'Y';
        }

        /**
         * EATABILITY doesn't support include_timeslots parameter
         */
        if($country_code == 'en_AU' || $this->_country_code == 'en_AU')
        {
            unset($fields['include_timeslots']);
        }

        if (is_array($more_params) && count($more_params)) {
            $fields = array_merge($fields, $more_params);
        }
        //$fields = array_merge($fields, $params);

        $results = $this->doGetWrapper($url, $this->_business_seach_info['consumer_secret'], $fields);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $results['status'])
        {
            return array();
        }

        // set state for each biz
        $state_default_locales = $this->_container->getParameter('country_route_path_locale');
        $country_code = ($country_code == '') ? $this->_country_code : $country_code;
        foreach ($results['data']['business'] as &$biz) {
            $state = isset($biz['address']['state']) ? $biz['address']['state'] : '';
            if($state == '') {
                $state = $state_default_locales[$country_code];
            } else {
                $state = Common::convertToSlug($state);
            }
            $biz['state'] = $state;

            $stateName = isset($biz['address']['state']) ? $biz['address']['state'] : '';
            if($stateName == '') {
                $stateName = $state_default_locales[$country_code];
            }
            $biz['state_name'] = $stateName;
        }

        return $results;
    }

    public function parseBusinessSearch($params, $include_Rnr = false, $include_properties = true, $include_timslots = false, $image_size = '150x113') {

        $items_list = array();
        $restaurants = array();
        $resultsTemp = array();
        $total_page = 0;

        $searchResult = $this->searchForBusinesses($params, $include_Rnr, $include_properties, $include_timslots);
        if (!empty($searchResult['data']['results']) && count($searchResult['data']['results'])) {
            $total_page = !empty($searchResult['total']) ? ceil($searchResult['total'] * 1.0 / $params['per_page']) : 0;
            $restaurants = $searchResult['data']['results'];
        }
        $results['max_result'] = isset($searchResult['total']) ? $searchResult['total'] : 0;
        $results['total_page'] = $total_page;
        $facetsDefault['type_of_place'] = array();
        $facetsDefault['cuisine'] = array();
        $facetsDefault['suitable_for'] = array();
        $facetsDefault['neighbourhood'] = array();

        $results['facets'] = isset($searchResult['data']['facets']) && count($searchResult['data']['facets']) ? $searchResult['data']['facets'] : $facetsDefault;
        $item_ids = array();
        try {
            foreach ($restaurants as $key => $restaurant) {
                $restaurants[$key] = $restaurant['details'];
                if (isset($restaurant['details']['id']) && $restaurant['details']['id']) {
                    $item_ids[] = $restaurant['details']['id'];
                }
            }
            $items_list = $this->_container->get('rnr.api')->getItemListApi($item_ids, '', 1, count($item_ids), true);

            if (count($items_list)) {
                foreach ($items_list as $key => $item) {
                    $items_list[$item['item_id']] = $item;
                    unset($items_list[$key]);
                }
            }
        } catch (\Exception $exc) {
            // Log
        }

        try {
            if (!empty($restaurants)) {
                foreach ($restaurants as $restaurant) {
                    $restaurant = $this->parseRestaurantDetails($restaurant, $image_size);

                    $restaurant_item_list = array();
                    if (array_key_exists($restaurant['id'], $items_list)) {
                        $restaurant_item_list = $items_list[$restaurant['id']];
                    }
                    if ($restaurant['no_image']) {
                        if (isset($restaurant_item_list['latest_review_photo']) && $restaurant_item_list['latest_review_photo']) {
                            //$params = array('image' => $restaurant_item_list['latest_review_photo'], 'w' => 0, 'h' => 0, 'width' => 150, 'height' => 113, 'type' => 'user_photo');
                            //$url_media = $this->_container->get('router')->generate('HgwBundle_generate_image_thumbnail', $params, true);
                            $url_media = $this->_container->get('common.utils')->generatePhotoFromUrl($restaurant_item_list['latest_review_photo'], $image_size);
                            $restaurant['image_url'] = $url_media;
                        }
                    }
                    if($include_timslots)
                    {
                        //$restaurant['time_slots_parse'] = $this->parseTimeSlots($restaurant, $restaurant['time_slots'], $filter_pax_time);
                        $time_slots = isset($restaurant['time_slots']) ? $restaurant['time_slots'] : array();
                        $filter_pax_time = isset($params['availability_datetime']) && $params['availability_datetime'] != '' ? new \DateTime($params['availability_datetime']) : null;
                        // Parse Timeslots
                        $restaurant['time_slots_parse'] = $this->parseTimeSlots($time_slots, $filter_pax_time);
                    }
                    else
                    {
                        $restaurant['time_slots_parse'] = array();
                    }

                    $restaurant['review'] = array();
                    if (count($restaurant_item_list)) {
                        if (isset($restaurant_item_list['fav_review']) && count($restaurant_item_list['fav_review'])) {
                            $restaurant['review'] = $restaurant_item_list['fav_review'];
                        } else {
                            if (isset($restaurant_item_list['latest_positive_review']) && count($restaurant_item_list['latest_positive_review'])) {
                                $restaurant['review'] = $restaurant_item_list['latest_positive_review'];
                            }
                        }
                    }
                    $resultsTemp[$restaurant['id']] = $restaurant;
                }
                $results['results'] = $resultsTemp;
            }
        } catch (\Exception $exc) {

        }

        return $results;
    }

    /**
     *
     * @param ContainerAwareInterface $container
     * @param array $ids_or_slug
     * @param string $image_size
     * @param string $country_code require on ADMIN page and Cronjob
     * @return array
     */
    public function getBusinessDetails(array $ids_or_slug, $image_size = '300x225', $country_code = '', $get_business_options = false) {
        $tmpIdOrSlug = $ids_or_slug;
        //$statisticByIds = array();
        $restaurantDetails = array();
        $result = array();

        if($country_code == '')
        {
            $country_code = $this->_country_code;
        }
        $keyCache = $country_code . '_' . self::RESTAURANT_CACHE . '_' . $image_size . '_';
        // Get data from cache
        foreach ($ids_or_slug as $key => $value) {
            $resCache = $this->restaurantCache->fetch($keyCache . $value);            
            if ($resCache) {
                $restaurantDetails[] = $resCache;
                unset($ids_or_slug[$key]);
            }
        }

        try {
            if (count($ids_or_slug)) {
                $result = $this->getListBusinesses($ids_or_slug, array(), $country_code, $get_business_options);
                if(!isset($result['data']['business']) || empty($result['data']['business']))
                {
                    $result['data']['business'] = array();
                }
            }
        } catch (\Exception $exc) {
            $this->_logger->error($exc->getMessage());
        }

        $cache_time = $this->_container->getParameter('business_details_cache_time');
        if(isset($result['data']['business']) && !empty($result['data']['business']))
        {
            foreach ($result['data']['business'] as $restaurantDetail) {
                $restaurant = $this->parseRestaurantDetails($restaurantDetail, $image_size, $country_code);
                if($restaurant['id'])
                {
                    $this->restaurantCache->save($keyCache . $restaurant['id'], $restaurant, $cache_time);
                }
                if(!empty($restaurant['slug']))
                {
                    $this->restaurantCache->save($keyCache . $restaurant['slug'], $restaurant, $cache_time);
                }
                $restaurantDetails[] = $restaurant;
            }
        }

        if(count($tmpIdOrSlug) == 1 && count($restaurantDetails))
        {
            $restaurantDetails = $restaurantDetails[count($restaurantDetails) - 1];
        }

        return $restaurantDetails;
    }

    /**
     * Get Business Detail for Search Page - No implement cache
     * @ref: getBusinessDetail()
     * @author LapTo
     */
    public function getBusinessDetailForSearch(array $ids_or_slug, $image_size = '300x225', $country_code = '', $get_business_options = false) {
	$tmpIdOrSlug = $ids_or_slug;
        //$statisticByIds = array();
        $restaurantDetails = array();
        $result = array();

        if($country_code == '')
        {
            $country_code = $this->_country_code;
        }

        try {
            if (count($ids_or_slug)) {
                $result = $this->getListBusinesses($ids_or_slug, array(), $country_code, $get_business_options);
                if(!isset($result['data']['business']) || empty($result['data']['business']))
                {
                    $result['data']['business'] = array();
                }
            }
        } catch (\Exception $exc) {
            $this->_logger->error($exc->getMessage());
        }

        if(isset($result['data']['business']) && !empty($result['data']['business']))
        {
            foreach ($result['data']['business'] as $restaurantDetail) {
                $restaurant = $this->parseRestaurantDetails($restaurantDetail, $image_size, $country_code);
                $restaurantDetails[] = $restaurant;
            }
        }

        if(count($tmpIdOrSlug) == 1 && count($restaurantDetails))
        {
            $restaurantDetails = $restaurantDetails[count($restaurantDetails) - 1];
        }
        return $restaurantDetails;
    }

    public function parseRestaurantDetails($restaurant, $image_size = '300x225', $country_code = '') {
        if($country_code == '') {
            $country_code = $this->_country_code;
        }

        $rnrObj = isset($restaurant['rnr']) ? $restaurant['rnr'] : array();

        $restaurant['id'] = $restaurant['id'];
        $restaurant['status'] = $restaurant['status'];
        $taxonomyNodes = isset($restaurant['taxonomy_nodes']) && count($restaurant['taxonomy_nodes']) ? $restaurant['taxonomy_nodes'] : array();

        $taxonomyNodeNameArr = count($taxonomyNodes) >= 2 ? explode('>', $taxonomyNodes[1]['path']) : array(); // Food & Drink > Restaurants

        $category = '';
        //Checking name is "Food & Drink"
        if (count($taxonomyNodeNameArr) >= 2 && self::BUSINESS_DETAILS_CATEGORY_FOOD_AND_DRINK == strtoupper(trim($taxonomyNodeNameArr[0]))) {
            $category = trim($taxonomyNodeNameArr[1]);
        }
        $restaurant['category'] = $category;

        if (isset($rnrObj['ratings'])) {
            foreach ($rnrObj['ratings'] as $key => $value) {
                $keyValue = array_keys($value);
                $rnrObj['ratings'][$keyValue[0]] = $value[$keyValue[0]];
                unset($rnrObj['ratings'][$key]);
            }
        }

        $restaurant['rating'] = isset($rnrObj['ratings']) ? $rnrObj['ratings'] : array();

        $restaurant['vote_count'] = isset($rnrObj['vote_count']) ? $rnrObj['vote_count'] : 0;
        $restaurant['thumbs_up_count'] = isset($rnrObj['thumbs_up_count']) ? $rnrObj['thumbs_up_count'] : 0;
        $restaurant['review_count'] = isset($rnrObj['review_count']) ? $rnrObj['review_count'] : 0;

        // round up data thumbs_up_percentage
        $restaurant['thumbs_up_percentage'] = isset($rnrObj['thumbs_up_percentage']) ?
        ceil($rnrObj['thumbs_up_percentage']) : 0;

        $restaurant['avg_price'] = isset($rnrObj['avg_price']) ? $rnrObj['avg_price'] : 0;
        $restaurant['minPrice'] = isset($rnrObj['min_price']) ? $rnrObj['min_price'] : 0;
        $restaurant['maxPrice'] = isset($rnrObj['max_price']) ? $rnrObj['max_price'] : 0;

        $restaurant['menu_url'] = '';
        if (isset($restaurant['catalogs']) && count($restaurant['catalogs'])) {
            $restaurant['menu_url'] = $this->_container->get('common.utils')->generateMediaUrl($restaurant['catalogs'][0]['media_id'], 'catalogue', 'pdf');
        }

        $restaurant['name'] = isset($restaurant['trading_name']) ? $restaurant['trading_name'] : '';
        $restaurant['trading_name'] = $restaurant['name'];
        $restaurant['company_name'] = isset($restaurant['company_name']) ? $restaurant['company_name'] : '';
        $restaurant['outlet_name'] = isset($restaurant['outlet_name']) ? $restaurant['outlet_name'] : '';
        if ($restaurant['name'] == '') {
            $restaurant['name'] = $restaurant['company_name'];
        }
        //Add secondary status prefix
        $restaurant['secondaryStatus'] = isset($restaurant['status']) && $restaurant['status'] != 'Open' ? '(' . $restaurant['status'] . ')' : '';
        if ($restaurant['secondaryStatus'] != '') {
            $restaurant['name'] = $restaurant['secondaryStatus'] . ' ' . $restaurant['name'];
        }
        if ($restaurant['outlet_name'] != '') {
            $restaurant['name'] = $restaurant['name'] . ' (' . $restaurant['outlet_name'] . ')';
        }

        $restaurant['booking_id'] = isset($restaurant['tabledb']) ? $restaurant['tabledb']['restaurant_id'] : 0;

        $restaurant['street'] = isset($restaurant['address']) ? trim(join(' ', array($restaurant['address']['block_number'], $restaurant['address']['street_name']))) : '';
        $restaurant['build_name'] = isset($restaurant['address']) ? trim(join(' ', array($restaurant['address']['unit_number'], $restaurant['address']['building_name']))) : '';
        $address_arr = array($restaurant['address']['unit_number'], $restaurant['address']['building_name'], $restaurant['address']['block_number']);
        $address_arr = array_filter($address_arr, function ($item) {
            return $item != '' ? true : false;
        });
        //$restaurant['long_address'] = isset($restaurant['address']) ? trim(join(', ', $address_arr) . ' ' . $restaurant['address']['street_name']) : '';
        $restaurant['long_address'] = LocaleParser::getInstance()->parseBusinessAddress($restaurant, $country_code, false);
        $restaurant['long_address_breakline'] = LocaleParser::getInstance()->parseBusinessAddress($restaurant, $country_code, true);

        $restaurant['neighbourhood'] = isset($restaurant['address']['neighbourhood']) ? $restaurant['address']['neighbourhood'] : '';
        $restaurant['state'] = isset($restaurant['address']['state']) ? $restaurant['address']['state'] : '';

        if($restaurant['state'] == '')
        {
            $state_default_locales = $this->_container->getParameter('country_route_path_locale');
            $restaurant['state'] = $state_default_locales[$country_code];
        }
        else {
            $restaurant['state'] = Common::convertToSlug($restaurant['state']);
        }

        $restaurant['state_name'] = isset($restaurant['address']['state']) ? $restaurant['address']['state'] : '';
        if($restaurant['state_name'] == '')
        {
            $state_default_locales = $this->_container->getParameter('country_route_path_locale');
            $restaurant['state_name'] = $state_default_locales[$country_code];
        }

        $restaurant['phone_number'] = isset($restaurant['phone_number']) ? $restaurant['phone_number'] : '';

        $restaurant['fax_number'] = isset($restaurant['fax_number']) ? $restaurant['fax_number'] : '';
        $restaurant['virtual_number'] = isset($restaurant['phone_number']) ? Common::removeRegionNumber($restaurant['phone_number']) : '';
        $restaurant['tagline'] = isset($restaurant['tagline']) ? $restaurant['tagline'] : '';

        $restaurant['emails'] = isset($restaurant['emails']) ? $restaurant['emails'] : array();
        $restaurant['images'] = isset($restaurant['images']) ? $restaurant['images'] : null;

        $restaurant['remove_competitor_banner'] = isset($restaurant['hgw']['remove_competitor_banner']) ? $restaurant['hgw']['remove_competitor_banner'] : 0;

        $restaurant['type_place'] = '';
        $restaurant['specialties'] = array();
        $restaurant['dishes'] = array();
        $restaurant['suitablefor'] = array();
        $restaurant['sponsorship'] = array();

        $restaurant['displayed_phones'] = isset($restaurant['displayed_phones']) ? $restaurant['displayed_phones'] : array();
        $restaurant['phones_out_region'] = !empty($restaurant['virtual_number']) ? (in_array($restaurant['virtual_number'], $restaurant['displayed_phones']) ? $restaurant['displayed_phones'] : array($restaurant['virtual_number'])) : $restaurant['displayed_phones'];
        array_walk($restaurant['phones_out_region'], function(&$value) {
            Common::removeRegionNumber($value);
        });
        /**
        * add country code before phone number
         * @author tri.van
         */
        $countryCodePlusForPhone = '';
        if($country_code == Constant::COUNTRY_CODE_SINGAPORE) {
        	$countryCodePlusForPhone = '+65 ';
        }
        else if($country_code == Constant::COUNTRY_CODE_MALAYSIA) {
        	$countryCodePlusForPhone = '+60 ';
        }
        if($restaurant['phones_out_region'] && is_array($restaurant['phones_out_region'])) {
        	$arrayPhoneTmp = array();
        	foreach ($restaurant['phones_out_region'] as $key=> $phone) {
        		if($phone) {
        			$arrayPhoneTmp[] = $countryCodePlusForPhone.$phone;
        		}
        	}
        	$restaurant['phones_out_region'] = $arrayPhoneTmp;
        }

        $cuisine = array();
        $type_of_place = array();
        $dishes = array();
        $specialties = array();

        if (isset($restaurant['properties']) && count($restaurant['properties'])) {
            $cuisine = $restaurant['properties']['cuisine'];
            $type_of_place = $restaurant['properties']['type_of_place'];
            $dishes = $restaurant['properties']['dishes'];
            $specialties = $restaurant['properties']['specialties'];
            if (isset($restaurant['properties']['sponsorship']) && $restaurant['properties']['sponsorship']) {
                $restaurant['sponsorship'] = $restaurant['properties']['sponsorship'];
            }
        }

        $restaurant['cuisine'] = $cuisine;
        $restaurant['type_place'] = $type_of_place;
        $restaurant['dishes'] = $dishes;
        $restaurant['specialties'] = $specialties;

        $restaurant['open_hours'] = '';
        $close_days = array();
        $open_days = array();
        if (isset($restaurant['business_hours'])) {
            $strList = '';

            $close_days = array();
            foreach ($restaurant['business_hours'] as $value) {
                if(!$value['is_opened'])
                {
                    if($value['from_day'] != '')
                    {
                        $close_days[] = $value['from_day'];
                    }
                    continue;
                }
                $open_days[] = $value;

                // [IBL] Display opening hours for restaurants open 24/7
                // https://jira.insing.com/jira/browse/HGW-495
                if (isset($value['from_hour']) && $value['from_hour'] == '24 Hours') {
                    $str = '<p>';
                    // Day
                    $str .= $value['from_day'];
                    if(isset($value['to_day']) && $value['to_day']){
                        $str .= ' - ' . $value['to_day'];
                    }
                    $str .= ': ';
                    // Hour
                    $str .= $value['from_hour'];
                    $strList .= $str . '</p>';
                } else {
                    if(isset($value['from_day']) && $value['from_day']
                            && isset($value['from_hour']) && $value['from_hour']
                            && isset($value['to_hour']) && $value['to_hour'])
                    {
                        $str = '<p>';
                        // Day
                        $str .= $value['from_day'];
                        if(isset($value['to_day']) && $value['to_day']){
                            $str .= ' - ' . $value['to_day'];
                        }
                        $str .= ': ';
                        // Hour
                        $str .= $value['from_hour'];
                        $str .= ' - ' . $value['to_hour'];
                        $strList .= $str . '</p>';
                    }
                }
            }

            // add more infor to open hours
            if(isset($restaurant['others_info']) && $restaurant['others_info']){
                $restaurant['others_info'] = nl2br($restaurant['others_info']);
                $strList .= '<p>'.$restaurant['others_info'].'</p>';
            }

            if(!empty($close_days)) {
                $strHoursClosed = implode(', ', $close_days);
                $strList .= '<p>Closed: ' . $strHoursClosed . '</p>';
            }

            $restaurant['open_hours'] = $strList;
        }

        $restaurant['open_days'] = $open_days;
        $restaurant['close_days'] = $close_days;

        $public_holiday = $this->_container->getParameter('public_holidays');
        $public_holiday = isset($public_holiday[$this->_country_code]) && !empty($public_holiday[$this->_country_code]) ? explode(',', $public_holiday[$this->_country_code]) : array();

        /*
         * Don't use this, The ticket is HGW-366
         * Also change function HgwUtils::timeslots_is_availables()
        $open_hours = $this->getOpenAvailables($restaurant, $public_holiday);
        $restaurant['hours_available'] = $open_hours['hours_available'];
        $restaurant['hours_unavailable'] = $open_hours['hours_unavailable'];
        $restaurant['hours_unavailable_str'] = implode(',', $open_hours['hours_unavailable']);
        */
        $restaurant['hours_available'] = array();
        $restaurant['hours_unavailable'] = array();
        $restaurant['hours_unavailable_str'] = '';

        $restaurant['banner_url'] = isset($restaurant['hgw']['banner_resource_id']) && $restaurant['hgw']['banner_resource_id'] ? $this->_container->get('common.utils')->generateMediaUrl($restaurant['hgw']['banner_resource_id'], 'pc_640x120') : '';
        $restaurant['management_intro'] = $restaurant['management_intro'];

        $restaurant['mrt_stations'] = isset($restaurant['mrt_stations']) && count($restaurant['mrt_stations']) ? $restaurant['mrt_stations'][0] : '';

        $restaurant['group'] = isset($restaurant['groups']) && is_array($restaurant['groups']) && count($restaurant['groups']) ? $restaurant['groups'][0]['name'] : '';

        $restaurant['description'] = isset($restaurant['description']) ? $restaurant['description'] : '';

        $restaurant['websites'] = isset($restaurant['websites']) && count($restaurant['websites']) ? $restaurant['websites'] : array();

        $restaurant['other_language_name'] = isset($restaurant['other_language_name']) ? $restaurant['other_language_name'] : '';
        $restaurant['reservations_hotline'] = isset($restaurant['reservations_hotline']) && $restaurant['reservations_hotline']
        ? $countryCodePlusForPhone.$restaurant['reservations_hotline'] : '';

        $restaurant['hot_deals'] = array();
        if (isset($restaurant['hot_deals'])) {
            $i = 0;
            $hotDeals = array();
            foreach ($restaurant['hot_deals'] as $value) {
                $hotDeals[$i]['no'] = $i + 1;
                $hotDeals[$i]['title'] = $value['name'];
                $hotDeals[$i]['description'] = $value['description'];
                //$hotDeals[$i]['date'] = $value->startDate . ' - ' . $value->endDate;

                $sDate = new \DateTime($value['start_date']);
                $eDate = new \DateTime($value['end_date']);

                $hotDeals[$i]['start_date'] = $sDate;
                $hotDeals[$i]['end_date'] = $eDate;
                $hotDeals[$i]['bankName'] = $value['bank_name'] != 'NO_BANK_NAME' ? $value['bank_name'] : '';
                $hotDeals[$i]['remark'] = $value['remarks'];
                if (isset($value['credit_cards']) && count($value['credit_cards'])) {
                    $hotDeals[$i]['logo'] = !empty($value['credit_cards'][0]['logo_resource_id']) ? $this->_container->get('common.utils')->generateMediaUrl($value['credit_cards'][0]['logo_resource_id'], 'pc_143x107') : $this->_container->get('router')->getContext()->getScheme() . '://' . $this->_container->get('router')->getContext()->getHost() . $this->_container->get('templating.helper.assets')->getUrl('bundles/hgw/img/hotdeal_default.jpg');
                    $hotDeals[$i]['logo_title'] = $value['credit_cards'][0]['name'];
                }
                $i++;
            }
            $restaurant['hot_deals'] = $hotDeals;
        }

        $restaurant['image_url'] = $restaurant['image_url_id'] = '';
        $restaurant['no_image'] = true;
        $restaurant['image_urls'] = array();
        if (!empty($restaurant['default_image_media_path'])) {
            $restaurant['image_url'] = $this->_container->get('common.utils')->generatePhotoFromPath($restaurant['default_image_media_path'], $image_size, $image_size);
            $restaurant['no_image'] = false;
            foreach ($restaurant['images'] as $media) {
                if($restaurant['default_image_media_path'] == $media['media_path']){
                    $restaurant['image_url_id'] = $media['media_id'];
                }
            }
        } else {
            if (!empty($restaurant['images'])) {
                foreach ($restaurant['images'] as $media) {
                    if (!empty($media['media_path'])) {
                        $restaurant['image_url'] = $this->_container->get('common.utils')->generatePhotoFromPath($restaurant['media_path'], $image_size, $image_size);
                        $restaurant['image_url_id'] = $media['media_id'];
                        $restaurant['no_image'] = false;
                        break;
                    }
                }
            }
        }

        // logo's restaurant
        $restaurant['logo'] = $restaurant['logo_for_facebook'] = '';
        if (isset($restaurant['logo']['media_id']) && $restaurant['logo']['media_id']) {
            $restaurant['logo_url'] = $this->_container->get('common.utils')->generateMediaUrl($restaurant['logo']['media_id'], 'pc_100x75');
            $restaurant['logo_for_facebook'] = $this->_container->get('common.utils')->generateMediaUrl($restaurant['logo']['media_id'], 'pc_300x225');
        }

        if (isset($restaurant['videos']) && count($restaurant['videos'])) {
            $restaurant['video_thumb_default'] = $this->_container->get('common.utils')->generateMediaUrl($restaurant['videos'][0], 'pc_295x225');
            $restaurant['video_url_default'] = $this->_container->get('common.utils')->generateMediaUrl($restaurant['videos'][0], 'pc_264_aac_320x240', 'mp4');
        }

        $restaurant['slug'] = $restaurant['hgw']['hgw_url'];

        // Parse url
        $restaurant['url'] = '';
        if($restaurant['slug']) {
            $restaurant['url'] = $this->_container->get('common.utils')
                ->generateIblUrl($restaurant['state'], $country_code, $restaurant['slug']);
        }
        $restaurant['latitude'] = isset($restaurant['latitude']) ? $restaurant['latitude'] : 0;
        $restaurant['longitude'] = isset($restaurant['longitude']) ? $restaurant['longitude'] : 0;

        $rnrObj['percent'] = 0;

        if (isset($rnrObj['vote_count']) && $rnrObj['vote_count'] > 0) {
            if ($rnrObj['thumbs_up_count'] > 0) {
                $percent = (intval($rnrObj['thumbs_up_count']) * 100) / intval($rnrObj['vote_count']);
                $rnrObj['percent'] = ceil($percent);
            }
        }
        $restaurant['statistic'] = $rnrObj;


        $restaurant['suitable_fors'] = array();
        $restaurant['recommendations'] = array();
        if (isset($rnrObj['suitable_fors']) && count($rnrObj['suitable_fors'])) {
            $arrSort = array();
            foreach ($rnrObj['suitable_fors'] as $key => $value) {
                $arrSort[$key] = $value['count'];
            }
            array_multisort($arrSort, SORT_DESC, $rnrObj['suitable_fors']);
            $restaurant['suitable_fors'] = $rnrObj['suitable_fors'];
        }
        if (isset($rnrObj['recommendations']) && count($rnrObj['recommendations'])) {
            $arrSort = array();
            foreach ($rnrObj['recommendations'] as $key => $value) {
                $arrSort[$key] = $value['count'];
            }
            array_multisort($arrSort, SORT_DESC, $rnrObj['recommendations']);
            $restaurant['recommendations'] = $rnrObj['recommendations'];
        }

        $restaurant['time_slots'] = isset($restaurant['time_slots']) ? $restaurant['time_slots'] : array();

        return $restaurant;
    }

    public function parsePromotions($restaurant, $sort = true) {

        if(empty($restaurant))
        {
            return array();
        }
        $cacheKey = $this->_country_code . '_' . self::IBL_PROMOTIONS_CACHE . $restaurant['id'];
        $cacheKey = $sort ? $cacheKey . '_SORTED' : $cacheKey;

        $results = $this->restaurantCache->fetch($cacheKey);
        if (!$results && isset($restaurant['promos']) && count($restaurant['promos'])) {
            $results = array();
            $proms = $restaurant['promos'];

            foreach ($proms as $key => $value) {
                if (!empty($value)) {

                    array_walk($value, function(&$value, $key_child, $key) {
                        $arr_sort = array('special' => 3, 'lastmin' => 2, 'daily' => 1);
                        $value['is_spec'] = ($key == 'special' ? 1 : 0);
                        $value['startTimestamp'] = strtotime($value['start_date']);
                        $value['endTimestamp'] = strtotime($value['end_date']);
                        $value['description'] = isset($value['description']) ? $value['description'] : '';
                        $value['sort'] = $arr_sort[$key] . $value['startTimestamp'];
                    }, $key);
                    $results = array_merge($results, $value);
                }
            }

            if ($sort) {
                $arrFieldSort = array();
                foreach ($results as $value) {
                    $arrFieldSort[] = $value['sort'];
                }
                array_multisort($arrFieldSort, SORT_DESC, $results);
            }
            if (count($results)) {
                $this->restaurantCache->save($cacheKey, $results);
            }
        }
        return $results;
    }

    public function getAutoCompleteTradingName($name) {
        $country_biz_listing = $this->_container->getParameter('business_search_api');
        $this->_business_seach_info = $country_biz_listing[$this->_country_code];
        $url = $this->_business_seach_info['autocomplete_url'];

        $url = $url . str_replace(' ', '%20', (trim(urlencode($name))));
        $session_token = $this->getSessionToken();
        if(!$session_token)
        {
            $this->_logger->error('Session token is null');
        }

        $fields = array(
            'session_token' => $session_token,
            'filter_category' => Constant::API_BIZ_FILTER_CATEGORY
        );
        $results = $this->doGetJsonWrapper($url, $this->_business_seach_info['consumer_secret'], $fields);

        return $results;

        // format output from api call after change
        // from https://api.search.insing.com/5.0/autocomplete/name/{name}
        // to   https://api.search.insing.com/5.0/autocomplete/trading_name/{name}
        $tradingNameList = array();
        if(!empty($results) && !isset($results['error'])) {
            foreach ($results as $item) {
                if (isset($item['trading_name'])) {
                    $tradingName = $item['trading_name'];
                    if (isset($item['outlet_name']) && trim($item['outlet_name']) != '') {
                        $tradingName = $tradingName . " (" . trim($item['outlet_name']) . ")";
                    }
                    $tradingNameList[] = $tradingName;
                }
            }
        }

        return array_unique($tradingNameList);
    }

    public function parseAutoCompleteTradingName($results) {
        $ret = array();
        if(!empty($results) && !isset($results['error'])) {
            $ret = $results;
        }

        return $ret;
    }

    public function getRestaurantPromotions($bookingId, $sort = false) {
        $cacheKey = $this->_country_code . '_' . self::IBL_PROMOTIONS_CACHE . $bookingId;
        $cacheKey = $sort ? $cacheKey . '_SORTED' : $cacheKey;

        $results = $this->restaurantCache->fetch($cacheKey);

        if (!$results) {
            $results = array();
            $promotionUrl = $this->_container->getParameter('booking_get_promotion_details');
            $promotionUrl = str_replace('{restaurant_id}', $bookingId, $promotionUrl);

            $promotions = $this->getPromotionList($promotionUrl);

            if (isset($promotions['data']) && !empty($promotions['data'])) {
                array_walk($promotions['data'], function(&$value, $key_child) {
                    $sort = 1;
                    if (!$value['editable']) {
                        $sort = 3;
                    } elseif ($value['lastMinute']) {
                        $sort = 2;
                    }

                    $value['is_spec'] = ($value['editable'] ? 0 : 1);
                    $value['startTimestamp'] = ($value['startTimestamp'] / 1000);
                    $value['endTimestamp'] = ($value['endTimestamp'] / 1000);
                    $value['description'] = isset($value['description']) ? $value['description'] : '';
                    $value['sort'] = $sort . $value['startTimestamp'];
                });
                if ($sort) {
                    $arrFieldSort = array();
                    foreach ($promotions['data'] as $value) {
                        $arrFieldSort[] = $value['sort'];
                    }
                    array_multisort($arrFieldSort, SORT_DESC, $promotions['data']);
                }
                $results = $promotions['data'];
            }

            $this->restaurantCache->save($cacheKey, $results);
        }

        return $results;
    }

    public function getRecommendationsOfBusiness($business_id) {
        $cacheTMTConstant = $this->_country_code . '_' . self::IBL_RESTAURANT_TOPMUSTTRY . $business_id;
        $itemRecommendations = $this->restaurantCache->fetch($cacheTMTConstant);

        if (!$itemRecommendations) {
            // Get MustTry
            try {
                $listChannel = $this->_container->getParameter('rnr_channel');
                $itemRecommendations = $this->_container->get('rnr.api')->getItemRecommendationListApi($listChannel[$this->_country_code], $business_id, array(
                            'per_page' => 200, //Get max 200 items from API
                            'keyword' => ''
                ));
            } catch (\Exception $exc) {
                $itemRecommendations = array();
            }

            if (isset($itemRecommendations) && count($itemRecommendations)) {
                $this->restaurantCache->save($cacheTMTConstant, $itemRecommendations, $this->_container->getParameter('business_details_cache_time'));
            }
        }
        return $itemRecommendations;
    }

    public function getReviewsOfBusiness($business_id, $itemPerPage, $page = 1) {
        $cacheRConstant = $this->_country_code . '_' . self::IBL_RESTAURANT_REVIEW . $business_id . $page;

        $reviewsOfIBL = $this->restaurantCache->fetch($cacheRConstant);
        if (!$reviewsOfIBL) {
            $reviewsOfIBL['reviews'] = array();
            $reviewsOfIBL['total'] = 0;
            try {
                $results = $this->_container->get('rnr.api')->getLatestSubmissions('', $itemPerPage, $page, $business_id, ReviewUtils::SUBMISSION_TYPE_REVIEW);
            } catch (\Exception $exc) {
                $results = array();
            }
            if (!empty($results) && count($results['submissions'])) {
                $re = array();
                foreach ($results['submissions'] as $review) {
                    $reTemp = HgwUtils::parseSubmission($review, $this->_container);
                    if (count($reTemp)) {
                        $re[] = $reTemp;
                    }
                }
                $reviewsOfIBL['reviews'] = $re;
                $reviewsOfIBL['total'] = isset($results['total']) ? $results['total'] : 0;

                if (count($reviewsOfIBL['reviews']) && $reviewsOfIBL['total']) {
                    $this->restaurantCache->save($cacheRConstant, $reviewsOfIBL, $this->_container->getParameter('business_details_cache_time'));
                }
            }
        }
        return $reviewsOfIBL;
    }

    public function getFavouriteReviewsOfBusiness($business_id) {
        // Favourite review
        $results = array();

        $cacheFRConstant = $this->_country_code . '_' . self::IBL_RESTAURANT_FAVOURITE_REVIEW . $business_id;
        $resFavouriteReview = $this->restaurantCache->fetch($cacheFRConstant);

        if (!$resFavouriteReview) {
            // Get one favourite review
            try {
                $results = $this->_container->get('rnr.api')->getLatestSubmissions('', 1, 1, $business_id, ReviewUtils::SUBMISSION_TYPE_FAVOURITE);
            } catch (\Exception $exc) {
                $results = array();
            }
            if (!empty($results) && count($results['submissions'])) {
                $resFavouriteReview = $results['submissions'][0];
                $resFavouriteReview = HgwUtils::parseSubmission($resFavouriteReview, $this->_container);
            }

            if (count($resFavouriteReview)) {
                $this->restaurantCache->save($cacheFRConstant, $resFavouriteReview, $this->_container->getParameter('business_details_cache_time'));
            }
        }
        return $resFavouriteReview;
    }

    public function getSponsorsOfBusiness($cuisine, $business_slug) {
        $cacheKey = $this->_country_code . '_' . $this->IBL_RESTAURANT_DETAIS . '_SPONSORS_' . $business_slug;
        $results_sponsor = $this->restaurantCache->fetch($cacheKey);

        if (!$results_sponsor) {
            $params = array('filter_category' => 'Food & Drink', 'filter_cuisine' => implode('|', $cuisine), 'filter_remove_competitor_banner' => 'Y', 'page' => 1, 'per_page' => 20);
            try {
                $results_sponsor = $this->parseBusinessSearch($params, false, false, false, '100x75');
                $groups = array();
                foreach ($results_sponsor['results'] as $key => $value) {
                    array_walk($value['groups'], function(&$value) {
                        $value = $value['name'];
                    });
                    $spon_groups = md5(implode('_', $value['groups']));
                    if (!in_array($spon_groups, $groups)) {
                        $groups[] = $spon_groups;
                        if ($value['slug'] == $business_slug) {
                            unset($results_sponsor['results'][$key]);
                        }
                    } else {
                        unset($results_sponsor['results'][$key]);
                    }
                }

                if (count($results_sponsor['results']) > 3) {
                    array_splice($results_sponsor['results'], 3);
                }
                if (count($results_sponsor['results'])) {
                    $this->restaurantCache->save($cacheKey, $results_sponsor, $this->_container->getParameter('business_details_cache_time'));
                }
            } catch (\Exception $exc) {
                $results_sponsor['results'] = array();
            }
        }
        return $results_sponsor['results'];
    }

    public function getLatestVotesOfUserOnBusiness($userId, $business_id) {

        $result = array();
        try {
            $resultsOnUser = $this->_container->get('rnr.api')->getLatestSubmissions('', 1, 1, $business_id, ReviewUtils::SUBMISSION_TYPE_ALL, $userId);
        } catch (\Exception $exc) {
            $resultsOnUser = array();
        }
        if (!empty($resultsOnUser) && count($resultsOnUser['submissions'])) {
            foreach ($resultsOnUser['submissions'] as $review) {
                $reTemp = HgwUtils::parseSubmission($review, $this->_container);
                if (count($reTemp)) {
                    $result['id'] = key_exists('id', $reTemp) ? $reTemp['id'] : 0;
                    $result['thumbs_up'] = key_exists('vote', $reTemp) ? $reTemp['vote']['thumbs_up'] : null;
                    $result['review_id'] = key_exists('review_id', $reTemp) ? $reTemp['review_id'] : 0;
                    break;
                }
            }
        }
        return $result;
    }

    public function searchBusinessForModuleAdmin($keyWord, $reservationOnline = false, $extParams = array()) {

        $arrayResult = array();
        $params = array('term' => $keyWord, 'page' => 1, 'per_page' => 20);
        if ($reservationOnline) {
            $params['filter_dining_reservation_url'] = 'Y';
        }
        if (count($extParams)) {
            $params = array_merge($params, $extParams);
        }
        if (!isset($params['filter_category'])) {
            $params['filter_category'] = 'Food & Drink';
        }
        $businessResults = $this->searchForBusinesses($params);
        if (isset($businessResults['data']['results'])) {
            $businessResults = $businessResults['data']['results'];
        } else {
            $businessResults = array();
        }

        foreach ($businessResults as $key => $value) {
            $value = $value['details'];
            array_walk($value['address'], function(&$val) {
                $val = is_array($val) ? $val : html_entity_decode($val, ENT_QUOTES);
            });
            //Add missing data to synchronize in twig data
            $arrayResult[$key]['restaurantId'] = isset($value['id']) ? $value['id'] : 0;
            ;
            $arrayResult[$key]['restaurantName'] = isset($value['trading_name']) ? $value['trading_name'] : '';
            $arrayResult[$key]['description'] = isset($value['description']) ? $value['description'] : '';
            $arrayResult[$key]['address'] = isset($value['address']) ? $value['address']['street_name'] : '';
            $arrayResult[$key]['neighbourHood'] = isset($value['address']) ? $value['address']['neighbourhood'] : '';
            $arrayResult[$key]['outletName'] = isset($value['outlet_name']) ? " - " . $value['outlet_name'] : '';

            $address_arr = array($value['address']['unit_number'], $value['address']['building_name'], $value['address']['block_number']);
            $address_arr = array_filter($address_arr, function ($item) {
                return $item != '' ? true : false;
            });
            $longAddress = isset($value['address']) ? trim(join(', ', $address_arr) . ' ' . $value['address']['street_name']) : '';

            $arrayResult[$key]['longAddress'] = $longAddress;
            $arrayResult[$key]['imageUrl'] = '';
            $arrayResult[$key]['contentType'] = 'business';
        }
        return $arrayResult;
    }

    public function getTableDBMapping(array $business_id, array $restaurant_id = array(), $country_code = '') {
        $arrayResult = array();

        if($country_code != '')
        {
            $country_biz_listing = $this->_container->getParameter('business_search_api');
            $this->_business_seach_info = $country_biz_listing[$country_code];
        }

        $url = $this->_business_seach_info['tabledb_mapping_url'];

        $session_token = $this->getSessionToken();

        $fields = array(
            'session_token' => $session_token,
        );

        if (!empty($business_id)) {
            $fields['business_id'] = implode(',', $business_id);
        }
        else if (!empty($restaurant_id)) {
            $fields['restaurant_id'] = implode(',', $restaurant_id);
        }
        else
        {
            return array();
        }

        $arrayResult = $this->doGetWrapper($url, $this->_business_seach_info['consumer_secret'], $fields);
        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $arrayResult['status'])
            return array();

        return isset($arrayResult['data']['results']) ? $arrayResult['data']['results'] : array();
    }

    /**
     *
     * @param array $restaurant
     * @param array $public_holiday
     * @return array
     *  // If this array contains date(Y-m-d), it is close day
      // If day of week have no hours range, it's close day
      // If day of week have any hours range, that ranges are available hours
     */
    public function getOpenAvailables($restaurant, array $public_holiday) {

        $open_hours = $restaurant['open_days'];
        $close_hours = $restaurant['close_days'];

        $open_hours = $this->parse_open_days($open_hours, $public_holiday);
        $close_hours = $this->parse_close_days($close_hours, $public_holiday);

        $close_temp = array();
        foreach ($open_hours as $key => $value) {
            if (empty($value) || !$value) {
                unset($open_hours[$key]);
                $close_temp[] = $key;
            }
        }
        if (!empty($open_hours)) {
            $close_hours = array_merge($close_hours, $close_temp);
        }

        return array('hours_available' => $open_hours, 'hours_unavailable' => $close_hours);
    }

    private function parse_open_days($open_hours, $public_holiday) {
        $dayofWeek = array('Mon' => 0, 'Tue' => 0, 'Wed' => 0, 'Thu' => 0, 'Fri' => 0, 'Sat' => 0, 'Sun' => 0);
        $public_holidays_open = array();
        if (!empty($open_hours)) {
            /*
             * array items as
             *  {
             *    [from_hour] => 11:30
              [from_day] => Tue( or Daily)
              [to_hour] => 14:30
              [to_day] => Fri
             *  }
             */

            $func = function(&$value, $key, &$ext_data) {

                $value = is_array($value) ? $value : array();
                $data = $ext_data['data'];
                $from_day = !empty($data['from_day']) ? $data['from_day'] : '';

                if (strtolower($from_day) == 'daily') {
                    $hour_open = array('open' => !empty($data['from_hour']) ? $data['from_hour'] : '',
                        'close' => !empty($data['to_hour']) ? $data['to_hour'] : '');
                    $value[] = $hour_open;
                    return;
                }

                // Exist if before is "to_day"
                if ($ext_data['is_to']) {
                    return;
                }
                // This is "from_day"
                if (strtolower($from_day) == strtolower($key)) {
                    $ext_data['is_from'] = true;
                }

                if ($ext_data['is_from']) {
                    $hour_open = array('open' => !empty($data['from_hour']) ? $data['from_hour'] : '',
                        'close' => !empty($data['to_hour']) ? $data['to_hour'] : '');
                    $value[] = $hour_open;

                    $to_day = !empty($data['to_day']) ? $data['to_day'] : '';
                    if (strtolower($to_day) == strtolower($key)) {
                        $ext_data['is_to'] = true;
                    }
                }
            };

            foreach ($open_hours as $value) {

                // Check from_day with Day & Day, PH, PH & Day, ...
                if (!empty($value['from_day']) && (stripos($value['from_day'], '&') !== false || stripos($value['from_day'], 'PH') !== false)) {
                    $ready = str_replace(array(' & ', ', ', '&', ','), '-', $value['from_day']);
                    $daysPH = explode('-', $ready);

                    $hour_open = array(array('open' => !empty($value['from_hour']) ? $value['from_hour'] : '',
                            'close' => !empty($value['to_hour']) ? $value['to_hour'] : ''));
                    $daysPH = array_fill_keys($daysPH, $hour_open);
                    $public_holidays_open = array_merge_recursive($public_holidays_open, $daysPH);
                    continue;
                }
                $from = false;
                $to = false;
                array_walk($dayofWeek, $func, array('data' => $value, 'is_from' => $from, 'is_to' => $to));
            }

            array_walk($public_holiday, function (&$value, $key) {
                $value = date('Y') . '-' . $value;
            });

            if (array_key_exists('PH', $public_holidays_open)) {
                $public_holiday_temp = array_fill_keys($public_holiday, $public_holidays_open['PH']);
                $public_holidays_open = array_merge($public_holidays_open, $public_holiday_temp);
                unset($public_holidays_open['PH']);
            }

            // Check if close date have "Eve of PH"
            if (array_key_exists('Eve of PH', $public_holidays_open)) {
                array_walk($public_holiday, function (&$value, $key) {
                    $before_date = new \DateTime($value);
                    $before_date = $before_date->sub(new \DateInterval('P1D'));
                    $value = $before_date->format('Y-m-d');
                });

                $public_holiday = array_fill_keys($public_holiday, $public_holidays_open['Eve of PH']);
                $public_holidays_open = array_merge($public_holidays_open, $public_holiday);
                unset($public_holidays_open['Eve of PH']);
            }
        }

        return array_merge_recursive($dayofWeek, $public_holidays_open);
    }

    private function parse_close_days($close_hours, $public_holiday) {

        $close_days = array();
        // Check close days and public holiday
        if (!empty($close_hours)) {
            $close_days_parse = array();

            array_walk($close_hours, function (&$value, $key) use(&$close_days_parse) {
                // Parse from "Eve of PH & PH" or "Sat, Sun & PH" or "Sat & Sun"
                $ready = str_replace(array(' & ', ', ', '&', ','), '-', $value);
                $daysPH = explode('-', $ready);
                $close_days_parse = array_merge($close_days_parse, $daysPH);
            });

            $close_days = array_fill_keys($close_days_parse, array());

            array_walk($public_holiday, function (&$value, $key) {
                $value = date('Y') . '-' . $value;
            });

            if (array_key_exists('PH', $close_days)) {
                $public_holiday_temp = array_fill_keys($public_holiday, 'close');
                $close_days = array_merge($close_days, $public_holiday_temp);
                unset($close_days['PH']);
            }

            // Check if close date have "Eve of PH"
            if (array_key_exists('Eve of PH', $close_days)) {
                array_walk($public_holiday, function (&$value, $key) {
                    $before_date = new \DateTime($value);
                    $before_date = $before_date->sub(new \DateInterval('P1D'));
                    $value = $before_date->format('Y-m-d');
                });

                $public_holiday = array_fill_keys($public_holiday, 'close');
                $close_days = array_merge($close_days, $public_holiday);
                unset($close_days['Eve of PH']);
            }
        }
        return array_keys($close_days);
    }

    /**
     * Get Promotion List
     *
     * @author Vu Tran
     * @param string $promotionUrl
     */
    public function getPromotionList($promotionUrl) {
        return $this->doCurlGet($promotionUrl);
    }

    public function parseTimeSlots($time_slots, $filter_pax_time, $num_time_slot = 4) {
        // https://jira.insing.com/jira/browse/FOOD-1724
        // The search result will arrange time slots according to this order:
        // [Slot 1] [Slot 2] [Slot 3] [Slot 4] [More..]
        //
        // Slot 1 = Search Time - 30 mins
        //   If Search Time - 30 mins not available, then try Search Time - 15mins instead.
        //   If both not available, then display Search Time - 30mins with disabled button.
        // Slot 2 = current search time
        // Slot 3 = Slot 2 + 30mins
        //   If Slot 2 + 30 mins not available, then try Slot 2 + 15 mins instead.
        //   If both not available, then display Slot2 + 30mins but with disabled button
        // Slot 4 = Slot 2 + 60mins
        //   If Slot 2 + 60 mins not available, then try Slot 2 + 45 mins instead.
        //   If both not available, then display Slot2 + 60mins disabled time slots

        // where Search time is:
        // For specific DateTimePax search, search time is the queried time
        // For generic search, Current Time - Use current Singapore Time + 30 mins then round forward to 30 mins interval
        //   If current time is 3.05PM, it should search for 4.00PM instead.
        // If filter pax/date/time
        if($filter_pax_time) {
            // Slot 2 = queried time
            $slot2Time = $filter_pax_time->format('Hi');
        } else {
            // Slot 2 = current search time
            $slot2Time = $this->roundTimeSlotUpWithExtraTime(date('Hi'));
        }

        $slot2TimeArr = explode(':', substr_replace($slot2Time, ':', 2, 0));
        $slot2TimeTotal = $slot2TimeArr[0] * 60 + $slot2TimeArr[1];
        // Slot 1 = Search Time - 30 mins
        $slot1Time30 = sprintf("%02s", floor(($slot2TimeTotal + 1440 - 30) / 60) % 24) . sprintf("%02s", ($slot2TimeTotal + 1440 - 30) % 60);
        $slot1Time15 = sprintf("%02s", floor(($slot2TimeTotal + 1440 - 15) / 60) % 24) . sprintf("%02s", ($slot2TimeTotal + 1440 - 15) % 60);
        // Slot 3 = Slot 2 + 30mins
        $slot3Time30 = sprintf("%02s", floor(($slot2TimeTotal + 30) / 60) % 24) . sprintf("%02s", ($slot2TimeTotal + 30) % 60);
        $slot3Time15 = sprintf("%02s", floor(($slot2TimeTotal + 15) / 60) % 24) . sprintf("%02s", ($slot2TimeTotal + 15) % 60);
        // Slot 4 = Slot 2 + 60mins
        $slot4Time30 = sprintf("%02s", floor(($slot2TimeTotal + 60) / 60) % 24) . sprintf("%02s", ($slot2TimeTotal + 60) % 60);
        $slot4Time15 = sprintf("%02s", floor(($slot2TimeTotal + 45) / 60) % 24) . sprintf("%02s", ($slot2TimeTotal + 45) % 60);

        $slot1Info = array();
        $slot2Info = array();
        $slot3Info = array();
        $slot4Info = array();
        foreach ($time_slots as $value) {
            $timeSlot = isset($value['time_slot']) ? $value['time_slot'] : '';
            // Slot 1
            if ($timeSlot == $slot1Time30) {
                // If Search Time - 30 mins or 15mins is available
                if (count($slot1Info) == 0) {
                    $slot1Info = $this->setTimeSlotInfo($value);
                }
            }
            if ($timeSlot == $slot1Time15) {
                // If Search Time - 30 mins or 15mins is available
                if (count($slot1Info) == 0) {
                    $slot1Info = $this->setTimeSlotInfo($value);
                }
            }
            // Slot 2
            if ($timeSlot == $slot2Time) {
                $slot2Info = $this->setTimeSlotInfo($value);
            }

            // Slot 3
            if ($timeSlot == $slot3Time30 || $timeSlot == $slot3Time15) {
                // If Slot 2 + 30 mins or + 15 mins is available
                $slot3Info = $this->setTimeSlotInfo($value);
            }

            // Slot 4
            if ($timeSlot == $slot4Time30 || $timeSlot == $slot4Time15) {
                // If Slot 2 + 60 mins or + 45 mins is available
                $slot4Info = $this->setTimeSlotInfo($value);
            }
        }
        // disabled time slots
        if (count($slot1Info) == 0) {
            $slot1Info = array(
                'time_value' => $slot1Time30,
                'time_label' => substr_replace($slot1Time30, ':', 2, 0),
                'date' => '',
                'shift_id' => '',
                'notfully' => 0,
                'min_covers' => 0,
                'max_check_available' => 0
            );
        }
        if (count($slot2Info) == 0) {
            $slot2Info = array(
                'time_value' => $slot2Time,
                'time_label' => substr_replace($slot2Time, ':', 2, 0),
                'date' => '',
                'shift_id' => '',
                'notfully' => 0,
                'min_covers' => 0,
                'max_check_available' => 0
            );
        }
        if (count($slot3Info) == 0) {
            $slot3Info = array(
                'time_value' => $slot3Time30,
                'time_label' => substr_replace($slot3Time30, ':', 2, 0),
                'date' => '',
                'shift_id' => '',
                'notfully' => 0,
                'min_covers' => 0,
                'max_check_available' => 0
            );
        }
        if (count($slot4Info) == 0) {
            $slot4Info = array(
                'time_value' => $slot4Time30,
                'time_label' => substr_replace($slot4Time30, ':', 2, 0),
                'date' => '',
                'shift_id' => '',
                'notfully' => 0,
                'min_covers' => 0,
                'max_check_available' => 0
            );
        }

        $time_slots_30mins = array($slot1Info, $slot2Info, $slot3Info);
        if ($num_time_slot > 3) {
            $time_slots_30mins[] = $slot4Info;
        }

        return $time_slots_30mins;
    }

    public function parseTimeSlotsForSERP($time_slots, $filter_pax_time, $filter_pax, $num_time_slot = 4) {
        $slotInfo = array();
        if(!empty($time_slots)) {
            $numTimeSlot = 0;
            foreach($time_slots as $value) {
                $timeSlot = $this->setTimeSlotInfo($value);
                if($timeSlot['date'] && $filter_pax_time
                    && strtotime($timeSlot['date']) == strtotime($filter_pax_time->format('Y-m-d'))) {
                    if(!$timeSlot['notfully'] || ($filter_pax > 0 && ($filter_pax > $timeSlot['max_check_available'] || $filter_pax < $timeSlot['min_covers']))) {
                        continue;
                    } else {
                        $slotInfo[] = $timeSlot;
                        $numTimeSlot++;
                        if($numTimeSlot == $num_time_slot) {
                            break;
                        }
                    }
                }
            }
        }

        return $slotInfo;
    }



    public function getClosest($search, $arr, $key_get_value) {
      $closest = null;
      $key_item = null;
      foreach ($arr as $key => $item) {
        if ($closest == null || intval($search) == intval($item[$key_get_value]) || abs(intval($search) - intval($closest[$key_get_value])) >= abs(intval($item[$key_get_value]) - intval($search))) {
          $closest = $item;
          $key_item = $key;
        }
      }
      return array('key' => $key_item, 'timeslot' => $closest);
    }

    public function authenticate() {
        try {
            $result_authenticate = $this->doPostWrapper($this->_business_seach_info['authenticate_url'], $this->_business_seach_info['consumer_secret'], array(
                        'consumer_key' => $this->_business_seach_info['consumer_key'],
                        'consumer_secret' => $this->_business_seach_info['consumer_secret'],
                        'access_token' => $this->_business_seach_info['access_token'],
                        'access_token_secret' => $this->_business_seach_info['access_token_secret']
                            ));
            if (RestfulAPIHelper::HTTP_OK != $result_authenticate['status']) {
                return null;
            }
            return $result_authenticate['data'];
        } catch (\Exception $exc) {
            // Log
        }
    }

    /**
     * Get session token
     * @return string sessionToken
     */
    public function getSessionToken() {
        try {
            $cache_key = $this->_country_code . '_' . self::SESSION_TOKEN_CACHE;
            $sessionToken = $this->restaurantCache->fetch($cache_key);
            if (empty($sessionToken)) {
                $result = $this->authenticate();
                if (!empty($result) && count($result)) {
                    $sessionToken = $result['session_token'];
                    $expire_at = new \DateTime();
                    $expire_at->setTimestamp($result['expire_at']);
                    $current_date = new \DateTime();
                    $diff_date = $current_date->diff($expire_at);

                    $minutes = $diff_date->days * 24 * 60;
                    $minutes += $diff_date->h * 60;
                    $minutes += $diff_date->i;
                    $second = ($diff_date->s + ($minutes * 60));
                    // Time backup
                    if ($second > 600) {
                        $second = $second - 600;
                    }

                    $this->restaurantCache->save($cache_key, $sessionToken, $second);
                }
            }
            return $sessionToken;
        } catch (\Exception $exc) {
            // Log
        }

        return null;
    }

    private function getCallingString($url, $secret, $fields, $is_return_array = true)
    {
        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;

        $fields['sig'] = $this->genSignature($fields, $pathinfo);

        if(!$is_return_array)
        {
            // form up variables in the correct format for HTTP POST
            $fields_string = '';
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . urlencode($value) . '&';
            }
            $fields_string = rtrim($fields_string,'&');

            return $fields_string;
        }
        return $fields;
    }

    private function genSignature($params, $pathinfo)
    {
        $result = $pathinfo;

        ksort($params);
        foreach ($params as $key => $value)
        {
            $result .=  urlencode($key . $value);
        }
        return md5($result);
    }

    protected function doGetWrapper($url, $secret, $fields) {

        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_GET);
        $curl->setChannel('monolog.logger.bizapi');

        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);

        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $this->restaurantCache->delete($this->_country_code . '_' . self::SESSION_TOKEN_CACHE);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if(is_array($res))
            {
                $this->_logger->error($url, $res);
            }
            else
            {
                $this->_logger->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    protected function doGetJsonWrapper($url, $secret, $fields) {
        try {
            $curl = new Curl($this->_container);
            $curl->setMethod(Curl::HTTP_GET);
            $curl->setChannel('monolog.logger.bizapi');
            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
            if (!$res) {
                return array();
            }
            return $res;
        } catch (\Exception $exc) {
            $this->_logger->error($url);
            $this->_logger->error($exc->getMessage());
        }
        return array();
    }

    protected function doPostWrapper($url, $secret, $fields) {

        $curl = new Curl($this->_container);
        $curl->setMethod(Curl::HTTP_POST);
        $fields = $this->getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);
        $this->_logger->info($url, $fields);

        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED) {
            $this->restaurantCache->delete($this->_country_code . '_' . self::SESSION_TOKEN_CACHE);
            $sessionToken = $this->getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = $this->getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if(is_array($res))
            {
                $this->_logger->error($url, $res);
            }
            else
            {
                $this->_logger->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    public function doCurlGet($url) {
        try {
            // initialize cURL
            $ch = curl_init();

            // set options for cURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);

            // execute HTTP POST request
            $response = curl_exec($ch);
            // close connection

            curl_close($ch);
            $ret = json_decode($response, true);
            if (isset($ret) && $ret['response']['status'] == RestfulAPIHelper::TBL_STATUS_OK) {
                return $ret['response'];
            }
            return array();
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

    /**
     * Round time up with given interval
     * ex: interval 30 then 11:13 -> 11:30
     *
     * @param string $time
     * @param int $interval
     * @param int $extraTime
     * @return string
     */
    protected function roundTimeSlotUpWithExtraTime($time, $interval = 30, $extraTime = 30) {
        $timeArr = explode(':', substr_replace($time, ':', 2, 0));
        $totalTime = $timeArr[0] * 60 + $timeArr[1] + $extraTime;
        $timeRoundUpTotal = floor(($totalTime - 1) / $interval) * $interval + $interval;

        return sprintf("%02s", floor($timeRoundUpTotal / 60)) . sprintf("%02s", $timeRoundUpTotal % 60);
    }

    /**
     * Return time slot info for showing in view
     *
     * @param array $timeSlotInfo
     * @return array
     */
    public function setTimeSlotInfo($timeSlotInfo) {
        $shift_capacity = isset($timeSlotInfo['remaining_shift_capacity']) && !empty($timeSlotInfo['remaining_shift_capacity']) ? $timeSlotInfo['remaining_shift_capacity'] : 0;
        $time_slot_capacity = isset($timeSlotInfo['remaining_time_slot_capacity']) && !empty($timeSlotInfo['remaining_time_slot_capacity']) ? $timeSlotInfo['remaining_time_slot_capacity'] : 0;
        $max_covers = isset($timeSlotInfo['max_covers']) && !empty($timeSlotInfo['max_covers']) ? $timeSlotInfo['max_covers'] : 0;
        $min_value_check = min(array($shift_capacity, $time_slot_capacity, $max_covers));
        $timeSlot = isset($timeSlotInfo['time_slot']) ? $timeSlotInfo['time_slot'] : '';

        $currTimeArr = explode(':', date('H:i'));
        $timeSlotArr = explode(':', substr_replace($timeSlot, ':', 2, 0));
        $currDate = date('Y-m-d');
        $result = array(
            'time_value' => $timeSlot,
            'time_label' => substr_replace($timeSlot, ':', 2, 0),
            'date' => $timeSlotInfo['date']
        );
        if ( strtotime($timeSlotInfo['date']) > strtotime($currDate) ||
            (strtotime($timeSlotInfo['date']) == strtotime($currDate) && $timeSlotArr[0] * 60 + $timeSlotArr[1] > $currTimeArr[0] * 60 + $currTimeArr[1])) {
            $result['shift_id'] = $shift_capacity;
            $result['notfully'] = $shift_capacity > $time_slot_capacity ? $time_slot_capacity : $shift_capacity;
            $result['min_covers'] = isset($timeSlotInfo['min_covers']) && !empty($timeSlotInfo['min_covers']) ? $timeSlotInfo['min_covers'] : 0;
            $result['max_check_available'] = $min_value_check;
        } else {
            $result['shift_id'] = '';
            $result['notfully'] = 0;
            $result['min_covers'] = 0;
            $result['max_check_available'] = 0;
        }

        return $result;
    }

    /**
     * 13 PROMO SEARCH API
     * Production - https://api.search.insing.com/5. 0/{country}/business/promo/search
     * Test – https://api.search.test.insing.com/5.0/{country}/business/promo/search
     *
     * @param param [filter_name, filter_cuisine, filter_neighbourhood ]
     */
    public function searchPromotion(array $params = NULL) {
        $results = array();

        $session_token = $this->getSessionToken();
        $fields = array(
            'session_token' => $session_token,
            'filter_category' => 'Food & Drink'
        );
        $fields = array_merge($fields, $params);

        $url = $this->_business_seach_info['promotion_search_url'];
        $results = $this->doGetWrapper($url, $this->_business_seach_info['consumer_secret'], $fields);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $results['status']) {
            return array();
        }

        return $results;
    }

    /**
    * get available timeslot by bizId
    */
    public function searchDatesAvailableTimeSlots($bizId, array $params = array(), $country_code = '') {
        $results = array();

        if($country_code != '')
        {
            $country_biz_listing = $this->_container->getParameter('business_search_api');
            $this->_business_seach_info = $country_biz_listing[$country_code];
        }

        $url = $this->_business_seach_info['search_availability_url'];
        $url = str_replace('bizId', $bizId, $url);

        $session_token = $this->getSessionToken();
        $fields = array(
            'session_token' => $session_token,
            'include_facets' => 'date',
            'per_page' => 0
        );

        $fields = array_merge($fields, $params);
        
        $results = $this->doGetWrapper($url, $this->_business_seach_info['consumer_secret'], $fields);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $results['status']) {
            return array();
        }

        return $results;
    }

}

?>
