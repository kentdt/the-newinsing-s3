<?php
namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Parse data of business
 * @author LapTo
 */
class BusinessParser
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var loger
     */
    protected $log;

    /**
     * @var cache
     */
    protected $cache;

    /**
     * Define key cache
     */
    const DISH_LIST = 'DISH_LIST_'; //_businiessId

    /**
     * Constructor of BusinessParser
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->log = $container->get('monolog.logger.bizapi');
        $this->cache = $container->get('hgw.cache');
    }

    /**
     * Get restaurant image
     *
     * @author LapTo
     * @param array $restaurant
     * @param string $size size resize
     * @param string $sizeCrop
     * @return array
     *
     * Priority rule: Restaurant image >> Dish photo >> Latest reivew
     */
    public function getRestaurantImage($restaurant, $size = '620x468', $sizeCrop = '') {
        try {
            $image = '';
            if ($restaurant['no_image'] == false) {
                $image = $this->container->get('common.utils')
                            ->generatePhotoFromPath($restaurant['default_image_media_path'], $size, $sizeCrop);
                //$image = $restaurant['$sizeCrop'];
            } else {
                /**
                 * Nguyen confirmed: A release not show dish photo
                 */
                /*$keyCacheDish = self::DISH_LIST . $restaurant['id'];
                if (! $dishList = $this->cache->fetch($keyCacheDish)) {
                    $dishList = $this->container->get('bl.api')->getDishList($restaurant['id']);
                    $this->cache->save($keyCacheDish, $dishList, Constant::CACHE_LIFE_TIME_IN_TEN_MINUTE);
                }
                if ($dishList && is_array($dishList)) {
                    foreach ($dishList as $media) {
                        if (!empty($media['media_path'])) {
                            $image = $this->container->get('common.utils')
                                    ->generatePhotoFromPath($media['media_path'], $size, $sizeCrop);
                            break;
                        }
                    }
                }*/
                //Get latest review image
                if(empty($image)){
                    // get first image review of restaurant
                    $arrUserPhotos = $this->container->get('rnr.api')->getUserPhotosByRestaurantId($restaurant['id'], 7);

                    if (!empty($arrUserPhotos) && isset($arrUserPhotos['total']) && $arrUserPhotos['total'] > 0) {
                        $image = $this->container->get('common.utils')
                            ->generatePhotoFromUrl($arrUserPhotos['photos'][0]['url'], $size, $sizeCrop);
                    }
                }
            }
            return $image;
        } catch (Exception $ex) {
            $this->log->error($ex->getMessage());
            return '';
        }
    }
}


