<?php

/*
 * This file is part of the prestaSitemapPlugin package.
 * (c) David Epely <depely@prestaconcept.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace inSing\DataSourceBundle\Utilities;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service for dumping sitemaps into static files
 *
 * @author Konstantin Tjuterev <kostik.lv@gmail.com>
 * @author Konstantin Myakshin <koc-dp@yandex.ru>
 */

class HGWRMemCached
{
    /**
     *
     * @var CacheProvider
     */
    protected $cache = null;

    public function __construct(ContainerInterface $container){
        $servers = array();

        $servers_config = $container->getParameter('s3_cache_memcached_servers');
        foreach ($servers_config as $server)
        {
            list($host, $port) = explode(':', $server[0]);
            $servers[] = array($host, $port);
        }
        $this->cacheObj($servers);
    }

    protected function cacheObj($server = array())
    {
        $cache = new \Memcached();

        $cache->setOption(constant('Memcached::OPT_SERVER_FAILURE_LIMIT'), 10);
        $cache->setOption(constant('Memcached::OPT_CONNECT_TIMEOUT'), 1000);
        $cache->setOption(constant('Memcached::OPT_RETRY_TIMEOUT'), 500);
        $cache->setOption(constant('Memcached::OPT_RECV_TIMEOUT'), 500);
        $cache->setOption(constant('Memcached::OPT_SEND_TIMEOUT'), 500);
        $cache->setOption(constant('Memcached::OPT_TCP_NODELAY'), true);

        $cache->addServers($server);

        $this->cache = $cache;
    }

    public function contains($id)
    {
        return $this->doContains($id);
    }

    public function delete($id)
    {
        return $this->doDelete($id);
    }

    public function fetch($id)
    {
        return $this->doFetch($id);
    }

    public function save($id, $data, $lifeTime = 600)
    {
        return $this->doSave($id, $data, $lifeTime);
    }

    /**
     * {@inheritdoc}
     */
    protected function doFetch($id)
    {
        return $this->cache->get($id);
    }

    /**
     * {@inheritdoc}
     */
    protected function doContains($id)
    {
        return (false !== $this->cache->get($id));
    }

    /**
     * {@inheritdoc}
     */
    protected function doSave($id, $data, $lifeTime = 0)
    {
        if ($lifeTime > 30 * 24 * 3600) {
            $lifeTime = time() + $lifeTime;
        }
        return $this->cache->set($id, $data, (int) $lifeTime);
    }

    /**
     * {@inheritdoc}
     */
    protected function doDelete($id)
    {
        return $this->cache->delete($id);
    }

    /**
     * {@inheritdoc}
     */
    protected function doFlush()
    {
        return $this->cache->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function doGetStats()
    {
        $stats   = $this->memcached->getStats();
        $servers = $this->memcached->getServerList();
        $key     = $servers[0]['host'] . ':' . $servers[0]['port'];
        $stats   = $stats[$key];
        return array(
            Cache::STATS_HITS   => $stats['get_hits'],
            Cache::STATS_MISSES => $stats['get_misses'],
            Cache::STATS_UPTIME => $stats['uptime'],
            Cache::STATS_MEMORY_USAGE     => $stats['bytes'],
            Cache::STATS_MEMORY_AVAILABLE => $stats['limit_maxbytes'],
        );
    }
}
