<?php

/**
 * Description of BusinessApiUtils
 *
 * @author bien.tran
 */

namespace inSing\DataSourceBundle\Utilities;

class BusinessListingApi {
    const SESSION_TOKEN_CACHE = 'BUSINESS_LISTING_API_SESSION_TOKEN_CACHE';

    const MEDIA_TYPE_GALLERY = 6;
    const MEDIA_TYPE_DISH = 15;

    protected static $_business_listing_info = null;
    protected static $_container = null;
    protected static $_logger = null;
    protected static $_country_code = null;

    public function __construct($container) {
        self::$_container = $container;
        self::$_country_code = $container->getParameter('country_code');
        self::$_logger = new HgwLogger(self::$_container, 'business_listing_api');
        $country_biz_listing = $container->getParameter('business_listing_api');
        self::$_business_listing_info = $country_biz_listing[$container->getParameter('country_code')];
    }
    
    public static function switchCountry($country_code)
    {
        if(!empty($country_code))
        {
            self::$_country_code = $country_code;
            $country_biz_listing = self::$_container->getParameter('business_listing_api');
            self::$_business_listing_info = $country_biz_listing[self::$_country_code];
        }
    }

    public static function authenticate() {
        try {
            $result_authenticate = self::doPostWrapper(self::$_business_listing_info['login_url'], self::$_business_listing_info['consumer_secret'], array(
                        'consumer_key' => self::$_business_listing_info['consumer_key'],
                        'consumer_secret' => self::$_business_listing_info['consumer_secret'],
                        'access_token' => self::$_business_listing_info['access_token'],
                        'access_token_secret' => self::$_business_listing_info['access_token_secret'],
                        'user_login' => self::$_business_listing_info['user_login'],
                        'password' => self::$_business_listing_info['password_login']
                            ));
            if (RestfulAPIHelper::HTTP_OK != $result_authenticate['status']) {
                return null;
            }
            return $result_authenticate['data'];
        } catch (\Exception $exc) {
        	self::$_logger->error($exc->getMessage());
        }
    }

    /**
     * Get session token
     * @return string sessionToken
     */
    public static function getSessionToken() {
        try {
            $cache = self::$_container->get('hgw.cache');
            $cache_key = self::$_country_code . self::SESSION_TOKEN_CACHE;
            $sessionToken = $cache->fetch($cache_key);
            if (empty($sessionToken)) {
                $result = self::authenticate();
                if (!empty($result) && count($result)) {
                    $sessionToken = $result['session_token'];
                    $expire_at = new \DateTime();
                    $expire_at->setTimestamp($result['expire_at']);
                    $current_date = new \DateTime();
                    $diff_date = $current_date->diff($expire_at);

                    $minutes = $diff_date->days * 24 * 60;
                    $minutes += $diff_date->h * 60;
                    $minutes += $diff_date->i;
                    $second = ($diff_date->s + ($minutes * 60));
                    // Time backup
                    if ($second > 300) {
                        $second = $second - 300;
                    }

                    $cache->save($cache_key, $sessionToken, $second);
                }
            }
            return $sessionToken;
        } catch (\Exception $exc) {
        }

        return null;
    }

    private static function getCallingString($url, $secret, $fields, $is_return_array = true)
    {
    	//Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;

        $fields['sig'] = self::genSignature($fields, $pathinfo);

        if(!$is_return_array)
        {
            // form up variables in the correct format for HTTP POST
            $fields_string = '';
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . urlencode($value) . '&';
            }
            $fields_string = rtrim($fields_string,'&');

            return $fields_string;
        }
        return $fields;
    }
    
    private static function filterParams($array) {
    	return array_filter($array, function($item) {
    		if (is_array($item)) {
    			return count($item) > 0;
    		} elseif (is_null($item)) {
    			return false;
    		} elseif (is_bool($item)) {
    			return true;
    		} else {
    			return strlen($item) > 0;
    		}
    	});
    }

    private static function genSignature($params, $pathinfo)
    {
    	$result = $pathinfo;

    	$params = self::filterParams($params);
        ksort($params);
        
    	foreach ($params as $key => $value)
    	{
            $result .=  urlencode($key . $value);
        }
        return md5($result);
    }

    protected static function doGetWrapper($url, $secret, $fields) {

        $curl = new Curl(self::$_container);
        $curl->setMethod(Curl::HTTP_GET);
        $curl->setChannel('monolog.logger.bizapi');

        $fields = self::getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);

        if ($res['status'] == Curl::SESSION_TOKEN_TIMEOUT || $res['status'] == Curl::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = self::$_container->get('hgw.cache');
            $sessionTokenCache->delete(self::SESSION_TOKEN_CACHE);
            $sessionToken = self::getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = self::getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if(is_array($res))
            {
                self::$_logger->error($url, $res);
            }
            else
            {
                self::$_logger->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    protected static function doGetJsonWrapper($url, $secret, $fields) {
        try {
            $curl = new Curl(self::$_container);
            $curl->setMethod(Curl::HTTP_GET);
            $curl->setChannel('monolog.logger.bizapi');
            $fields = self::getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
            return $res;
        } catch (\Exception $exc) {
            self::$_logger->error($url);
            self::$_logger->error($exc->getMessage());
        }
        return array();
    }

    protected static function doPostWrapper($url, $secret, $fields, $postJson = false) {

        $curl = new Curl(self::$_container);
        $curl->setMethod(Curl::HTTP_POST);
        $curl->setPostJson($postJson);
        $fields = self::getCallingString($url, $secret, $fields);
        $res = $curl->call($url, $fields);
        self::$_logger->info($url, $fields);

        if ($res['status'] == RestfulAPIHelper::SESSION_TOKEN_TIMEOUT || $res['status'] == RestfulAPIHelper::HTTP_UNAUTHORIZED) {
            $sessionTokenCache = self::$_container->get('hgw.cache');
            $sessionTokenCache->delete(self::SESSION_TOKEN_CACHE);
            $sessionToken = self::getSessionToken();
            $fields['session_token'] = $sessionToken;

            $fields = self::getCallingString($url, $secret, $fields);
            $res = $curl->call($url, $fields);
        } else if ($res['status'] != RestfulAPIHelper::HTTP_OK) {
            if(is_array($res))
            {
                self::$_logger->error($url, $res);
            }
            else
            {
                self::$_logger->error('Result NULL: ' . $url, $fields);
            }
        }
        return $res;
    }

    public static function doCurlGet($url) {
        try {
            // initialize cURL
            $ch = curl_init();

            // set options for cURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);

            // execute HTTP POST request
            $response = curl_exec($ch);
            // close connection

            curl_close($ch);
            $ret = json_decode($response, true);
            if (isset($ret) && $ret['response']['status'] == RestfulAPIHelper::TBL_STATUS_OK) {
                return $ret['response'];
            }
            return array();
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

    public static function getMediaDetails($media_id, $media_type_id) {
    	$params = array();
    	$params['media_type_id'] = $media_type_id;
    	$session_token = self::getSessionToken();
    	$params['session_token'] = $session_token;
    	$params['use_cache'] = 'N';
    	$results = self::doGetWrapper(self::$_business_listing_info['media_get_url'] . $media_id, self::$_business_listing_info['consumer_secret'], $params);

    	//Return if not SUCCESS
    	if (RestfulAPIHelper::HTTP_OK == $results['status'])
    	{
    		return $results['data'];
    	}

    	return array();
    }

    public static function getMediaList($business_id, $media_id_type = 0, $more_params = array()) {
    	$params = array();
        $params['filter_business_id'] = $business_id;
        if($media_id_type)
        {
            $params['filter_media_type_id'] = $media_id_type;
        }
        $session_token = self::getSessionToken();
        $params['session_token'] = $session_token;
        $params['use_cache'] = 'N';
        $params['sort_fields'] = 'created_date';
        $params['sort_reverse'] = 'Y';
        if (is_array($more_params) && !empty($more_params)) {
            $params = array_merge($params, $more_params);
        }
        $results = self::doGetWrapper(self::$_business_listing_info['media_list_url'], self::$_business_listing_info['consumer_secret'], $params);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK == $results['status'])
        {
            if(isset($results['data']) && !empty($results['data']))
            {
                foreach ($results['data'] as $key => $value) {
                    $results['data'][$value['media_id']] = $value;
                    unset($results['data'][$key]);
                }
                return $results['data'];
            }
        }

        return array();
    }

    public static function getDishList($business_id, $more_params = array()) {
        return self::getMediaList($business_id, 15, $more_params);
    }

    public static function getDealMediaList($business_id) {
        return self::getMediaList($business_id, 14);
    }

    public static function getBankLogoList($business_id) {
        return self::getMediaList($business_id, 11);
    }

    public static function getBankCardList($business_id) {
        return self::getMediaList($business_id, 12);
    }

    public static function getBankLogo($media_id) {
    	return self::getMediaDetails($media_id, 11);
    }

    public static function getBankCard($media_id) {
    	return self::getMediaDetails($media_id, 12);
    }

    public static function getCatalogList($business_id) {
        return self::getMediaList($business_id, 3);
    }

    public static function getDealList($business_id) {
        $params['filter_business_id'] = $business_id;
        $session_token = self::getSessionToken();
        $params['session_token'] = $session_token;
        $params['use_cache'] = 'N';
        $params['sort_fields'] = 'start_date';
        $params['sort_reverse'] = 'Y';
        $results = self::doGetWrapper(self::$_business_listing_info['deal_list_url'], self::$_business_listing_info['consumer_secret'], $params);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK != $results['status'])
        {
            return array();
        }

        return $results;
    }

    /**
     * Get List Restaurant Awards https://jira.insing.com/jira/browse/HGW-65
     * @param type $business_id
     * @param type $more_params
     * @return array
     */
    public static function getAwardsListByBusinessId($business_id, $more_params = array())
    {
        $params = array();
        if ($business_id) {
            if (is_array($business_id) && !empty($business_id)) {
                $business_id = implode('|', $business_id);
            }

            $params['filter_business_ids'] = $business_id;
            $session_token = self::getSessionToken();
            $params['session_token'] = $session_token;
            $params['use_cache'] = 'N';
            $params['sort_fields'] = 'award_date';
            $params['sort_reverse'] = 'Y';

            if (is_array($more_params) && !empty($more_params)) {
                $params = array_merge($params, $more_params);
            }

            $results = self::doGetWrapper(self::$_business_listing_info['awards_list_url'], self::$_business_listing_info['consumer_secret'], $params);

            if (RestfulAPIHelper::HTTP_OK == $results['status'])
            {
                if(isset($results['data']) && !empty($results['data']))
                {
                    return $results['data'];
                }
            }

            return array();
        }
    }

    public static function getBusinessListingDetail($bizId) {
        $params = array(
            'session_token' => self::getSessionToken(),
            'use_cache' => 'N'
        );
        $businessDetailUrl = self::$_business_listing_info['business_detail_api'] . $bizId;
        $results = self::doGetWrapper($businessDetailUrl, self::$_business_listing_info['consumer_secret'], $params);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK == $results['status']) {
            if(isset($results['data']) && !empty($results['data'])) {
                return $results['data'];
            }
        }

        return array();
    }

    public static function getListBusinesses($params, $country_code = '', $full_response = false) {

        self::switchCountry($country_code);

        $params['session_token'] = self::getSessionToken();
        $params['use_cache'] = 'N';
        $params['filter_primary_status'] = 'active';
        $params['filter_secondary_status'] = 'open|new opening|opening soon|under renovation';

        $getListBusinesses = self::$_business_listing_info['business_list_api'];
        $results = self::doGetWrapper($getListBusinesses, self::$_business_listing_info['consumer_secret'], $params);
        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK == $results['status']) {
            if(isset($results['data']) && !empty($results['data'])) {
                return $full_response ? $results : $results['data'];
            }
        }

        return array();
    }

    public static function getListBusinessNames($params, $country_code = '', $full_response = false) {

        self::switchCountry($country_code);

        $params['session_token'] = self::getSessionToken();
        $params['use_cache'] = 'N';
        $params['filter_primary_status'] = 'active';

        $getListBusinesses = self::$_business_listing_info['business_name_list_api'];
        $results = self::doGetWrapper($getListBusinesses, self::$_business_listing_info['consumer_secret'], $params);
        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK == $results['status']) {
            if(isset($results['data']) && !empty($results['data'])) {
                return $full_response ? $results : $results['data'];
            }
        }

        return array();
    }
    
    public static function getListBusinessCategories($params, $full_response = false) {
    	
    	$params['session_token'] = self::getSessionToken();
    	$params['use_cache'] = 'N';
    
    	$getListBusinesses = self::$_business_listing_info['category_search_api'];
    	$results = self::doGetWrapper($getListBusinesses, self::$_business_listing_info['consumer_secret'], $params);
    	//Return if not SUCCESS
    	if (RestfulAPIHelper::HTTP_OK == $results['status']) {
    		if(isset($results['data']) && !empty($results['data'])) {
    			return $full_response ? $results : $results['data'];
    		}
    	}
    
    	return array();
    }

    public static function searchBusinessCategory($params, $country_code = '', $full_response = false) {
        self::switchCountry($country_code);

        $params['session_token'] = self::getSessionToken();
        $params['use_cache'] = 'N';
        $params['filter_depth'] = '1';

        $searchBusinessCategory = self::$_business_listing_info['category_search_api'];
        $results = self::doGetWrapper($searchBusinessCategory, self::$_business_listing_info['consumer_secret'], $params);

        //Return if not SUCCESS
        if (RestfulAPIHelper::HTTP_OK == $results['status']) {
            if(isset($results['data']) && !empty($results['data'])) {
                return $full_response ? $results : $results['data'];
            }
        }

        return array();
    }
    
    /**
     * 93.1 Create report business error API.
     *
     * @param array $params
     *
     * @return boolean
     */
    public function createReportBusinessError($params) {
    	$session_token = self::getSessionToken();
    	$params['session_token'] = $session_token;
    	try {
    		$result = self::doPostWrapper(self::$_business_listing_info['report_error_api'], self::$_business_listing_info['consumer_secret'], $params, true);
    		if (RestfulAPIHelper::HTTP_OK != $result['status']) {
    			return false;
    		}
    		return true;
    	} catch (\Exception $exc) {
    		self::$_logger->error($exc->getMessage());
    	}
    }
    
    /**
     * 88.1 Create Submit_Business Request API.
     *
     * @param array $params
     *
     * @return boolean
     */
    public function createSubmitBusiness($params) {
    	$session_token = self::getSessionToken();
    	$params['session_token'] = $session_token;
    	try {
    		$result = self::doPostWrapper(self::$_business_listing_info['submit_business_api'], self::$_business_listing_info['consumer_secret'], $params, true);
    		if (RestfulAPIHelper::HTTP_OK != $result['status']) {
    			return false;
    		}
    		return true;
    	} catch (\Exception $exc) {
    		self::$_logger->error($exc->getMessage());
    	}
    }
    
    /**
     * 142.1 Create Submit_Business Request API.
     *
     * @param array $params
     *
     * @return boolean
     */
    public function createClaimBusiness($params) {
    	$session_token = self::getSessionToken();
    	$params['session_token'] = $session_token;
    	try {
    		$result = self::doPostWrapper(self::$_business_listing_info['claim_business_api'], self::$_business_listing_info['consumer_secret'], $params, true);
    		if (RestfulAPIHelper::HTTP_OK != $result['status']) {
    			return false;
    		}
    		return true;
    	} catch (\Exception $exc) {
    		self::$_logger->error($exc->getMessage());
    	}
    }
}

?>
