<?php

namespace inSing\DataSourceBundle\Services;

/**
 * BaseService
 *
 */

class BaseService{

    protected $_doctrine;

    protected $_entity_manager;

    protected $_repository;

    protected $_router;

    protected $_config;

    protected $_data_source;

    protected $_repository_name = '';

    public function __construct($doctrine,$data_source , $router, $config, $knp_paginator)
    {
        $this->_doctrine         = $doctrine;
        $this->_entity_manager   = $doctrine->getManager();
        if($this->_repository_name){
            $this->_repository       = $doctrine->getManager()->getRepository($this->_repository_name);
        }
        $this->_router           = $router;
        $this->_config           = $config;
        $this->_data_source      = $data_source;
        $this->_knp_paginator    = $knp_paginator;        
    }    
}