<?php

namespace inSing\DataSourceBundle\Lib;

use inSing\DataSourceBundle\Lib\DataWrapper;

/**
 * A class is responsible for keeping the IDs list has been read
 *  
 * @author Trung Nguyen
 */
class DataTracker implements \SplSubject
{
    /**
     * @var array
     */
    protected $data;
    
    /**
     * @var array
     */
    private $observers = array();
    
    /**
     * @var arrays
     */
    private $cmd = array(
        'One'	=> 1,        
        'Two'	=> 2,        
        'Three'	=> 3,        
        'Four'	=> 4,        
        'Five'	=> 5,        
        'Six'	=> 6,        
        'Seven'	=> 7,        
        'Eight'	=> 8,        
        'Nine'	=> 9,        
        'Ten'	=> 10        
    );
    
    /**
     * @var int
     */
    private $currentBlock = 1;
    
    /**
     * @var int
     */
    private $currentSidebarBlock = 1;
    
    /**
     * Constructor
     * 
     * @param array $data
     */
    public function __construct($data = array())
    {
        $this->data = $data;
    }
    
    /**
     * Update $data variable
     *
     * @param  mixed $data
     * @return void
     */
    public function add($data)
    {
        $xp = array_merge($this->data, ( is_array($data) ? $data : explode(',', $data) ) );
        
        $this->data = array_unique($xp);
    }
    
    /**
     * Setter of [currentBlock] property
     *
     * @param  int $block_order
     * @return void
     */
    public function setCurrentMainBlock($block_order)
    {
        $this->currentBlock = $block_order;
    }
    
    /**
     * Getter of [currentBlock] property
     *
     * @return int
     */
    public function getCurrentMainBlock()
    {
        return $this->currentBlock;
    }
    
    /**
     * Setter of [currentBlock] property
     *
     * @param  int $block_order
     * @return void
     */
    public function setCurrentSidebarBlock($block_sidebar_order)
    {
    	$this->currentSidebarBlock = $block_sidebar_order;
    }
    
    /**
     * Getter of [currentSidebarBlock] property
     *
     * @return int
     */
    public function getCurrentSidebarBlock()
    {
    	return $this->currentSidebarBlock;
    }
    
    /**
     * Get KEYs list 
     *
     * @return void
     */
    public function getTrackData()
    {
        return $this->data;
    }
    
    /**
     * Magic call
     *
     * @param string $method
     * @param array $args
     * @throws \Exception
     * @return Ambigous <boolean, mixed>
     * @author Trung Nguyen
     */
    public function __call($method, $args)
    {
        $prefix = substr($method, 0, 7);
        $subfix = substr($method, 7);
        
        if ($prefix != 'fetchBy' || !array_key_exists($subfix, $this->cmd)) {
            throw new \Exception('Call to undefined method');
        } elseif (count($args) <= 0 || !$args[0] instanceof DataWrapper) {
            throw new \InvalidArgumentException();
        }
        
        return $this->fetch($args[0], $this->cmd[$subfix], ( isset($args[1]) ? (bool)$args[1] : false ));
    }
    
    /**
     * Fetch n element at the beginning of array
     * 
     * @param DataWrapper $obj
     * @param int         $row        - the number of row returned
     * @param bool        $full_data  - true: return total row equa to $row. Otherwise false (default).
     * @return mixed - array if data is ok. Otherwise false.
     */
    protected function fetch(DataWrapper $obj, $row, $full_data = false)
    {
        $ret = array();
        
        // we check data to make sure we have enough items as developer's need
        if (!$full_data || $this->tryToFetch($obj->getArrayCopy(), $row)) {
            while (true) {
                // in case of reaching end of array
                if (is_null($key = key($obj))) {
                    break;
                }
                
                // get real key
                $realKey = $this->getRealKey($key);
                
                // just remove this item if it's taken before
                if (in_array($realKey, $this->data)) {
                    $this->notify($key);
                } else {
                    // save item's key to storage for monitoring
                    $this->add($realKey);
                    
                    $ret[] = current($obj);
                    
                    // traverse observers and remove items'key match $key
                    $this->notify($key);
                    
                    // force to exit loop if we have enough items
                    if (count($ret) >= $row) {
                        break;
                    }
                }
            }
        }
        
        return count($ret) >= 1 ? $ret : false;
    }
    
    /**
     * Used to check if data source has enough items as developer's need
     * 
     * @param array $arr
     * @param int $row
     * @return bool
     */
    protected function tryToFetch(array $arr, $row)
    {
        $ret = false;
        
        // get a copy of track data
        $trackData = $this->data;
        
        while (true) {
            // in case of reaching end of array
            if (is_null($key = key($arr))) {
                break;
            }
            
            // get real key
            $realKey = $this->getRealKey($key);
            
            // in case this item has been taken before
            if (in_array($realKey, $trackData)) {
                unset($arr[$key]);
            } else {
                $trackData[] = $realKey;
                unset($arr[$key]);
                $row--;
            }
            
            if ($row <= 0) {
                $ret = true;
            }
        }
        
        return $ret;
    }
    
    /**
     * Get real key
     * 
     * @param string $key
     * @return string
     */
    protected function getRealKey($key) 
    {
        $pos = strrpos($key, '_');
        
        return substr($key, 0, $pos === false ? strlen($key) : $pos);
    }
    
    /**
     * (non-PHPdoc)
     * @see SplSubject::attach()
     */
    public function attach(\SplObserver $obs)
    {
        $this->observers[$obs->id] = $obs;
    }
    
    /**
     * (non-PHPdoc)
     * @see SplSubject::detach()
     */
    public function detach(\SplObserver $obs)
    {
        unset($this->observers[$obs->id]);
    }
    
    /**
     * (non-PHPdoc)
     * @see SplSubject::notify()
     */
    public function notify($key = '')
    {
        foreach($this->observers as $obs) {
            if (isset($obs[$key])) {
                unset($obs[$key]);
            }
        }
    }
}