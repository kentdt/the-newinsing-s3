<?php 

namespace inSing\DataSourceBundle\Lib\Monitor;

/**
 * An observer class for monitoring CURL
 * 
 * @author Trung Nguyen
 */
class CurlMonitor implements \SplObserver 
{
    const MAX_EXECUTE_TIME = 5;
    
    const CURL_LOG_FILE = 'curl.log';
    
    const SLOWORDIE_LOG_FILE = 'slow_or_die.log';
    
    /**
     * @var Logger
     */
    protected $logger;
    
    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * Calculate the duration of execution and save to log file
     *
     * @param  SplSubject $subject
     * @return void
     */
    public function update(\SplSubject $subject)
    {
        $lastError = $subject->getLastError();
        
        // in case of having no error
        if (is_null($lastError)) {
            
            $context = array(
                $subject->currentUrl,
                $subject->stopTime - $subject->startTime
            );
            
            // log to file
            $this->logger->addInfoToFile($subject->pid, $context, self::CURL_LOG_FILE);
            
            // log to slow_or_die.log in case the duration of execution reach to limit 
            if ($subject->stopTime - $subject->startTime >= self::MAX_EXECUTE_TIME) {
                $this->logger->addInfoToFile($subject->pid, $context, self::SLOWORDIE_LOG_FILE);
            }
            
        } else {
            
            // define error codes 
            $cUrlErrCodes = array(
                CURLE_COULDNT_RESOLVE_PROXY,
                CURLE_COULDNT_RESOLVE_HOST,
                CURLE_COULDNT_CONNECT,
                28,    // CURLE_OPERATION_TIMEDOUT
                CURLE_SSL_CONNECT_ERROR,
                CURLE_TOO_MANY_REDIRECTS,
            );
            
            $context = array(
                $subject->currentUrl,
                $lastError['error_code'],
                $lastError['error_str']
            );
            
            if (in_array($lastError['error_code'], $cUrlErrCodes)) {
                // log to file
                $this->logger->addErrorToFile($subject->pid, $context, self::SLOWORDIE_LOG_FILE);
            } else {
                $this->logger->addErrorToFile($subject->pid, $context, self::CURL_LOG_FILE);
            }
            
        }
    }
}