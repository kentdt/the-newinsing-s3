<?php
namespace inSing\DataSourceBundle\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use inSing\DataSourceBundle\Lib\AwsApi;
use inSing\DataSourceBundle\Lib\InSingAwsS3;

class UtilHelper
{
    const PUBLISH_STATUS = 1;
    const PAGESIZE_CHANNEL_FILTER = 20;
    const PAGESIZE_TAGS_FILTER = 20;
    const PAGESIZE_VIDEO_FILTER = 20;
    const PAGESIZE_EVERGREEN_FILTER = 20;
    const PAGESIZE_ALL_CHANNEL = 100;
    const HGW = 'hungrygowhere.com';
    const TRAVEL = 'travel';

    const MOVIE_TAGLINES                    = 'Movie | to watch | this week';
    const EVENT_TAGLINES                    = 'Things | to do | this week';
    const RESTAURANT_TAGLINES               = 'Restaurant | to dine in | this week';
    const RESTAURANT_DEAL_TAGLINES          = 'Great | dining deal | this week';
    const RESTAURANT_HUNGRYDEAL_TAGLINES    = 'Great | dining deal | this week';
    const ARTICLE_TAGLINES                  = 'Featured | Article | this week';
    const GALLERY_TAGLINES                  = 'Featured | Photos | this week';

    const CHANNEL_MAPPING_MOVIE                     = 'Movie';
    const CHANNEL_MAPPING_EVENT                     = 'Event';
    const CHANNEL_MAPPING_RESTAURANT                = 'HungryGoWhere';
    const CHANNEL_MAPPING_RESTAURANT_DEAL           = 'HungryGoWhere';
    const CHANNEL_MAPPING_RESTAURANT_HUNGRYDEAL     = 'HungryGoWhere';

    private static $_container = null;
    private static $_generator = null;

    public function __construct(ContainerInterface $container, UrlGeneratorInterface $generator)
    {

        self::$_container = $container;
        self::$_generator = $generator;
    }

    /**
     * @author Vu Luu
     * upload Photo to AWS server and get image link
     *
     */
    public static function uploadPhoto($file, $container)
    {
        try{

            if (is_null($file)) {
                //return json_encode(array('status' => 0 , 'msg' => "No photo is uploaded"));
                return false;
            }

            $fileType = $file->getMimeType();
            if($fileType != 'image/jpeg' && $fileType != 'image/gif' && $fileType != 'image/png') {
                //return json_encode(array('status' => 0 , 'msg' => "Uploaded file is not image"));;
                return false;
            }

            $photo_arr = AwsApi::createPhotoObject($container, $file);
            if (!$photo_arr) {
                //return json_encode(array('status' => 0 , 'msg' => "Upload is successfully"));;
                return false;
            } else {
                //Create Photo Image Url
                $photo_arr['photo_img'] = self::getPhotoUpload($photo_arr['path'], $container);

                //return json_encode(array('status' => 1 , "img_url" => $photo_arr['photo_img']));;
                return $photo_arr['photo_img'];

            }

            return false;
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @author Vu Luu
     * Get Photo Upload Action
     */
    public static function getPhotoUpload($path, $container)
    {
        $aws_id = $container->getParameter('aws_s3_id');
        $aws_secret = $container->getParameter('aws_s3_secret');
        $bucket = $container->getParameter('aws_s3_bucket_name');

        $client = new InSingAwsS3($aws_id, $aws_secret);

        $url_image = $client->getURL($bucket, $path, '10 minutes');
        if(!$url_image)
        {
            //throw $this->createNotFoundException('Page Not Found!');
            return false;
        }

        $image_url = '';

        if($url_image != null){
            $image_url = $container->getParameter('upload_photo_cdn').'/'.$path;
        } else {
            return false;
        }

        //return json_encode(array('status' => 1 , "img_url" => $image_url ));
        return $image_url;
    }

    /**
     * Generate API Signature ( Copy form Authenicate API )
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public static function genSignature($params, $pathinfo)
    {
        $result = $pathinfo;
        ksort($params);

        foreach ($params as $key => $value) {
            $result .= $key . urlencode($value);
        }

        return md5($result);
    }

    /*
     * Copy from HGW
     * @author Vu.Luu
     */
    public static function generatePhotoFromPath($container, $photo_path, $photo_width_height = '', $cropping_width_height = '') {
        $imageUrl = self::generateImageFromImagePath($container, $photo_path);

        $image_size = explode('x', $photo_width_height);
        if (count($image_size) == 2){
            if($cropping_width_height == '') {
                $cropping_width_height = $photo_width_height;
                $photo_width_height = ($image_size[0] + intval($image_size[0] / 4)) . 'x' . ($image_size[1] + intval($image_size[1] / 4));
            }
        }else{
            return $imageUrl;
        }
        return self::generatePhotoFromUrlOptions($container, $imageUrl, $cropping_width_height, $photo_width_height);
    }

    /*
     * Copy from HGW
     * @author Vu.Luu
     */
    public static function generateImageFromImagePath($container, $imagePath, $bucketName = 'business') {
        $imagePath = '/' . trim($imagePath, '/');
        $domains = $container->getParameter('cdn_links_accept');
        $index = array_rand($domains);
        $url = "http://{$domains[$index]}/{$bucketName}{$imagePath}";
        return $url;
    }

    /*
     * Copy from HGW
     * @author Vu.Luu
     */
    public static function generatePhotoFromUrlOptions($container, $photo_url, $cropping_width_height, $photo_width_height = '', $fillbg = true, $no_stretch = false) {
        $cdn_links = $container->getParameter('cdn_links_accept');
        $url_info  = parse_url($photo_url);

        if (empty($photo_url) || strpos($photo_url, 'staticc0') !== false || empty($url_info['host']) || !in_array($url_info['host'], $cdn_links)) {
            return $photo_url;
        }

        if(!empty($cropping_width_height))
        {
            $fillbg = false;
        }

        //Get Photos Size
        preg_match('/^(?P<width>[0-9]+)x(?P<height>[0-9]+)$/', $photo_width_height, $matches);

        $photo_width = ( isset($matches['width']) ? trim($matches['width']) : '' );
        $photo_height = ( isset($matches['height']) ? trim($matches['height']) : '' );

        //Get Cropping Photos Size
        preg_match('/^(?P<width>[0-9]+)x(?P<height>[0-9]+)$/', $cropping_width_height, $matches);

        $crop_width = ( isset($matches['width']) ? trim($matches['width']) : '' );
        $crop_height = ( isset($matches['height']) ? trim($matches['height']) : '' );

        //Get Photos Information from URL
        $url_info_path = isset($url_info['path']) ? $url_info['path'] : '' ;
        $photo_info = pathinfo( $url_info_path );

        $secret = self::$_container->getParameter('cdn_secret');

        //Create Photo Information
        $photo = array(
            'base_photo_url'    => $photo_url,
            'photo_url'         => '',
            'photo_name_full'   => '',
            'photo_name'        => (isset($photo_info) && isset($photo_info['filename'])) ? $photo_info['filename'] : '',
            'photo_width'       => $photo_width,
            'photo_height'      => $photo_height,
            'fillbg'            => ($fillbg == true)? 'fillbg' : '',
            'no_stretch'        => ($no_stretch == true)? 'no_stretch' : '',
            'crop_width'        => $crop_width,
            'crop_height'       => $crop_height,
            'photo_ext'         => (isset($photo_info) && isset($photo_info['extension'])) ? $photo_info['extension'] : '',
            'photo_secret'      => $secret
        );

        //Generate Photo Sig
        $photo['photo_sig'] = self::generatePhotoSig( $photo );

        //Create File Name
        $photo['photo_name_full'] = strtr('[name][widthxheight][fillbg][no_stretch][cwidthxcheight]_[sig].[ext]', array(
            '[name]' => $photo['photo_name'],
            '[widthxheight]' => $photo['photo_width'] ? '_' . $photo['photo_width'] . 'x' . $photo['photo_height'] : '',
            '[fillbg]' => $photo['fillbg'] ? '_fillbg' : '',
            '[no_stretch]' => $photo['no_stretch'] ? '_no_stretch' : '',
            '[cwidthxcheight]' => $photo['crop_width'] ? '_crop_' . $photo['crop_width'] . 'x' . $photo['crop_height'] : '',
            '[sig]' => $photo['photo_sig'],
            '[ext]' => $photo['photo_ext']
        ));

        //Generate Photo Url
        $photo['photo_url'] = strtr('[scheme]://[host][dirname]/[photo_name_full]', array(
            '[scheme]'          => isset($url_info['scheme']) ? $url_info['scheme'] : '',
            '[host]'            => isset($url_info['host']) ? $url_info['host'] : '',
            '[dirname]'         => (isset($photo_info) && isset($photo_info['dirname'])) ? $photo_info['dirname'] : '',
            '[photo_name_full]' => $photo['photo_name_full']
        ));

        return $photo['photo_url'];
    }


    /**
     * Ex :
     * @author  phuc.nguyen
     * @method generate sig when calling some api
     * @param
     * @see
     * @return 702e85a9f45d2ffe0703cd4fc7e7bd64
     * @since   Jun 20, 2013 4:58:23 PM 2013
     */
    public static function genSignatureWithSecret($url, $params, $secret)
    {
        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $secret . $pathinfo;

        $sig = self::genSignature($params, $pathinfo);

        return $sig;
    }

    /**
     *
     * Slug function
     * @author Vu Tran
     * @param unknown_type $stringToken
     * @param unknown_type $token_split
     */
    public static function slug($stringToken, $token_split = " ")
    {
        $token_split = trim($token_split);

        $stringToken = htmlspecialchars_decode($stringToken, ENT_QUOTES);
        $stringToken = trim($stringToken, ' ');
        $stringToken = preg_replace('/[^A-Za-z0-9\s]/', '-', $stringToken);
        $stringToken = preg_replace('/\s+/', '-', $stringToken);
        $stringToken = preg_replace('/-+/', '-', $stringToken);
        $stringToken = trim($stringToken, '-');

        return strtolower($stringToken);
    }

    /**
     * Convert an array of fields into string
     *
     * @param array $fields
     * @return string
     */
    public static function convertFieldsToString(array $fields)
    {
        $xp = array();

        foreach ($fields as $key => $value) {
            $xp[] = $key . '=' . urlencode($value);
        }

        return implode('&', $xp);
    }

    /**
     * convert stdClass Object to an Array
     *
     * @author Vu Tran
     * @param $d
     * @return array
     */
    public static function objectToArray($d)
    {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
             * Return array converted to object
             * Using __FUNCTION__ (Magic constant)
             * for recursive call
             */
            return array_map('inSing\DataSourceBundle\Lib\UtilHelper::objectToArray', $d);
        } else {
            // Return array
            return $d;
        }
    }

    /**
     * Get micro time in float
     *
     * @author Trung Nguyen
     * @return float
     */
    public static function getMicroTimeInFloat()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     * Ex :
     * @author  Vu.Luu
     * @method generate signature when calling some api
     * get path url after web/ or get basic path
     * convert ASCII to URL encoded
     */
    public static function genSignatureWithSecret2($url, $params, $secret)
    {
        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        if (strpos($pathinfo['path'], 'web/')) {
            $path_explode = explode('web/', $pathinfo['path']);
            $pathinfo = str_replace('/', '', $path_explode[1]);
        } else {
            $pathinfo = str_replace('/', '', $pathinfo['path']);
        }
        $pathinfo = $secret . $pathinfo;

        $sig = self::genComplexSignature($params, $pathinfo);

        return $sig;
    }

    /**
     * @author Cuong.Bui
     */
    public static function genNewSignatureWithSecret($url, $params, $secret)
    {
        $pathinfo = parse_url($url);

        if (strpos($pathinfo['path'], 'web/')) {
            $path_explode = explode('web/', $pathinfo['path']);
            $pathinfo = str_replace('/', '', $path_explode[1]);
        } else {
            $pathinfo = str_replace('/', '', $pathinfo['path']);
        }

        $pathinfo = $secret . $pathinfo;
        $sig = self::genNewComplexSignature($params, $pathinfo);

        return $sig;
    }

    /**
     * Generate API Signature ( Copy form Authenicate API )
     *
     * @author  Vu.Luu
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public static function genComplexSignature($params, $pathinfo)
    {
        $result = $pathinfo;
        ksort($params);

        foreach ($params as $key => $value) {
            $value = urlencode($value);

            $value = str_replace(urlencode('&'), '', $value);
            $value = str_replace(urlencode('='), '', $value);

            $result .= $key . $value;
        }

        return md5($result);
    }

    /**
     * @author Cuong.Bui
     */
    public static function genNewComplexSignature($params, $pathinfo)
    {
        $result = $pathinfo;
        ksort($params);

        foreach ($params as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subValue) {
                    $subValue = urlencode($subValue);
                    $result .= $key . rawurlencode('[]') . $subValue;
                }
            } else {
                $value = urlencode($value);
                $result .= $key . $value;
            }
        }

        return md5($result);
    }

    /**
     * Ex :
     * @author  Vu.Luu
     * Convert an array of fields into string
     * Used when fields have platform "channel[]=?&channel[]=?"
     */
    public static function convertComplexFieldsToString(array $fields)
    {
        $xp = array();

        foreach ($fields as $key => $value) {
            $value = urlencode($value);

            $value = str_replace(urlencode('&'), '&', $value);
            $value = str_replace(urlencode('='), '=', $value);

            $xp[] = $key . '=' . $value;
        }

        return implode('&', $xp);
    }

    /**
     * @author Cuong.Bui
     */
    public static function convertNewComplexFieldsToString(array $fields)
    {
        $xp = array();

        foreach ($fields as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subValue) {
                    $xp[] = $key . rawurlencode('[]') . '=' . urlencode($subValue);
                }
            } else {
                $xp[] = $key . '=' . urlencode($value);
            }
        }

        return implode('&', $xp);
    }

    /**
     * Convert Sessions from array(1,2) -> array('Morning','Evening')
     *
     * @author Vu Tran
     * @param Session string delimiter by ',' $source
     * @return string
     */
//    public static function mappingSessionToSessionStr($sessions)
//    {
//        $sessions_str = array();
//        foreach ($sessions as $session) {
//            if ($session != '') {
//                switch ($session) {
//                    case Constant::SESSION_WHOLEDAY:
//                        $sessions_str[] = Constant::SESSION_WHOLEDAY_TEXT;
//                        break;
//                    case Constant::SESSION_MORNING:
//                        $sessions_str[] = Constant::SESSION_MORNING_TEXT;
//                        break;
//                    case Constant::SESSION_AFTERNOON:
//                        $sessions_str[] = Constant::SESSION_AFTERNOON_TEXT;
//                        break;
//                    case Constant::SESSION_EVENING:
//                        $sessions_str[] = Constant::SESSION_EVENING_TEXT;
//                        break;
//                }
//            }
//        }
//
//        return $sessions_str;
//    }

    /**
     * Return article_id from url
     * @author Cuong.Bui
     */
//    public static function getContentParts($url)
//    {
//        $ret = '';
//        $url = trim($url);
//        $array = parse_url($url);
//
//        $ret['host'] = $array['host'];
//
//        if (isset($array['path']) && $array['path'] != '') {
//            $parts = explode('/', $array['path']);
//            $hex_id = '';
//
//            foreach ($parts as $item) {
//                if (preg_match('/id-[0-9a-z]+/', $item)) {
//                    $hex_id = $item;
//                }
//            }
//
//            if ($hex_id != '') {
//                $array = explode('-', $hex_id);
//                $hex = end($array);
//                if (preg_match('/[0-9a-z]+/', $hex)) {
//                    $ret['id'] = self::urlIdToId($hex);
//                }
//            }
//        }
//
//        return $ret;
//    }

    /**
     * Convert from int to hash
     *
     * @author Vu Tran
     * @param int $id
     * @return hash id
     */
    public static function idToUrlId($id)
    {
        $paddedId = sprintf("%08s", dechex($id));
        $strArr = str_split($paddedId, 2);
        return implode(array_reverse($strArr));
    }

    /**
     * Convert from hash to int
     *
     * @author Vu Tran
     * @param has $urlId
     * @return int
     */
    public static function urlIdToId($urlId)
    {
        $strArr = str_split($urlId, 2);
        $paddedId = implode(array_reverse($strArr));
        return hexdec($paddedId);
    }

    /**
     * Generate media URL from the resource ID
     *
     * @author  Thai Pham
     * @param   Int $resourceId, $transcodeName, $fileType
     * @return  String
     */
//    static public function generateMediaUrl($resourceId, $transcodeName = 'pc_143x107', $fileType = 'jpg')
//    {
//        $str = self::idToUrlId($resourceId);
//        $hex_array = str_split($str, 2);
//        $path = implode('/', $hex_array);
//
//        return 'http://staticc0' . rand(1, 5) . '.insing.com/images/' . $path . '/' . $transcodeName . '.' . $fileType;
//    }

    /**
     * Generate media URL from the resource URL ( with url format ex: bf/ea/10/00 )
     *
     * @author  Vu Tran
     * @param   Int $resourceUrl, $transcodeName, $fileType
     * @return  String
     */
//    static public function generateMediaUrlFromResourceUrl($resourceUrl, $transcodeName = 'pc_143x107', $fileType = 'jpg')
//    {
//      return 'http://staticc0' . rand(1, 5) . '.insing.com/images/' . $resourceUrl . '/' . $transcodeName . '.' . $fileType;
//    }

    /**
     * @author Cuong.Bui
     */
    public static function generateArticleUrl($id, $slug, $series, $series_slug, $channel, $channel_domains, $type = 0)
    {
        $old_gallery_id_start = 15000000;
        $old_gallery_id_end = 15999999;

        $id = (int) $id;
        $slug = trim($slug);
        $series = trim($series);
        $series_slug = trim($series_slug);
        $channel = strtolower($channel);
        $type = (int) $type; // 0: Article; 1: Gallery
        $url = '';

        if ($old_gallery_id_start <= $id && $id <= $old_gallery_id_end) {
            $id = $id - $old_gallery_id_start;
        }

        if ($series_slug == '') {
            $series_slug = 'n-a';
        }

        if (isset($channel_domains[$channel])) {
            $url = $channel_domains[$channel];

            $array = array_filter(explode('/', $slug));
            if (count($array) > 1) {
                if (isset($array[2])) {
                    $slug = $array[2];
                }
            }

            switch ($type) {
                case 0:
                    if ($channel != self::HGW) {
                    	if( $channel == self::TRAVEL ){
                    		$slug = 'travel-guide/'.$slug;
                    	}
                        $url .= '/feature/' . $slug . '/id-' . self::idToUrlId($id);
                    } else {
                        $url .= '/dining-guide/' . $series_slug . '/' . $slug . '-*aid-' . self::idToUrlId($id);
                    }
                    break;

                case 1:
                    if ($channel != self::HGW) {
                        $url .= '/gallery/' . $slug . '/id-' . self::idToUrlId($id);
                    } else {
                        $url .= '/gallery/' . $slug . '-*gid-' . self::idToUrlId($id);
                    }
                    break;
            }
        }

        return $url;
    }

    /*
     * @author: Vu Luu
     */
    public static function generateFeatureUrl($container, $slug, $id, $type = 0)
    {
        $featureGalleryUrl = $container->getParameter('insing_feature_gallery_url');
        $featureArticleUrl = $container->getParameter('insing_feature_article_url');

        //check slug. Ex: "/tabloid/weight-loss-surgery-prevent-diabetes/id-c05c3f00".
        //Get "tabloid/weight-loss-surgery-prevent-diabetes"

        $slug = ltrim($slug, '/');
        $slug_check = explode("/", $slug);

        if(strpos($slug_check[count($slug_check) - 1], 'id-') !== false){
            $slug = rtrim(str_replace($slug_check[count($slug_check) - 1], '', $slug), '/');
        }


        $hexId = self::idToUrlId($id);
        $searchParams = array("{slug}", "{hexid}");
        $replaceParams = array($slug, $hexId);

        if( 1 == $type){ // gallery type
            return str_replace($searchParams, $replaceParams, $featureGalleryUrl);

        } else {
            return str_replace($searchParams, $replaceParams, $featureArticleUrl);

        }
    }

    /*
     * @author: Vu Luu
     */
    public static function getSlug($slug)
    {
        $slug = trim($slug, '/');
        $slug_check = explode('/', $slug);

        if (strpos($slug_check[count($slug_check) - 1], 'id-') !== false) {
            $slug = trim(str_replace($slug_check[count($slug_check) - 1], '', $slug), '/');
        }

        return self::slug($slug);
    }

    /**
     * @author Vu Tran
     */
//    public static function generateFeaturesUrl($id, $slug, $seo_url, $type = 0)
//    {
//      $id = (int) $id;
//      $slug = trim($slug, '/');
//      $seo_url = trim($seo_url);
//
//      $type = (int) $type; // 0: Article; 1: Gallery
//      $url = '';
//
//      if ( $type == 0 ) {
//          // for Article /[article-slug]/aid-[hash-id]/in/[sub-page-seo-url]4x
//          $url = strtr('/[article-slug]/aid-[id]/in/[sub-page-seo-url]', array(
//                  '[article-slug]'        => $slug,
//                  '[id]'                  => $id,
//                  '[sub-page-seo-url]'    => $seo_url
//          ));
//      } elseif ( $type == 1 ) {
//          // for Gallery /[gallery-slug]/gid-[hash-id]/in/[sub-page-seo-url]
//          $url = strtr('/[gallery-slug]/gid-[id]/in/[sub-page-seo-url]', array(
//                  '[gallery-slug]'        => $slug,
//                  '[id]'                  => $id,
//                  '[sub-page-seo-url]'    => $seo_url
//          ));
//      }
//
//      return $url;
//    }

    /**
     * @author Vu Tran
     */
    public static function generateFeaturesUrlFromRouting($id, $type, $slug)
    {
        $id = self::idToUrlId((int)$id);
        $type = (int)$type; // 0: Article; 1: Gallery
        $slug = trim($slug, '/');

        if ($type == 0) {
            return self::$_generator->generate('in_sing_features_article', array(
                'slug'  => $slug,
                'hexId' => $id
            ));
        } elseif ($type == 1) {
            return self::$_generator->generate('in_sing_features_gallery_slide', array(
                'slug' => $slug,
                'hexId' => $id
            ));
        }
    }
    /**
     * @author Cuong.Au
     */
    public static function generateFeaturesUrlCustomFromRouting($id, $type, $slug,$slugCustom)
    {
        $id = self::idToUrlId((int)$id);
        $type = (int)$type; // 0: Article; 1: Gallery
        $slug = trim($slug, '/');

        if ($type == 0) {
            return self::$_generator->generate('in_sing_features_article_custom_page', array(
                'slug'  => $slug,
                'hexId' => $id,
                'slug_custom'   => $slugCustom,
            ));
        } elseif ($type == 1) {
            return self::$_generator->generate('in_sing_features_gallery_slide_custom_page', array(
                'slug' => $slug,
                'hexId' => $id,
                'slug_custom'   => $slugCustom
            ));
        }
    }

    /**
     * @author Cuong.Bui
     */
    public static function generateSeriesUrl($series, $series_slug, $channel, $channel_domains)
    {
        $series      = trim($series);
        $series_slug = trim($series_slug);
        $channel     = strtolower($channel);
        $url         = '';

        if ($series_slug != '') {
            if (isset($channel_domains[$channel])) {
                $url         = $channel_domains[$channel];
                $series_slug = trim(trim($series_slug, '/'));

                if ($series_slug != '') {
                    if ($channel != self::HGW) {
                        $url .= '/series/' . $series_slug;
                    } else {
                        $url .= '/dining-guide/' . $series_slug;
                    }
                }
            }
        }

        return $url;
    }

    /**
     * @author Cuong.Bui
     */
    public static function generateArticleEditor($author)
    {
        $author = trim($author);
        $ret = $author;

        if ($author != '') {
            $editors_array = explode(',', $author);
            if (!empty($editors_array) && count($editors_array) > 1) {
                $index = mt_rand(0, abs(count($editors_array) - 1));

                if (isset($editors_array[$index])) {
                    $ret = trim($editors_array[$index]);
                }
            }
        }

        return $ret;
    }

    /**
     * @author Unknown (edited Cuong.Bui)
     */
//    public function getChannelDomains($channel)
//    {
//        $channel = strtolower($channel);
//        $channel_domains = $this->container->getParameter('insing_channel_domains');
//
//        return isset($channel_domains[$channel]) ? $channel_domains[$channel] : '';
//    }

    /**
     * @author Unknown (edited Cuong.Bui)
     */
//    public function getChannelName($channel)
//    {
//        $channel = strtolower($channel);
//        $channel_names = $this->container->getParameter('insing_channel_name');
//
//        return isset($channel_names[$channel]) ? $channel_names[$channel] : '';
//    }

    /**
     * Generate Photo url with Size & Sig
     *
     * @author Vu Tran
     * @param String $photo_url
     * @param String $photo_width
     * @param String $photo_height
     * @return Array $photo
     *
     * sample return array:
     *     array (
     *         'base_photo_url'     => 'http://local.cdn/rnr/4653/res-b-8f42b9b95d4d1b6a85637af37cf8595d.jpg'
     *         'photo_url'          => 'http://local.cdn/rnr/4653/res-b-8f42b9b95d4d1b6a85637af37cf8595d_300x300_b18222f6d4.jpg'
     *         'photo_name_full'    => 'res-b-8f42b9b95d4d1b6a85637af37cf8595d_300x300_b18222f6d4.jpg'
     *         'photo_name'         => 'res-b-8f42b9b95d4d1b6a85637af37cf8595d'
     *         'photo_width'        => '143'
     *         'photo_height'       => '107'
     *         'photo_ext'          => 'jpg'
     *         'photo_secret'       => 'axkdsa3'
     *         'photo_sig'          => 'b18222f6d4'
     *     )
     *
     * sample :
     *     Common::generatePhotoFromUrl(
     *         "http://local.cdn/rnr/4653/res-b-8f42b9b95d4d1b6a85637af37cf8595d.jpg",
     *         "143x107",
     *         $this->container->getParameter('cdn_secret')
     *     );
     */
    static public function generatePhotoFromUrl($photo_url, $photo_width, $photo_height, $photo_secret=null)
    {
        //Get Photos Information from URL
        $url_info = parse_url($photo_url);
        $url_info_path = isset($url_info['path']) ? $url_info['path'] : '';
        $photo_info = pathinfo($url_info_path);
        $photo_secret = is_null($photo_secret) ? self::$_container->getParameter('insing_features_cdn_secret') : $photo_secret;

        //Create Photo Information
        $photo = array(
            'base_photo_url' => $photo_url,
            'photo_url' => '',
            'photo_name_full' => '',
            'photo_name' => (isset($photo_info) && isset($photo_info['filename'])) ? $photo_info['filename'] : '',
            'photo_width' => $photo_width,
            'photo_height' => $photo_height,
            'photo_ext' => (isset($photo_info) && isset($photo_info['extension'])) ? $photo_info['extension'] : '',
            'photo_secret' => $photo_secret
        );

        //Generate Photo Sig
        $photo['photo_sig'] = self::generatePhotoSig($photo);

        //Create File Name
        $photo['photo_name_full'] = strtr('[name]_[width]x[height]_[sig].[ext]', array(
            '[name]' => $photo['photo_name'],
            '[width]' => $photo['photo_width'],
            '[height]' => $photo['photo_height'],
            '[sig]' => $photo['photo_sig'],
            '[ext]' => $photo['photo_ext']
        ));

        //Modified Host URL if the photo_url has port inside the photo_url
        if (isset($url_info['host'])) {
            $url_info['host'] = isset($url_info['port']) ? $url_info['host'] . ':' . $url_info['port'] : $url_info['host'];
        }

        //Generate Photo Url
        if ($photo_url != '') {
            $photo['photo_url'] = strtr('[scheme]://[host][dirname]/[photo_name_full]', array(
                '[scheme]' => isset($url_info['scheme']) ? $url_info['scheme'] : '',
                '[host]' => isset($url_info['host']) ? $url_info['host'] : '',
                '[dirname]' => (isset($photo_info) && isset($photo_info['dirname'])) ? $photo_info['dirname'] : '',
                '[photo_name_full]' => $photo['photo_name_full']
            ));
        } else {
            $photo['photo_url'] = '';
        }

        return $photo;
    }

    static public function generatePhotoFromUrlExt($photo_url, $photo_width, $photo_height)
    {
        $arrUrl = generatePhotoFromUrl($photo_url, $photo_width, $photo_height);
        return $arrUrl['photo_url'];
    }

    /**
     * Generate sigature
     *
     * @author  Vu Tran
     * @param   Array   $photo_info
     * @return  String
     */
    static public function generatePhotoSig($photo)
    {
        $file_name = strtr('[name]_[width]x[height].[ext][secret]', array(
            '[name]' => $photo['photo_name'],
            '[width]' => $photo['photo_width'],
            '[height]' => $photo['photo_height'],
            '[ext]' => $photo['photo_ext'],
            '[secret]' => $photo['photo_secret']
        ));

        return substr(md5($file_name), 0, 10);
    }

    /**
     * @author Cuong.Bui
     */
//    public static function shortDescription($description, $limit = 0)
//    {
//        $description = trim($description);
//        $limit = intval($limit);
//
//        if ($limit > 0) {
//            if (strlen($description) <= $limit) {
//                return $description;
//            } else {
//                return substr($description, 0, $limit) . '...';
//            }
//        }
//
//        return $description;
//    }

    /**
     * Check the $seriesName does exist in $series
     * @author Chung Do
     * @param string $seriesName, array $series
     */
    public static function checkExistSeriesName($series, $seriesName)
    {
    	$check = false;
    	if( !is_array($series) ){
    		return $check;
    	}
    	
    	foreach ( $series as $serie ){
    		if( trim(strtolower($seriesName)) == trim(strtolower($serie['series_name'])) ){
    			$check = true;
    			break;
    		}
    		else {
    			continue;
    		}
    	}
    	
    	return $check;
    }
    
    /**
     *
     * @param type $text input text
     * @param type $maxchar max lenght
     * @param type $end
     * @author Vu Tran
     * @return result string
     */
    public static function substrWords($text, $maxchar, $end = '...')
    {
        $text = trim(strip_tags($text));
        $maxchar = (int) $maxchar;

        if ($text != '') {
            $words = explode(' ', $text);
            $combine = array();

            if (!empty($words)) {
                foreach ($words as $item) {
                    $item = trim($item);
                    if ($item != '') {
                        $combine[] = $item;
                    }
                }
            }

            if (!empty($combine)) {
                $text = trim(implode(' ', $combine));
            }
        }

        if (strlen($text) > $maxchar) {
            $words = explode(' ', $text);
            $output = '';
            $i = 0;

            while (1) {
                $length = strlen($output) + strlen($words[$i]);

                if ($length > $maxchar) {
                    break;
                } else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }

            $output .= $end;
        } else {
            $output = $text;
        }

        return trim($output);
    }

    /**
     * @author Cuong.Bui
     */
//    public static function removeDuplicatedFromFresh($result, $readItems)
//    {
//        if ($readItems && !empty($readItems)) {
//            $read = $readItems;
//
//            foreach ($result as $k => $r) {
//                $entity = $r['article_id'];
//                $url = $r['article_url'];
//
//                // --------- Check is gallery url
//                if (preg_match('/\/gallery\//', $url)) {
//                    $entity .= ':2';
//                } else {
//                    $entity .= ':1';
//                }
//                // Check is gallery url ---------
//
//                if (in_array($entity, $read)) {
//                    unset($result[$k]);
//                }
//            }
//        }
//
//        return $result;
//    }

    /**
     * @author Cuong.Bui
     */
//    public static function sortByPriorities($result, $priorities)
//    {
//        $totalArray = array();
//        $prepareArray = array();
//        $masterArray = array();
//
//        if ($priorities && !empty($priorities)) {
//            foreach ($priorities as $priority) {
//                $channel = strtolower($priority->getChannel());
//                $prepareArray[$channel] = array();
//
//                foreach ($result as $k => $r) {
//                    if (strtolower($r['channel']) == $channel) {
//                        $prepareArray[$channel][][$k] = $r;
//                    }
//                }
//
//                $totalArray[] = count($prepareArray[$channel]);
//            }
//
//            $loop = max($totalArray);
//
//            if ($loop > 0) {
//                for ($i = 0; $i < $loop; $i++) {
//                    foreach ($priorities as $priority) {
//                        $channel = strtolower($priority->getChannel());
//
//                        if (isset($prepareArray[$channel][$i])) {
//                            $key = key($prepareArray[$channel][$i]);
//                            $masterArray[$key] = $prepareArray[$channel][$i][$key];
//                        }
//                    }
//                }
//            }
//
//            if ($masterArray && !empty($masterArray)) {
//                $result = array();
//                $result = $masterArray;
//            }
//        }
//
//        return $result;
//    }


    /**
     * Return 5 Block number in case 10 Block
     *
     * @author Vu Tran
     * @param $value
     * @return integer
     */
//    public static function getFiveBlockNumber($value)
//    {
//      return intval(($value+1)/2);
//    }

    /**
     * checking odd & even block
     *
     * @author Vu Tran
     * @param $value
     * @return true:false
     */
//    public static function isOddNumber($value)
//    {
//      if ( $value & 1 ) {
//          //odd
//          return true;
//      } else {
//          //even
//          return false;
//      }
//    }

    /**
     * checking Article is Exclude or NOT
     *
     * @param $article
     * @return boolean
     */
//    public static function isExcludeArticle( $article, $channel_ex_article )
//    {
//      $result = false;
//      $channel_name = strtolower(trim($article['channel']));
//
//      //Getting config for Article Channel
//      if ( isset($channel_ex_article[$channel_name]) ) {
//          //Get Channel Series config RULE
//          $channel_series = $channel_ex_article[$channel_name];
//
//          if ( $channel_series == 'ALL' ) {
//              //Always return TRUE when filter ALL
//              $result = true;
//          } else if ( $channel_series == '' ) {
//              //Return FALSE if empty
//              $result = false;
//          } else {
//              //Getting Series rule, comma seperate
//              $channel_series = split(',', trim($channel_series));
//              $series = array();
//
//              //Modified value (lower & trim)
//              foreach ( $channel_series as $item ) {
//                  $series[] = strtolower(trim($item));
//              }
//
//              //Checking for special series rule ( have more than one series )
//              $article_series = $article['series'];
//
//              foreach ($article_series as $article_serie) {
//                  if (
//                          !$result && isset($article_serie['series_name']) &&
//                          in_array(strtolower(trim($article_serie['series_name'])), $series)
//                  ) {
//                      $result = true;
//                  }
//              }
//          }
//      } else {
//          //If don't have config => FALSE
//          $result = false;
//      }
//
//      return $result;
//    }

    /**
     * Remove EOL chars \n \r \n\r
     *
     * @param $content
     * @return $content without EOL
     */
//    public static function removeEOL($content, $replace = "")
//    {
//      $eol = array("\r\n", "\n", "\r");
//
//      // Processes \r\n's first so they aren't converted twice.
//      $result = str_replace($eol, $replace, $content);
//
//      return $result;
//    }

    /**
     * Get Breadcrumb from white_october_breadcrumbs bundle
     * @author Khuong Phan
     */
    public static function getBreadCrumb()
    {
        $breadcrumbs = null;
        if (self::$_container !== null) {
            $insing_url = self::$_container->getParameter('new_insing_url');
            $breadcrumbs = self::$_container->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Home', $insing_url);
        }

        return $breadcrumbs;
    }
    /**
     * Get Breadcrumb from white_october_breadcrumbs bundle
     * @author Khuong Phan
     */
    public static function getBreadCrumbHomePage()
    {
        $breadcrumbs = null;
        if (self::$_container !== null) {
            $insing_url = self::$_container->getParameter('new_insing_url');
            $breadcrumbs = self::$_container->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Home', $insing_url);
        }

        return $breadcrumbs;
    }

    /**
     * parse format: [[iframe:w=600:h=600]https://vine.co/v/bQePm27I3E3/embed/simple]
     * @param string $iframeCode
     * @author: BienTran
     * @return: string
     */
    public static function iframeCodeParser($iframeCode)
    {
        /* Matching codes */
        $urlmatch = "([a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+)";
        $match["iframe"] = "/\[\[iframe:w=([0-9]+(%|px)?):h=([0-9]+(%|px)?)\]\[" . $urlmatch . "\]\]/is";

        /*
        array (size=6)
            0 => string '[iframe:w=800:h=600][https://vine.co/v/bQePm27I3E3/embed/simple]'
            1 => string '800'
            2 => string ''
            3 => string '600'
            4 => string ''
            5 => string 'https://vine.co/v/bQePm27I3E3/embed/simple' (length=42)
        */

        $replace["iframe"] = "<iframe src=\"$5\" width=\"$1\" height=\"$3\" frameborder=\"0\"></iframe>";
        $iframe = preg_replace($match, $replace, $iframeCode);

        return $iframe;
    }

     /**
     * @param $publishDate, $updateDate
     * @author: ThinhNguyen
     * @return: Datetime
     */
    public static function getLatestDate($publishDate, $updateDate)
    {
        $pd= strtotime($publishDate);
        $ud = strtotime($updateDate);
        if( $ud  > $pd )
        {
            return $updateDate;
        }
        else
        {
            return $publishDate;
        }
    }
}