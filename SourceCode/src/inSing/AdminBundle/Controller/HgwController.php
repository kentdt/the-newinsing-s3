<?php
namespace inSing\AdminBundle\Controller;

use inSing\DataSourceBundle\Lib\Constant;
use inSing\AdminBundle\Form\ContentType;
use inSing\DataSourceBundle\Entity\Content;
use inSing\DataSourceBundle\Repository\ContentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HgwController extends Controller
{
    public function indexAction()
    {
        $user_token = $this->container->get('security.context')->getToken();

        if (!$user_token->isAuthenticated())
        {
            throw new \Exception('No permission.', 403);
        }

        $request = $this->get('request');

        $page = $request->query->get('page', 1);
        $hgwFilter = $request->query->get('hgw_filter', 'all');

        $params = array(
            'module' => ContentRepository::HGW_MODULE,
            'filter' => $hgwFilter
        );

        $hgwList = $this->getDoctrine()->getRepository('inSingDataSourceBundle:Content')->getList($params);

        $pagination = $this->get('knp_paginator')->paginate($hgwList, $page, $this->container->getParameter('insing_admin_max_size_per_page'));

        $hgws = array();

        foreach ($pagination as $item)
        {
            $tmp = array();
            $tmp['id'] = $item->getId();
            $tmp['title'] = $item->getTitle();
            $tmp['slot'] = $item->getSlot();

            $publishDate = $item->getPublishOn()->format('Y-m-d');

            $expireDate = $item->getExpireOn();

            if (!empty($expireDate))
            {
                $expireDate = $item->getExpireOn()->format('Y-m-d');
            }

            $currentDate = date("Y-m-d");

            if (!empty($expireDate) && $expireDate < $currentDate)
            {
                $tmp['status'] = "Expired";
            }
            else
            {
                if ($currentDate < $publishDate)
                {
                    $tmp['status'] = "Scheduled";
                }
                else
                {
                    $tmp['status'] = "Published";
                }
            }

            if ($item->getContentType() == ContentRepository::MOVIES_CHANNEL)
            {
                $tmp['content_type'] = "Movie";
            }
            elseif ($item->getContentType() == ContentRepository::EVENTS_CHANNEL)
            {
                $tmp['content_type'] = "Event";
            }
            elseif ($item->getContentType() == ContentRepository::RESTAURANT_CHANNEL)
            {
                $tmp['content_type'] = "Restaurant";
            }
            elseif ($item->getContentType() == ContentRepository::RESTAURANT_DEALS_CHANNEL)
            {
                $tmp['content_type'] = "Restaurant Deal";
            }
            elseif ($item->getContentType() == ContentRepository::HUNGRY_DEALS_CHANNEL)
            {
                $tmp['content_type'] = "Hungry Deal";
            }
            elseif ($item->getContentType() == ContentRepository::ARTICLE_CHANNEL)
            {
                $tmp['content_type'] = "Article";
            }
            elseif ($item->getContentType() == ContentRepository::GALLERY_CHANNEL)
            {
                $tmp['content_type'] = "Gallery";
            }
            else
            {
                $tmp['content_type'] = "";
            }

            $hgws[] = $tmp;
        }

        return $this->render('inSingAdminBundle:Hgw:list.html.twig', array(
            'pager' => $pagination,
            'hgws' => $hgws,
            'hgw_filter' => $hgwFilter
        ));
    }

    /**
     * @author Vu.Luu
     */
    public function addAction()
    {
        $user_token = $this->container->get('security.context')->getToken();
        if (!$user_token->isAuthenticated()) {
            throw new \Exception('No permission.', 403);
        }

        $request = $this->get('request');
        $session = $this->get('session');

        $filed_uploaded = false;

        $files = $request->files->all();
        $file = isset($files['admin_content']['image_browse']) ? $files['admin_content']['image_browse'] : false;
        //check upload file, if uploaded is remove costraints  "image_url"
        if ($file) {
            $filed_uploaded = true;
        }

        $contentObj = new Content();

        $form = $this->createForm(new ContentType($this->getDoctrine(), $this->container, array('type' => 'add', 'module' => 'hgw', 'file_uploaded' => $filed_uploaded)), $contentObj);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $extraInfo = array();
                $data = $request->request->get('admin_content');

                //Upload Image
                if ($file) {
                    $imageUrl = UtilHelper::uploadPhoto($file, $this->container);
                    if ($imageUrl) {
                        $contentObj->setImageUrl($imageUrl);
                    } else {
                        $session->getFlashBag()->add('error', 'Oops! Can not upload image. Please try again');
                        return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
                    }
                }

                //save extra info
                if (null != $data['deal_id']) {
                    $extraInfo['deal_id'] = $data['deal_id'];
                }
                if (null != $data['tabledb_id']) {
                    $extraInfo['tabledb_id'] = $data['tabledb_id'];
                }
                if (null != $data['slug']) {
                    $extraInfo['slug'] = $data['slug'];
                }
                if (null != $data['event_data']) {
                    $extraInfo['event_data'] = $data['event_data'];
                }
                if (null != $data['deal_id']) {
                    $contentObj->setExtraField(json_encode($extraInfo));
                }
                if (null != $data['series_seo_url']) {
                    $extraInfo['series_seo_url'] = $data['series_seo_url'];
                }

                $contentObj->setCreatedAt(new \DateTime('now'));
                $contentObj->setStatus(Constant::STATUS_ACTIVE);
                if (count($extraInfo) > 0) {
                    $contentObj->setExtraField(json_encode($extraInfo));
                }

                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($contentObj);
                    $em->flush();

                    $session->getFlashBag()->add('success', 'Changes saved successfully.');
                } catch (\Exception $e){
                    $session->getFlashBag()->add('error', 'Oops! An Error Occurred (' . $e->getMessage() . ').');
                }

                return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
            }
        }

        return $this->render('inSingAdminBundle:Hgw:edit.html.twig', array(
            'form'         => $form->createView(),
            'type'         => 'add',
            'id'           => ''
        ));
    }

    /**
     * @author Vu.Luu
     */
    public function editAction($id)
    {
        $user_token = $this->container->get('security.context')->getToken();
        if (!$user_token->isAuthenticated()) {
            throw new \Exception('No permission.', 403);
        }

        $request = $this->get('request');
        $session = $this->get('session');

        $filed_uploaded = false;

        if ($id != '') {
            $contentObj = $this->getDoctrine()->getRepository('inSingDataSourceBundle:Content')->findOneById($id);
        } else {
            $session->getFlashBag()->add('error', 'Unable to edit, please try again later.');
            return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
        }

        $files = $request->files->all();
        $file = isset($files['admin_content']['image_browse']) ? $files['admin_content']['image_browse'] : false;
        //check upload file, if uploaded is remove costraints  "image_url"
        if ($file) {
            $filed_uploaded = true;
        }

        $form = $this->createForm(new ContentType($this->getDoctrine(), $this->container, array('type' => 'edit', 'module' => 'hgw', 'file_uploaded' => $filed_uploaded, 'id' => $id)), $contentObj);

        if ($request->isMethod('POST')) {
            $form->bind($request);

            //Upload Image
            if ($file) {
                $imageUrl = UtilHelper::uploadPhoto($file, $this->container);
                if ($imageUrl) {
                    $contentObj->setImageUrl($imageUrl);
                } else {
                    $session->getFlashBag()->add('error', 'Oops! Can not upload image. Please try again');
                    return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
                }
            }

            if ($form->isValid()) {
                try {
                    $extraInfo = array();
                    $data = $request->request->get('admin_content');
                    //save extra info
                    if (null != $data['deal_id']) {
                        $extraInfo['deal_id'] = $data['deal_id'];
                    }
                    if (null != $data['tabledb_id']) {
                        $extraInfo['tabledb_id'] = $data['tabledb_id'];
                    }
                    if (null != $data['slug']) {
                        $extraInfo['slug'] = $data['slug'];
                    }
                    if (null != $data['event_data']) {
                        $extraInfo['event_data'] = $data['event_data'];
                    }
                    if (null != $data['series_seo_url']) {
                        $extraInfo['series_seo_url'] = $data['series_seo_url'];
                    }

                    $contentObj->setUpdatedAt(new \DateTime('now'));
                    if (count($extraInfo) > 0) {
                        $contentObj->setExtraField(json_encode($extraInfo));
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($contentObj);
                    $em->flush();

                    $session->getFlashBag()->add('success', 'Changes saved successfully.');
                } catch (\Exception $e) {
                    $session->getFlashBag()->add('error', 'Oops, error occurred (' . $e->getMessage() . ').');
                }

                return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
            }

        }

        return $this->render('inSingAdminBundle:Hgw:edit.html.twig', array(
            'form'         => $form->createView(),
            'type'         => 'edit',
            'id'           => $id,
        ));
    }

    public function deleteAction($id)
    {
        $user_token = $this->container->get('security.context')->getToken();

        if (!$user_token->isAuthenticated())
        {
            throw new \Exception('No permission.', 403);
        }

        $request = $this->get('request');
        $session = $this->get('session');

        $id = $request->attributes->get('id', '');

        $em = $this->getDoctrine()->getManager();
        $contentObj = $this->getDoctrine()->getRepository('inSingDataSourceBundle:Content')->findOneById($id);

        if (!$contentObj)
        {
            $session->getFlashBag()->add('error', 'Unable to delete, please try again later.');
            return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
        }

        try
        {
            $contentObj->setStatus(Constant::STATUS_INACTIVE);
            $em->persist($contentObj);
            $em->flush();

            $session->getFlashBag()->add('success', 'Deleted successfully.');
        }
        catch (Exception $e)
        {
            $session->getFlashBag()->add('error', 'Unable to delete, please try again later (' . $e->getMessage() . ').');
        }

        return $this->redirect($this->generateUrl('in_sing_admin_hgw'));
    }
}