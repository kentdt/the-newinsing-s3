<?php
namespace inSing\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\InMemoryUserProvider;
use Symfony\Component\Security\Core\User\User;

class AuthController extends Controller
{
    /**
     * Login action with oAuth access
     * @author Cuong.Bui
     */
    public function indexAction()
    {
        $session = $this->get('session');

        if ($session->has('_security_secured_area')) {
            return $this->redirect($this->generateUrl('in_sing_admin_carousel', array(), true));
        }

        $session->set('redirectUrl', $session->get('_security.secured_area.target_path'));
        $this->clearCredentials();

        $consumerKey = $this->container->getParameter('insing_admin_consumer_key');
        $consumerSecret = $this->container->getParameter('insing_admin_consumer_secret');
        $requestTokenUrl = $this->container->getParameter('insing_admin_oauth_request_token_url');
        $callBackUrl = $this->generateUrl('in_sing_admin_auth_ready', array(), true);

        $oauth = new \OAuth($consumerKey, $consumerSecret);
        $oauth->setSSLChecks(1);

        // STEP 1: Request token
        try {
            $requestToken = $oauth->getRequestToken($requestTokenUrl, $callBackUrl);
        } catch (\Exception $e) {
            throw new \Exception('Error, can not request token (' . $e->getMessage() . ')', 400);
        }

        // STEP 2: Authorize
        if (!is_null($requestToken) && !empty($requestToken)) {
            $response = json_decode($oauth->getLastResponse(), true);

            if (is_array($response) && array_key_exists('status', $response)) {
                if ($response['status'] == 200) {
                    $session->set('oAuthTokenSecret', $response['oauth_token_secret']);
                    $url = $this->container->getParameter('insing_admin_oauth_authorize_url') . '?oauth_token=' . $response['oauth_token'];

                    return $this->redirect($url);
                } else {
                    throw new \Exception($response['error'], 400);
                }
            }
        }

        throw new \Exception('Invalid request token', 400);
    }

    /**
     * Callback URL after authorization from OAuth server
     * @author Cuong.Bui
     */
    public function authReadyAction()
    {
        $Request = $this->get('request');
        $session = $this->get('session');

        $this->clearCredentials();

        $consumerKey = $this->container->getParameter('insing_admin_consumer_key');
        $consumerSecret = $this->container->getParameter('insing_admin_consumer_secret');
        $accessTokenUrl = $this->container->getParameter('insing_admin_oauth_access_token_url');
        $userRoleUrl = $this->container->getParameter('insing_admin_oauth_user_role_url');
        $oAuthTokenSecret = $session->get('oAuthTokenSecret');

        $oauth = new \OAuth($consumerKey, $consumerSecret);
        $oauth->setSSLChecks(1);

        $query = $Request->query->all();
        if (!array_key_exists('oauth_token', $query) && !array_key_exists('oauth_verifier', $query)) {
            return $this->redirect($this->generateUrl('in_sing_admin_login'));
        }

        // STEP 3: Access token
        try {
            $oauth->setToken($query['oauth_token'], $oAuthTokenSecret);
            $accessToken = $oauth->getAccessToken($accessTokenUrl, '', $query['oauth_verifier']);
        } catch (\Exception $e) {
            throw new \Exception('Error, can not access token (' . $e->getMessage() . ')', 400);
        }

        // STEP 4: Get user role
        if (!is_null($accessToken) && !empty($accessToken)) {
            $response = json_decode($oauth->getLastResponse(), true);

            if (is_array($response) && array_key_exists('status', $response)) {
                if ($response['status'] == 200) {
                    try {
                        $oauth->setToken($response['oauth_token'], $response['oauth_token_secret']);
                        $oauth->fetch($userRoleUrl, null, OAUTH_HTTP_METHOD_GET);
                        $response = json_decode($oauth->getLastResponse(), true);
                    } catch (\Exception $e) {
                        throw new \Exception('Error, can not get user role (' . $e->getMessage() . ')', 400);
                    }
                } else {
                    throw new \Exception($response['error'], 400);
                }

                if (!is_null($response) && !empty($response)) {
                    if (is_array($response) && array_key_exists('status', $response)) {
                        if ($response['status'] == 200) {
                            /*
                              Receive User credentials from protected resources and set into SecurityContext & Session
                              Use $this->get('security.context')->isGranted(array('ROLE_ADMIN', 'ROLE_CHECKER'...)) to check action permission
                             */

                            $username = $response['user']['display_name'];
                            $credentials = $response['user']['id'];
                            $roles = array();
                            foreach ($response['roles'] as $item) {
                                $roles[] = 'ROLE_' . $item;
                            }

                            $MemoryUserProvider = new InMemoryUserProvider();
                            $MemoryUserProvider->createUser(new User($username, $credentials, $roles));

                            $token = new UsernamePasswordToken($username, $credentials, 'secured_area', $roles);
                            $session->set('_security_secured_area', serialize($token));
                            $this->get('security.context')->setToken($token);

                            // Redirect to last referer url
                            //$redirectUrl = $session->get('redirectUrl');

                            //if ($redirectUrl == '') {
                            //    $redirectUrl = $this->generateUrl('in_sing_admin_carousel');
                            //}

                            //change logic: after login will redirect to carousel list
                            $redirectUrl = $this->generateUrl('in_sing_admin_carousel');
                            return $this->redirect($redirectUrl);
                        } else {
                            throw new \Exception($response['error'], 400);
                        }
                    }
                }

                throw new \Exception('Access token has been used before', 400);
            }
        }

        throw new \Exception('Invalid access token', 400);
    }

    /**
     * Logout
     * @author Cuong.Bui
     */
    public function logoutAction()
    {
        $this->clearCredentials();

        return $this->redirect($this->generateUrl('in_sing_admin_carousel'));
    }

    /**
     * Clear Session & Security context token
     * @author CuongBui
     */
    private function clearCredentials()
    {
        $session = $this->get('session');

        $session->remove('_security_secured_area');
        $this->get('security.context')->setToken(null);
    }
}