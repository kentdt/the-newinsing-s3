$(document).ready(function() {    
    $('#button-get-content').on('click', function() {
        $('#error-content-id').hide();
        $('#img-loading').show();
        $('#button-content-get-content').attr('disabled', 'disabled');

        var type = $('#admin_content_content_type').val();
        var id   = $('#admin_content_item_id').val();
        var module = $('#admin_content_module').val();

        if (id != '' ) {
            $.ajax({
                url : $('#get-content-url').val(),
                type: 'GET',
                data: {
                    'module': module,
                    'type': type,
                    'id'  : id
                },
                success: function(response) {
                    if (typeof response.status != 'undefined' && response.status == 200 && response.result != '') {
                        var details = response.result;
                        var deals_content = response.result_html;

                        //show deal list popup
                        if (deals_content) {
                            showDealsPopup(deals_content);
                        } else {
                            $('#admin_content_title').val(details.title);
                            $('#admin_content_description').val(details.description);
                            $('#admin_content_content_url').val(details.content_url);
                            $('#admin_content_channel_name').val(details.channel);
                            $('#admin_content_editor').val(details.editor);
                            $('#admin_content_tabledb_id').val(details.tabledb_id);
                            $('#admin_content_slug').val(details.slug);
                            $('#admin_content_event_data').val(details.event_data);
                            $('#admin_content_series_seo_url').val(details.series_seo_url);

                            if ('hgw' == module) {
                                $('#admin_content_image_url').val(details.image_url);
                            }
                        }

                        $taglines = response.taglines;
                        if($taglines.length > 0) {
                            $('#admin_content_tagline_1').val($.trim($taglines[0]));
                            $('#admin_content_tagline_2').val($.trim($taglines[1]));
                            $('#admin_content_tagline_3').val($.trim($taglines[2]));
                        }

                        //remove all error class after pull data success
                        $('.error').text('');
                        $('#img-loading').hide();
                        $('#button-content-get-content').removeAttr('disabled');
                    } else {
                        $('#admin_content_title').val('');
                        $('#admin_content_description').val('');
                        $('#admin_content_content_url').val('');
                        $('#admin_content_image_url').val('');
                        $('#admin_content_channel_name').val('');
                        $('#admin_content_editor').val('');
                        $('#admin_content_tabledb_id').val('');
                        $('#admin_content_slug').val('');
                        $('#admin_content_event_data').val('');
                        $('#admin_content_series_seo_url').val('');

                        $('#admin_content_tagline_1').val('');
                        $('#admin_content_tagline_2').val('');
                        $('#admin_content_tagline_3').val('');


                        $('#error-content-id').text(response.message);
                        $('#error-content-id').show();

                        $('#img-loading').hide();
                        $('#button-content-get-content').removeAttr('disabled');
                    }
                },
                error: function() {
                    alert('Oops, error occurred.');

                    $('#img-loading').hide();
                    $('#button-content-get-content').removeAttr('disabled');
                }
            });
        } else {
            $('#error-content-id').text('Please input ID to get data.');
            $('#error-content-id').show();
            $('#img-loading').hide();
            $('#button-content-get-content').removeAttr('disabled');
        }
    });
});

function showDealsPopup($details)
{
    $.colorbox({innerWidth:600,innerHeight:600,html:$details});
}
