<?php
namespace inSing\ApiBundle\Util;
use FOS\RestBundle\Util\Codes;
//use JMS\SecurityExtraBundle\Security\Util\String;
use Symfony\Component\DependencyInjection\ContainerAware;
use FOS\RestBundle\Util\Codes as APICode;

class IsAPI extends ContainerAware
{
    protected $container;
    protected $logger;

    /**
     * Constructor (need to set the environment)
     * PS. I use private cos I dont want DEV to create object directly
     *
     * @author  Trung Nguyen
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->logger = $this->container->get("monolog.logger.features_insing_api");
    }

    public function getAllFeaturesContent()
    {
        $this->logger->addInfo('===== BEGIN Get All Features Content =====');
        $restful = $this->container->get('FeaturesTable');
        $allFeaturesContent = $restful->getFeaturesContent();
        //Log action
        $this->logger->addInfo('===== END Get All Features Content');
        return $allFeaturesContent;
    }

    public function getAllEventsContent()
    {
        $this->logger->addInfo('===== BEGIN Get All Events Content =====');
        $restful = $this->container->get('EventsTable');
        $allEventsContent = $restful->getEventsContent();
        //Log action
        $this->logger->addInfo('===== END Get All Events Content');
        return $allEventsContent;

    }

    public function getAllMoviesContent()
    {
        $this->logger->addInfo('===== BEGIN Get All Movies Content =====');
        $restful = $this->container->get('MoviesTable');
        $allMoviesContent = $restful->getMoviesContent();
        //Log action
        $this->logger->addInfo('===== END Get All Movies Content');
        return $allMoviesContent;

    }

}
?>
