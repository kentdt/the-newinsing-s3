<?php
namespace inSing\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\JsonResponse,
    FOS\RestBundle\Util\Codes as APICode,
    API\CoreBundle\Util\InsingAPI,
    API\CoreBundle\Util\Helper as APIHelper;
use API\ApiBundle\Util\RestfulAPIHelper;

class TestController extends APIBaseController {  

    function kenttestAction(Request $request) {
        $logger = $this->container->get('insing.util.logger');        
        $logger->addInfoToFile(" ================= start write log Kent ================= ");
        /* return $this->output(APICode::HTTP_OK, array(
          'status' => (string) APICode::HTTP_OK,
          'result' => array('ngon', 'win')
          )); */
        return new JsonResponse(array(
            'status' => (string) APICode::HTTP_OK,
            'result' => array('ngon', 'win')));
    }

}
