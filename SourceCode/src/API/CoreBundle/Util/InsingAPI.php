<?php
/**
 * Wrapper inSing API class
 *
 * <b>Example</b>
 * <code>
 * $inSingAPI = InsingApiService::getInstance(InsingApiService::DEV_ENV);
 * try
 * {
 *	   $userProfile = $inSingAPI->getUserProfile('tung.co');
 *	   if (count($userProfile) > 0) {
 *		   // bla bla...
 *	   }
 * } catch (Exception $e) {
 *	   sprintf('Code: %s - Message: %s', $e->getCode(), $e->getMessage());
 * }
 * </code>
 *
 * @package API
 * @author	Trung.Nguyen
 */

namespace API\CoreBundle\Util;

use inSing\UtilBundle\Cache\Cache;

use FOS\RestBundle\Util\Codes;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class InSingAPI
{
	const DEV_ENV = 0;
	const PROD_ENV = 1;
	const STAG_ENV = 2;

	const C_INIT_SESSION_SUCCESS = 1;
	const C_INIT_SESSION_FAIL = 2;
	const C_SESSION_EXPIRE = 3;

	const C_PROCESS_REQUEST_FAIL = 4;
	const C_FETCH_DATA_COMPLETE = 5;
	const C_PARAMETER_REQUIRED = 6;

	const CODE_POSTER_BIG = 'pc_189x272';
	const CODE_POSTER_SMALL = 'stb_150x150';
	const CODE_TRAILER = 'pc_264_aac_240x192';
	const CHAIN_LOGO_URL = 'http://singtel.dc2go.net/insingmovie/ws/images/cinema/logo';

	protected $env;
	protected $serviceUrl = array(
		'auth' => '/1.0/authenticate?wsdl',
		'user' => '/1.0/user?wsdl',
		'business' => '/2.0/search/business?wsdl',
		'auth_rest' => '/1.0/authenticate/login',
		'business_details'	=> '/3.0/business/details'
	);

	private $sessionToken = null;
	private $sessionRestToken = null;
	private $info = array();

	protected $container;
	protected $cache;
	
	/**
	 * Constructor (need to set the environment)
	 * PS. I use private cos I dont want DEV to create object directly
	 *
	 * @author Trung.Nguyen
	 * @param int $env
	 */
	public function __construct($container)
	{
		$this->container 			= $container;
		$insing_api_env 			= $this->container->getParameter('rnr_admin_api_env');
		$insing_api_client_id 		= $this->container->getParameter('rnr_admin_api_client_id');
		$insing_api_password 		= $this->container->getParameter('rnr_admin_api_password');
		$insing_api_rest_client_id 	= $this->container->getParameter('rnr_admin_api_rest_client_id');
		$insing_api_rest_password 	= $this->container->getParameter('rnr_admin_api_rest_password');

		switch ($insing_api_env) {
			case 'test':
				$env = self::DEV_ENV;
				break;

			default:
				$env = self::PROD_ENV;
				break;
		}

		$this->env = $env;
		$this->info = array(
			array(
				'host' 				=> 'http://services-sandbox.insing.com/api',
				'host_rest' 		=> 'http://services-sandbox.insing.com/api/rest',
				'client_id' 		=> $insing_api_client_id,
				'password' 			=> $insing_api_password,
				'rest_client_id' 	=> $insing_api_rest_client_id,
				'rest_password' 	=> $insing_api_rest_password,

			),
			array(
				'host' 				=> 'http://services.insing.com/api',
				'host_rest' 		=> 'http://services.insing.com/api/rest',
				'client_id' 		=> $insing_api_client_id,
				'password' 			=> $insing_api_password,
				'rest_client_id' 	=> $insing_api_rest_client_id,
				'rest_password' 	=> $insing_api_rest_password,
			)
		);

		$this->cache = $container->get('new.insing.cache');
	}

	/**
	 * Make session
	 * @author Cuong.Bui
	 *
	 * @return String
	 */
	protected function makeSession()
	{
		try {
			$arrInput = array(
				'deviceIdentifier' => array('type' => 'IMEI', 'identifier' => '9876543210'),
				'clientId' => $this->info[$this->env]['client_id'],
				'password' => $this->info[$this->env]['password']
			);

			$this->writelogFileApiCaller('Make Session => call SoapClient', $arrInput);
			$client = new \SoapClient($this->info[$this->env]['host'] . $this->serviceUrl['auth'], array('exceptions' => 1));
			$result = $client->login($arrInput);

			$this->writelogFileApiCaller('Make Session', $arrInput, $result);

			if ($result->resultCode == 200) {
				return $result->sessionToken;
			} else {
				throw new \Exception($this->getResultCodeDesc($this->resultCode, 'Can\'t get session token'), self::C_INIT_SESSION_SUCCESS);
			}
		} catch (\Exception $e) {
			$this->writelogFileApiCaller('Make Session with Try Catch Block', $arrInput, $e);
			throw $e;
		}
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method	make rest session
	* @param
	* @return
	* @since	Sep 14, 2012 4:37:21 PM
	*/
	protected function makeRestSession()
	{
		$url = $this->info[$this->env]['host_rest'].$this->serviceUrl['auth_rest'];

		$post = array(
			'clientId' => $this->info[$this->env]['rest_client_id'],
			'password' => $this->info[$this->env]['rest_password'],
			'deviceIdentifier.identifier' => uniqid(),
			'deviceIdentifier.type' => 'IMEI'
		);

		$result = $this->doPostMethod($url, $post);

		$this->writelogFileApiCaller('Make Session with Rest', $post, $result);

		if ($result['resultCode'] == 200) {
			$this->sessionRestToken = $result['sessionToken'];
			return $this->sessionRestToken;
		}

		return '';
	}


	/**
	 * Generate message base on result code from inSing
	 *
	 * @author Trung.Nguyen
	 * @param int $code
	 * @param string $default_msg
	 * @return string
	 */
	final protected function getResultCodeDesc($code, $default_msg = '')
    {
		switch ($code) {
			case 200:
				$msg = 'Login Successful.';
				break;

			case 201:
				$msg = 'The search query returned no results.';
				break;

			case 400:
				$msg = 'Invalid Session Token.';
				break;

			case 401:
				$msg = 'The login credentials are invalid.';
				break;

			case 402:
				$msg = 'Invalid Request.';
				break;

			case 403:
				$msg = 'Invalid request, session token missing.';
				break;

			case 404:
				$msg = 'Invalid Request. Date must be in yyyymmdd format.';
				break;

			case 405:
				$msg = 'You are not authorized to perform this operation.';
				break;

			case 500:
				$msg = 'Unknown Server Error.';
				break;

			case 501:
				$msg = 'Service is temporarily unavailable.';
				break;

			default:
				$msg = $default_msg;
		}

		return $msg;
	}

	/**
	 * Get session token
	 *
	 * @author	Trung.Nguyen
	 * @return	void
	 * @throws	Exception
	 */
	protected function sessionStart($force_new = false)
	{
		/*
		if (!is_null($this->getSessionToken())) {
			return true;
		}
		*/

		try {
			$cache = $this->cache;

			$params = array();
			$params['prefix'] = 'rnradmin_session_cache';
			$cache_name = $cache->createCacheName($params);

			if ($force_new) {
				$this->sessionToken = $this->makeSession();
				$cache->setCache($cache_name, $this->sessionToken);
			} else {
				if (!$cache->hasCache($cache_name)) {
					$this->sessionToken = $this->makeSession();
					$cache->setCache($cache_name, $this->sessionToken);
				} else {
					$this->sessionToken = $cache->getCache($cache_name);
				}
			}
		} catch (\Exception $e) {
			$this->writelogFileApiCaller('sessionStart with Try Catch Block', $params, $e);
			throw $e;
		}
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method
	* @param
	* @return
	* @since	Sep 14, 2012 4:35:18 PM
	*/
	protected function sessionRestStart($force_new = false)
	{
		try {
			$cache = $this->cache;

			$params = array();
			$params['prefix'] = 'rnradmin_rest_session_cache';
			$cache_name = $cache->createCacheName($params);

			if ($force_new) {
				$this->sessionRestToken = $this->makeRestSession();
				$cache->setCache($cache_name, $this->sessionRestToken);
			} else {
				if (!$cache->hasCache($cache_name)) {
					$this->sessionRestToken = $this->makeRestSession();
					$cache->setCache($cache_name, $this->sessionRestToken, 600);
				} else {
					$this->writelogFileApiCaller('get cache name', array($cache_name),array($cache->getCache($cache_name)));
					$this->sessionRestToken = $cache->getCache($cache_name);
				}
			}
		} catch (\Exception $e) {
			$this->writelogFileApiCaller('sessionRestStart with Try Catch Block', $params, $e);
			throw $e;
		}
	}

	/**
	 * Get new session token in case of expiry
	 *
	 * @author	Trung.Nguyen
	 * @return	void
	 */
	public function renewSession()
	{
		$this->sessionStart(true);
	}

	/**
	 * Get current session token
	 *
	 * @author	Trung.Nguyen
	 * @return	string
	 */
	public function getSessionToken()
	{
		return $this->sessionToken;
	}

	/**
	 *
	 * Cal soap method and hanlde some exceptions
	 *
	 * @author Co Vu Thanh Tung
	 * @param SoapClient $object
	 * @param String $function
	 * @param Array	$param
	 *
	 */
	private function callSoapMethod($object, $function, $param)
	{
		try {
			$result = call_user_func_array(array($object, $function), array($param));

			$this->writelogFileApiCaller('callSoapMethod', $param, $result);

			switch ($result->resultCode) {
				case 400:
					$this->sessionStart(true);
					$result = call_user_func_array(array($object, $function), array($param));
					break;

				case 200:
					return $result;

				default:
					return $result;
			}
		} catch (\Exception $e) {
			$this->writelogFileApiCaller('callSoapMethod with Try Catch Block', $param, $e);
			throw $e;
		}
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method	curl
	* @param
	* @return
	* @since	Sep 14, 2012 2:47:24 PM
	*/

	protected function doPostMethod($url, $fields)
	{
		// form up variables in the correct format for HTTP POST
		$fields_string = '';

		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}

		$fields_string = trim($fields_string, '&');

		// initialize cURL
		$ch = curl_init();

		// set options for cURL
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		// execute HTTP POST request
		$response = curl_exec($ch);

		// close connection
		curl_close($ch);

		return json_decode($response, true);
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method	post muiltiple bussinessID with string
	* @param
	* @return
	* @since	Sep 17, 2012 9:33:42 AM
	*/
	private function curlPostString($url, $post, array $options = array())
	{
		$defaults = array(
			CURLOPT_POST 			=> 1,
			CURLOPT_URL 			=> $url,
			CURLOPT_HEADER 			=> 0,
			CURLOPT_TIMEOUT 		=> 20,
			CURLOPT_FORBID_REUSE 	=> 1,
			CURLOPT_FRESH_CONNECT 	=> 1,
			CURLOPT_RETURNTRANSFER 	=> 1,
			CURLOPT_SSL_VERIFYPEER 	=> 0,
			CURLOPT_SSL_VERIFYHOST 	=> 0,
			CURLOPT_POSTFIELDS 		=> $post,
		);

		$ch = curl_init();
		curl_setopt_array($ch, ($options + $defaults));
		$result = curl_exec($ch);
		curl_close($ch);

		return json_decode($result, true);
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method
	* @param
	* @return
	* @since	Sep 14, 2012 5:23:02 PM
	*/
	protected function doGetMethod($url, $fields)
	{
		$fields_string = '?';

		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
        }

		$fields_string = trim($fields_string, '&');

		// initialize cURL
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url.$fields_string);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = trim(curl_exec($ch));
		curl_close($ch);

		return json_decode($response, true);
	}


	/**
	 *
	 * @param string $url
	 * @param string $secret
	 * @param array json response
	 */
	private function getFieldsStringWithSignature($url, $fields)
	{
        $fieldsString = '';

        //Generate API Signature
        $pathinfo = parse_url($url); //Get Pathinfo for gen signature
        $pathinfo = str_replace('/', '', $pathinfo['path']);
        $pathinfo = $this->info[$this->env]['rest_password'] . $pathinfo;

        foreach($fields as $key => $value) {
        	if (!is_numeric($key)) {
        		//Not number
        		$fieldsString .= $key . '=' . urlencode($value) . '&';
        	} else {
        		//Is number
        		$fieldsString .= 'businessId=' . urlencode($value) . '&';
        	}
        }

        $fieldsString .= 'sig=' . $this->genSignatureString($fields, $pathinfo);
        $this->writelogFileApiCaller('Gen String signature',$fields, $fieldsString);

        return $fieldsString;

	}

	/**
	 *
	 * @param type $params
	 * @param type $pathinfo
	 * @return string signature with hash MD5
	 */
	private function genSignatureString($params, $pathinfo)
	{
		$result = $pathinfo;
		$sessionTokenString = '';

		foreach ($params as $key => $value) {
			if (!is_numeric($key)) {
				//Not number => This is the session token
				$sessionTokenString = $key . urlencode($value);
			} else {
				//Is number
				$result .= 'businessId' . $value;
			}
		}

		$result .= $sessionTokenString;
		
		return md5($result);
	}

	/**
	 *
	 * @param type $params
	 * @param type $pathinfo
	 * @return string signature with hash MD5
	 */
	private function genSignature($params, $pathinfo)
	{
		$result = $pathinfo;
		ksort($params);

		foreach ($params as $key => $value) {
			$result .= $key . urlencode($value);
		}
		
		return md5($result);
	}

	public function getUserFullName($listUser = array())
	{
		$info = $this->getUserDetails( $listUser );

		return isset($info['name']) ? $info['name'] : '';
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method	get user details from insing
	* @param	$id or array(1,2,3)	or array(oneElement)
	* @return
	* @since	Sep 12, 2012 5:54:23 PM
	*/
	public function getUserDetails($listUser = array())
	{
		if (sizeof($listUser) == 0) {
			return array( 'id' => '', 'firstName' => '', 'lastName' => '', 'name' => '', 'email' => '', 'insingUserName' => '' );
		}

		$this->sessionStart();

		try {
			$api_params = array();
			$api_params['sessionToken']	= $this->sessionToken;
			$api_params['userId']		= $listUser;

			$this->writelogFileApiCaller('getUserDetails => call SoapClient', $api_params);
			$client = new \SoapClient($this->info[$this->env]['host'] . $this->serviceUrl['user'], array('exceptions' => 1, 'connection_timeout' => 3));

			$result = $this->callSoapMethod($client, 'getUserDetails', $api_params);

			$this->writelogFileApiCaller('getUserDetails', $api_params, $result);

			if (is_null($result)) {
				//try to get new session and connect again
				for ($i = 0; $i <= 1; $i++) {
					$this->sessionStart(true);

					$api_params = array();
					$api_params['sessionToken']	= $this->sessionToken;
					$api_params['userId']		= $listUser;

					$client = new \SoapClient($this->info[$this->env]['host'] . $this->serviceUrl['user'], array('exceptions' => 1));
					$result = $this->callSoapMethod($client, 'getUserDetails', $api_params);
				}
			}

			$resultReturn = array();

			if (!is_null($result)) {
				if ($result->resultCode == 200) {
					if (sizeof($listUser) > 1) {
						foreach ($result->users as $user) {
							foreach ($user as $item) {
								$arrTmp = array();
								$arrTmp['id']				= (isset($item->id)) ? $item->id : null;
								$resultReturn[$arrTmp['id']] = $this->buildDataUserDetailReturn($item);
							}
						}
					} else {
						if(is_array($listUser)){
							foreach ($result->users as $item) {
								$arrTmp['id']				= (isset($item->id)) ? $item->id : null;
								$resultReturn[$arrTmp['id']] = $this->buildDataUserDetailReturn($item);
							}
						}else{
							foreach ($result->users as $item) {
								$resultReturn = $this->buildDataUserDetailReturn($item);
							}
						}
					}
				}
			}

			return $resultReturn;
		} catch (\Exception $e) {
			throw $e;
		}
	}

	private function buildDataUserDetailReturn($item)
	{
		$arrTmp = array();
		$arrTmp['id']			= (isset($item->id)) ? $item->id : null;
		$tmpFisrtName			= (isset($item->firstName)) ? $item->firstName : null;
		$tmpLastName			= (isset($item->lastName)) ? $item->lastName : null;
		$arrTmp['firstName']	= $tmpFisrtName;
		$arrTmp['lastName']		= $tmpLastName;
		$arrTmp['name']			= ($tmpFisrtName != '') ? $tmpFisrtName . ' ' . $tmpLastName : $tmpLastName;
		$arrTmp['email']		= (isset($item->emailAddress)) ? $item->emailAddress : null;
		$arrTmp['insingUserName']		= (isset($item->insingUserName)) ? $item->insingUserName : null;

		return $arrTmp;
	}
	/**
	 * @author Cuong.Bui
	 * @param array $params
	 */
	public function searchBusiness($params)
	{
		$this->sessionStart();

		if (!isset($params['term']) || trim($params['term']) == '') {
			throw new \Exception('[term] required', self::C_PARAMETER_REQUIRED);
		}

		if (!isset($params['pageNumber'])) {
			$params['pageNumber'] = 1;
		} else {
			$params['pageNumber'] = intval($params['pageNumber']);

			if ($params['pageNumber'] == 0) {
				$params['pageNumber'] = 1;
			}
		}

		if (!isset($params['pageSize'])) {
			$params['pageSize'] = 10;
		} else {
			$params['pageSize'] = intval($params['pageSize']);

			if ($params['pageSize'] == 0) {
				$params['pageSize'] = 10;
			}
		}

		$params['sessionToken'] = $this->sessionToken;

		try {
			$return = array();

			$client = new \SoapClient($this->info[$this->env]['host'] . $this->serviceUrl['business'], array('exceptions' => 1));
			$result = $this->callSoapMethod($client, 'searchOperation', $params);

			$this->writelogFileApiCaller('searchBusiness function', $params, $result);

			if (isset($result) && !empty($result) && $result->resultCode == 200) {
				$results = $result->results->result;

				if (count($results) == 1) {
					$return[] = array(
						'id' => $results->id,
						'name' => $results->name
					);
				} else {
					foreach ($results as $item) {
						$return[] = array(
							'id' => $item->id,
							'name' => $item->name
						);
					}
				}
			}

			return $return;
		} catch (\Exception $e) {
			$this->writelogFileApiCaller('searchBusiness function With Try Catch Block', $params, $e);
			throw $e;
		}
	}

	/**
	 * @author Cuong.Bui
	 * @param array $itemId or oneItemID
	 * @return : array(idItem => itemName,....)
	 */
	public function getItemNames($params = null)
	{
		$this->sessionRestStart();

		try {
			$url = $this->info[$this->env]['host_rest'] . $this->serviceUrl['business_details'];
			$commonFields = array(
				'sessionToken' => $this->sessionRestToken
			);

			$fields = array();
			$resultReturn = array();

			if(!is_array($params))	{
				$params = array($params);
			}

			if ($params == null) {
				$fields = $commonFields;
			} else {
				$paramTemp = array();
				foreach ($params as $item){
					$paramTemp[] = (int)$item;
				}
				$fields = $commonFields + $paramTemp;
			}

			$params = $this->getFieldsStringWithSignature($url, $fields);
			$result = $this->curlPostString($url, $params);

			$this->writelogFileApiCaller('getItemNames function', $params, $result);

			if (isset($result) && !empty($result) && $result['resultCode'] == 200) {
				foreach ($result['businessDetails'] as $item) {
					$resultReturn[$item['id']] = $item['tradingName'];
				}
			}

			return $resultReturn;
		} catch (\Exception $e) {
			$this->writelogFileApiCaller('getItemNames function With Try Catch Block', $params, $e);
			throw $e;
		}
	}

	/**
	* Ex :
	* @author	Phuc.Nguyen
	* @method	function write log
	* @param
	* @return
	* @since	Sep 17, 2012 4:19:13 PM
	*/
	private function writelogFileApiCaller($message, $input = array(), $result = array())
	{
		$context = array();
		$tmp = array();
		$tmp['data_input'] 		= $input;
		$tmp['data_output'] 	= $result;

		//get api log name from parameters.ini
		$file_name = $this->container->getParameter('rnr_admin_log_name', 'api_caller');

		//set file type
		$file_type = '.log';

		//get log directory
		$dir_log = $this->container->get('kernel')->getRootDir().'/logs/';

		//path default
		$path = $dir_log . $file_name . $file_type;

		//get api logs maxsize
		$fileSize       =   0;
		$maxSize        =   $this->container->getParameter('rnr_admin_max_size_log_file');;

		//File Exists
		$fileExists     =   file_exists($path);
		if (!$fileExists)
		{
			@chmod($path,0666);
		}else {

			$fileSize   =   filesize($path);
		}

		//Rename file
		$fileDate       =   date('YmdHms');
		$fileNew        =   $file_name.'-'.$fileDate.'.'.$file_type;
		$pathNew        =   $dir_log.$fileNew;

		if($fileSize > $maxSize)
		{
			@rename($path,$pathNew);
		}

		$stream = new StreamHandler($path, Logger::INFO);
		// Create the main logger of the app
		$logger = $this->container->get('logger');

		$logger->pushHandler($stream);

		$context['message'] = $tmp;

		$code_line 				= '';
		$temp_debug_track 		= debug_backtrace();
		$call_info 				= array_shift($temp_debug_track);

		if ($call_info != '')	{
			$code_line = $call_info['line'];
		}

		$context['extra'] = 'line:'.$code_line;

		$logger->addInfo($message, $context);
		//remove pop hander
		$logger->popHandler();
	}
}
?>