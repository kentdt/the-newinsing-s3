<?php

/**
 * This class contains some functions which use for querying db
 * 
 * @author Co Vu Thanh Tung (improved by Trung Nguyen)
 */

namespace API\CoreBundle\Util;

use Doctrine\ORM\EntityManager;

class SqlService 
{
	/**
	 * Doctrine Connection
	 *
	 * @var \Doctrine\DBAL\Connection
	 */
	protected $conn;
	
	/**
	 * @var inSing\UtilBundle\Cache
	 */
	protected $cache;
	
	/**
	 * @var inSing\UtilBundle\Logger
	 */
	protected $logger;
	
	/**
	 * Constructor of SqlService
	 * 
	 * @author Co Vu Thanh Tung
	 * @param  ContainerAware $container
	 * @param  EntityManager  $em
	 */
	public function __construct($container, EntityManager $em) 
	{
	    // get cache object
		$cacheType = $container->getParameter('new_insing_api_session_token_cache');
		$this->cache = $container->get('insing.util.cache')->getInstanceByName($cacheType);
		
		// get logger
		$this->logger = $container->get('insing.util.logger');
		
		// get DB connection
		$this->conn	= $em->getConnection();
	}
	
	/**
	 * Check $appId, $password are valid (exist and not expired)
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $appId 
	 * @param  string $password
	 * @return array
	 */
	public function chechAppId($appId, $password)
	{
		$sql = 'SELECT id FROM `application` WHERE app_id = ? AND secret = ?';
		
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(1, $appId);
		$stmt->bindValue(2, $password);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
	
	/**
	 * Check $password from $appId
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $appId
	 * @return string $password
	 */
	public function getSecretFromAppId($appId)
	{
		$sql = 'SELECT id, secret FROM `application` WHERE app_id = ?';
	
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(1, $appId);
		$stmt->execute();
		return $stmt->fetchAll();
	}
	
	/**
	 * Check session token is valid (exist and not expired)
	 * 
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @return array  $result
	 */
	public function checkSessionToken($token)
	{
		if (!is_null($this->cache) && $this->cache->hasCache($token)) {
			$data = $this->cache->getCache($token);
			
			if (!is_array($data) || !isset($data[0]) || !isset($data[0]['is_expired'])) {
    			$this->logger->addErrorToFile('[DEBUG]'.json_encode($data));
			    $data = null;
			} else {
			    $data[0]['is_expired'] = date('Y-m-d H:i:s', strtotime($data[0]['expired_datetime'])) >= date('Y-m-d H:i:s') ? 0 : 1; 
    			$this->cache->setCache($token, $data);
			}
			
			return $data;
			
		} else {
			$sql = "SELECT id, expired_datetime, IF(expired_datetime >= '[TIME]' , 0, 1) AS is_expired "; 
			$sql.= 'FROM `session_token` WHERE token = ?';

			$stmt = $this->conn->prepare(strtr($sql, array('[TIME]' => date('Y-m-d H:i:s'))));
			$stmt->bindValue(1, $token);
			$stmt->execute();
			
			return $stmt->fetchAll();
		}
	}
	
	/**
	 *
	 * Check user token is valid (exist and not expired)
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @return array $result
	 */
	public function checkUserToken( $token )
	{
		$sql = "SELECT id, IF(expired_datetime >= '[TIME]', 0, 1) AS is_expired FROM `user_token` WHERE token = ? ";

		$stmt = $this->conn->prepare(strtr($sql, array('[TIME]' => date('Y-m-d H:i:s'))));
		$stmt->bindValue(1, $token);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
	
	/**
	 *
	 * Insert $token into table session_token
	 *
	 * @author Co Vu Thanh Tung
	 * @param 	string $token 
	 * @param		string $appId
	 * @param		string $expiredDate
	 * @return 	void
	 */
	public function insertSessionToken($token, $appId, $expiredDate)
	{
		$data = array(
			'token'				=>	$token,
			'app_id'			=>	$appId,
			'expired_datetime'	=>  $expiredDate
		) ;
		$result = $this->conn->insert('session_token', $data);
		
		if ($result > 0 && !is_null($this->cache)) {
		    $arrCache = array(array('expired_datetime' => $expiredDate, 'is_expired' => 0));
			$this->cache->setCache($token, $arrCache);
		}
	}
	
	/**
	 * Insert $token into table user_token
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @param  string $insingId
	 * @param  string $expiredDate
	 * @return void
	 */
	public function insertUserToken($token, $insingId, $expiredDate)
	{
		$data = array(
			'token'				=>	$token,
			'insing_id'			=>	$insingId,
			'expired_datetime'	=>  $expiredDate
		) ;
		
		$this->conn->insert('user_token', $data);
	}
	
	/**
	 * Get information by valid session token (exist and not expired)
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @return array
	 */
	public function getInfoBySessionToken($token)
	{
		$sql = 'SELECT id, app_id FROM `session_token` WHERE token = ? ';
		
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(1, $token);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
	
	/**
	 * Get information by valid user token (exist and not expired)
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @return array
	 */
	public function getInfoByUserToken($token)
	{
		$sql = 'SELECT id, insing_id FROM `user_token` WHERE token = ?';
	
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(1, $token);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
	
	/**
	 * Get information by insing id 
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $insingId
	 * @return array
	 */
	public function getInfoByinSingId($insingId)
	{
		$sql = 'SELECT id, token FROM `user_token` WHERE insing_id = ? ';
	
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(1, $insingId);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
	
	/**
	 * Update expired date by session token
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @return boolean
	 */
	public function updateExpiredDateBySessionToken($token, $expiredTime)
	{
		$data = array('expired_datetime' =>	$expiredTime) ;
		$identifier = array('token' =>	$token) ;
		$result = $this->conn->update('session_token', $data, $identifier);
		
		if ($result > 0 && !is_null($this->cache)) {
		    $this->cache->setCache( $token, $arrCache);
		}
	}
	
	/**
	 * Update expired date by user token
	 *
	 * @author Co Vu Thanh Tung
	 * @param  string $token
	 * @return boolean
	 */
	public function updateExpiredDateByUserToken($token, $expiredTime)
	{
		$sql = 'UPDATE `user_token` SET expired_datetime = ? WHERE token = ?';
	
		$stmt = $this->conn->prepare($sql);
		$stmt->bindValue(1, $expiredTime);
		$stmt->bindValue(2, $token);
		$stmt->execute();
	}
	
	/**
	 * Get app_id by session_token
	 * 
	 * @author Trung nguyen
	 * @param  string	$session_token
	 * @return string
	 */
	public function getAppIdBySessionToken($session_token)
	{
		$stmt = $this->conn->executeQuery('SELECT app_id FROM session_token WHERE token = ?', array($session_token));
		$result = $stmt->fetch();
		
		return $result ? $result['app_id'] : false; 
	}
}