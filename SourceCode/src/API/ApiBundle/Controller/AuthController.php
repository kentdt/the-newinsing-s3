<?php
namespace API\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes as APICode;
use API\ApiBundle\Util\APIConstant;
use API\ApiBundle\Util\APIConstantCode;
use API\ApiBundle\Util\UserMessage;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends Controller
{
    public function authenticateAction(Request $request)
    {
        $params = $request->request->all();

        if (!isset($params['access_token']) || $params['access_token'] == '' || !isset($params['access_token_secret']) || $params['access_token_secret'] == '') {
            throw new \Exception(UserMessage::ERROR_PARAMS_AUTHETICATE_INVALID_MSG, UserMessage::ERROR_AUTH_FAIL_CODE);
        }

        // get 4 parameters for sso authentication
        $consumerKey = $request->get('consumer_key');
            //$this->container->getParameter('new_insing_api_sso_consumer_key');
        $consumerSecret = $request->get('consumer_secret');
            //$this->container->getParameter('new_insing_api_sso_consumer_secret');
        $accessToken = $params['access_token'];
        $tokenSecret = $params['access_token_secret'];

        $params['consumer_key'] = $consumerKey;
        $params['consumer_secret'] = $consumerSecret;

        $params['sig'] = $request->get('sig');

        $resultCode = Codes::HTTP_NOT_FOUND;

        if ($accessToken == '' || $tokenSecret == '') {
            throw new \Exception('', Codes::HTTP_BAD_REQUEST);
        }
        if (is_null($accessToken) || is_null($tokenSecret)) {
            throw new \Exception('', Codes::HTTP_BAD_REQUEST);
        }

        $contentType = '';

        $this->get('inSing.services.signature')
                 ->checkSignature($params, $contentType, $request->getPathInfo(), $request->getContent());

        // 1. API User calls API with user/pass to get session_token.
        // 2. API forwards the request to SSO, which will verify the user
        // and return with the session_token and the role.
        $oauth = new \OAuth($consumerKey, $consumerSecret);
        $oauth->setSSLChecks(1);
        $userRoleUrl = $this->container->getParameter('new_insing_api_oauth_user_role_url');


        // call sso system
        $oauth->setToken($accessToken, $tokenSecret);
        $oauth->fetch($userRoleUrl, null, OAUTH_HTTP_METHOD_GET);

        $response = json_decode($oauth->getLastResponse(), true);
        if (is_array($response) && $response['status'] == APICode::HTTP_OK) {
            $sessionToken = md5(uniqid('', true));
            $roles = array();
            foreach ($response['roles'] as $role) {
                $roles[] = strtoupper($role);
            }

            // for checking write permission
            $this->roles = implode('\',\'', $roles);

            // for storing roles in api_session table
            $roles = implode(',', $roles);

            // check permission, if only user has API_WRITE
            if ($this->container->get('insing.token.listener')->hasReadPermission($roles)) {
                $expireRange = $this->container->getParameter('new_insing_api_session_token_life_time');
                $expireTime = time() + intval($expireRange);

                $dateTimeFormat = APIConstant::DATETIME_DB_FORMAT;
                $storedExpireDate = \DateTime::createFromFormat($dateTimeFormat, date($dateTimeFormat, $expireTime));
                //st modify format expire_at into timestamp, Dat.Dao, 31-01-2015
                //$expireDateFormatResponse = $storedExpireDate->format(\DateTime::W3C);
                $expireDateFormatResponse = (string)$storedExpireDate->getTimestamp();
                //ed modify format expire_at into timestamp, Dat.Dao, 31-01-2015
                $storedExpireDate = $storedExpireDate->format('Y-m-d H:i:s');

                // 3. The session_token and role are written to the `api_session` table.
                $sessionTokenData = array(
                    'roles' => $roles,
                    'sessiontoken' => $sessionToken,
                    'expired_at' => $storedExpireDate,
                    'secret' => $tokenSecret,
                    'app_id' => $accessToken,
                );

                $this->container->get('SesstionTokenTable')->addNewSesstionToken($sessionTokenData);

                $resultCode = APIConstantCode::CODE_LOGIN_SUCCESSFUL;

                // 4. The session_token is returned to the API User.
                $data = array(
                    'status' => $resultCode,
                    'data' => array(
                        'session_token' => $sessionToken,
                        'expire_at' => $expireDateFormatResponse
                    )
                );

                return new JsonResponse($data);
            } else {
                throw new \Exception(UserMessage::ERROR_AUTH_FAIL_MSG, UserMessage::ERROR_AUTH_FAIL_CODE);
            }
        } else {
            throw new \Exception(UserMessage::ERROR_AUTH_FAIL_MSG, UserMessage::ERROR_AUTH_FAIL_CODE);
        }
    }
}
