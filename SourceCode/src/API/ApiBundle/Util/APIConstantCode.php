<?php
namespace API\ApiBundle\Util;

/**
 * contains all the code constants used in Stylplus API
 *
 * @author Hoang
 */
class APIConstantCode
{
    /**
     * value: 200
     * description: Success
     */
    const CODE_SUCCESSFUL = 200;
    
    /**
     * value: 201
     * description: Created
     */
    const CODE_CREATED = 201;
    
    /**
     * value: 400
     * description: Failure. See "error" in response for the reason.
     */
    const CODE_FAILURE = 400;
    
    /**
     * value: 401
     * description: Unauthorized, i.e. authentication is unsuccessful or session token is invalid.
     */
    const CODE_UNAUTHORIZED = 401;
    
    /**
     * value: 403
     * description: Forbidden, i.e. you do not have access to this API.
     */
    const CODE_FORBIDDEN = 403;
    
    /**
     * value: 404
     * description: Not Found.
     */
    const CODE_NOT_FOUND = 404;
    
    /**
     * value: 419
     * description: Session token timeout. Please re-authenticate with Authenticate API.
     */
    const CODE_SESSION_TOKEN_TIMEOUT = 419;
    
    /**
     * value: 500
     * description: Server Error
     */
    const CODE_SERVER_ERROR = 500;
    
    /**
     * value: 503
     * description: Service unavailable.
     */
    const CODE_SERVER_UNAVAILABLE = 503;
    
    
    /**
     * indicates something wrong in the server
     * value: 500
     * description: Success
     */
    const CODE_UNKNOW_SERVER_ERROR = 500;


    /**
     * indicates something that is invalid
     * value: 402
     */
    const CODE_SOMETHING_INVALID = 402;


    /**
     * indicates something that is invalid
     * value: 404
     */
    const CODE_INVALID_REQUEST = 404;
    
    const CODE_UNKNOWN_ERROR = 400;
    const CODE_LOGIN_CREDITIALS_INVALID = 401;    
    //const CODE_NOT_FOUND = 404;
    const CODE_NOT_AUTHORIZED = 403;
    const CODE_SERVICE_UNAVAILABLE = 501;
    const CODE_SESSION_TOKEN_EXPIRED = 408;

    const CODE_LOGIN_SUCCESSFUL = 200;    

    const CODE_METHOD_NOT_ALLOWED = 405;
}
