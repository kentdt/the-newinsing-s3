$(document).ready(function() {    
    $('#button-featured-get-content').on('click', function() {
        $('#error-content-id').hide();
        $('#img-loading').show();
        $('#button-content-get-content').attr('disabled', 'disabled');

        var type = $('#admin_content_content_type').val();
        var id   = $('#admin_content_content_id').val();

        if (id != '' ) {
            $.ajax({
                url : $('#get-content-url').val(),
                type: 'GET',
                data: {
                    'from': 'content',
                    'type': type,
                    'id'  : id,
                    'channel' : 'insing.com'
                },
                success: function(response) {
                    if (typeof response.status != 'undefined' && response.status == 200 && response.validate == 1) {
                        var details = response.result;

                        $('#admin_content_title').val(details.title);
                        $('#admin_content_description').val(details.description);
                        $('#admin_content_content_url').val(details.content_url);
                        $('#admin_content_features_url').val(details.features_url);
                        $('#admin_content_image_url').val(details.image_url);
                        $('#admin_content_video_url').val(details.video_url);
                        $('#admin_content_series_desc').val(details.series_description);
                        $('#admin_content_series_name').val(details.series_name);
                        $('#admin_content_series_url').val(details.series_url);
                        $('#admin_content_channel').val(details.channel);
                        $('#admin_content_published_at').val(details.published_at);
                        $('#admin_content_updated_at').val(details.updated_at);
                        $('#admin_content_photo_count').val(details.photo_count);

                        $('#img-loading').hide();
                        $('#button-content-get-content').removeAttr('disabled');
                    } else if (typeof response.status != 'undefined' && response.status == 200 && response.validate != 1) {
                        $('#admin_content_title').val('');
                        $('#admin_content_description').val('');
                        $('#admin_content_content_url').val('');
                        $('#admin_content_features_url').val('');
                        $('#admin_content_image_url').val('');
                        $('#admin_content_video_url').val('');
                        $('#admin_content_series_desc').val('');
                        $('#admin_content_series_name').val('');
                        $('#admin_content_series_url').val('');
                        $('#admin_content_channel').val('');
                        $('#admin_content_published_at').val('');
                        $('#admin_content_updated_at').val('');
                        $('#admin_content_photo_count').val(0);

                        $('#error-content-id').text('Content is not valid');
                        $('#error-content-id').show();

                        $('#img-loading').hide();
                        $('#button-content-get-content').removeAttr('disabled');
                    }else {
                        $('#admin_content_title').val('');
                        $('#admin_content_description').val('');
                        $('#admin_content_content_url').val('');
                        $('#admin_content_features_url').val('');
                        $('#admin_content_image_url').val('');
                        $('#admin_content_video_url').val('');
                        $('#admin_content_series_desc').val('');
                        $('#admin_content_series_name').val('');
                        $('#admin_content_series_url').val('');
                        $('#admin_content_channel').val('');
                        $('#admin_content_published_at').val('');
                        $('#admin_content_updated_at').val('');
                        $('#admin_content_photo_count').val(0);

                        $('#error-content-id').text('No data found.');
                        $('#error-content-id').show();

                        $('#img-loading').hide();
                        $('#button-content-get-content').removeAttr('disabled');
                    }
                },
                error: function() {
                    alert('Oops, error occurred.');

                    $('#img-loading').hide();
                    $('#button-content-get-content').removeAttr('disabled');
                }
            });
        } else {
            $('#error-content-id').text('Please input ID to get data.');
            $('#error-content-id').show();
            $('#img-loading').hide();
            $('#button-content-get-content').removeAttr('disabled');
        }
    });
});